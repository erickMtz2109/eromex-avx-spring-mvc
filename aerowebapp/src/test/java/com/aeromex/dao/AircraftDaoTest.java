//package com.aeromex.dao;
//
//import static org.junit.Assert.assertTrue;
//
//import java.util.List;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.aeromex.dao.AircraftDao;
//import com.aeromex.entity.AircraftEntity;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = { "file:WebContent/WEB-INF/dao-context.xml" })
//public class AircraftDaoTest {
//	
//	@Autowired
//	@Qualifier("aircraftDao")
//	AircraftDao aircraftDao;
//	
//	@Test
//	public void testListAircraft() {
//		List<AircraftEntity> airList = aircraftDao.AircraftList();
//		System.out.println("Aircraft  DAO");
//		for(AircraftEntity air : airList) {
//			System.out.println(air);
//		}
//	    
//		assertTrue(true);
//	}
//
//}
