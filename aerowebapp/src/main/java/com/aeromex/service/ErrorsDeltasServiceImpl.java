package com.aeromex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.aeromex.dao.PersonAccountDao;

@Service
public class ErrorsDeltasServiceImpl implements ErrorsDeltasService{
	@Autowired
	PersonAccountDao PsnAccDao;
	
	@Override
	@Async
	public void processErorrsSync() {
		PsnAccDao.clearPersonAccountHerokuErrors();
	}
	

}
