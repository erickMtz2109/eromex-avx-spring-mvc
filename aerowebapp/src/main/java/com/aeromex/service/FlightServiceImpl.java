package com.aeromex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aeromex.entity.FlightEntity;
import org.modelmapper.ModelMapper;
import com.aeromex.dao.FlightDao;
import com.aeromex.model.Flight;
import java.util.ArrayList;
import java.util.List;

@Service
public class FlightServiceImpl implements FlightService{

	@Autowired
	FlightDao flightDao;

	public List<Flight> getAllFlights() {
		return listFlightsToModel(flightDao.flightList());
	}

	private Flight FlightToModel(FlightEntity FlightEntity) {
		ModelMapper modelMapper = new ModelMapper();
		Flight flightLts = modelMapper.map(FlightEntity, Flight.class);
		return flightLts;
	}

	private List<Flight> listFlightsToModel(List<FlightEntity> allFlightEntities) {
		List<Flight> lstResult = new ArrayList<Flight>();
		for (FlightEntity obj : allFlightEntities) {
			lstResult.add(FlightToModel(obj));
		}
		return lstResult;
	}
}
