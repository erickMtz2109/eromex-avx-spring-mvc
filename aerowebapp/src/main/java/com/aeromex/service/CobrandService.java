package com.aeromex.service;

import java.util.List;
import com.aeromex.model.Cobrand;

public interface CobrandService {
	List<Cobrand> getAllCobrands();
}
