package com.aeromex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aeromex.entity.AircraftEntity;

import org.modelmapper.ModelMapper;
import com.aeromex.dao.AircraftDao;
import com.aeromex.model.Aircraft;

import java.util.ArrayList;
import java.util.List;

@Service
public class AircraftServiceImpl implements AircraftService{
	@Autowired
	AircraftDao aircraftDao;

	public List<Aircraft> getAllAircrafts() {
		return listAircraftsToModel(aircraftDao.AircraftList());
	}
	private static Aircraft AircraftToModel(AircraftEntity AircraftEntity) {
		ModelMapper modelMapper = new ModelMapper();
		Aircraft Aircrafts = modelMapper.map(AircraftEntity, Aircraft.class);
		return Aircrafts;
	}
	public static List<Aircraft> listAircraftsToModel(List<AircraftEntity> allAircraftEntities) {
		List<Aircraft> lstResult = new ArrayList<Aircraft>();
		for (AircraftEntity obj : allAircraftEntities) {
			lstResult.add(AircraftToModel(obj));
		}
		return lstResult;
	}
	
	public static List<AircraftEntity> modelToAircraftEntity(List<Aircraft> allAircrafts) {
		List<AircraftEntity> lstResult = new ArrayList<AircraftEntity>();
		for(Aircraft obj : allAircrafts) {
			lstResult.add(modelToPAEntity(obj));
		}
		return lstResult;
	}
	
	private static AircraftEntity modelToPAEntity(Aircraft accountObj) {		
		ModelMapper modelMapper = new ModelMapper();
		AircraftEntity Aircrafts = modelMapper.map(accountObj, AircraftEntity.class);	
		return Aircrafts;
	} 

}
