package com.aeromex.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aeromex.dao.DeltasLogEntityDAO;
import com.aeromex.dao.PersonAccountDao;
import com.aeromex.entity.PersonAccountEntity;
import com.aeromex.entity.RegresoAltasAnalitica;
import com.aeromex.model.FindAccount;
import com.aeromex.model.PersonAccount;

@Service
public class PersonAccountServiceImpl implements PersonAccountService {

    @Autowired
    PersonAccountDao personAccountDao;
    @Autowired
    DeltasLogEntityDAO deltasLogEntityDAO;

    public List<PersonAccount> getAllPersonAccounts() {
        return listEntityToModel(personAccountDao.personAccountList());
    }

//    public List<PersonAccount> createPersonAccounts(List<PersonAccount> personAccLts) {
//        List<PersonAccountEntity> result = personAccountDao
//                .createPersonAccount(modelToPersonAccountEntity(personAccLts));
//        return listEntityToModel(result);
//    }

//    public PersonAccount getSpecificPersonAccount(String id_sfdc) {
//        return entityToModel(personAccountDao.getSpecificPersonAccout(id_sfdc));
//    }

    public List<PersonAccount> updatePersonAccounts(List<PersonAccount> personAccLts) {
        return listEntityToModel(personAccountDao.updatePersonAccout(modelToPersonAccountEntity(personAccLts)));
    }

    public static PersonAccount entityToModel(PersonAccountEntity PersonAccountEntity) {
        ModelMapper modelMapper = new ModelMapper();
        PersonAccount PersonAccount = modelMapper.map(PersonAccountEntity, PersonAccount.class);
        return PersonAccount;
    }

    public static List<PersonAccount> listEntityToModel(List<PersonAccountEntity> allPersonAccountEntities) {
        List<PersonAccount> lstResult = new ArrayList<PersonAccount>();
        for (PersonAccountEntity obj : allPersonAccountEntities) {
            lstResult.add(entityToModel(obj));
        }
        return lstResult;
    }

    private List<PersonAccountEntity> modelToPersonAccountEntity(List<PersonAccount> allAccounts) {
        List<PersonAccountEntity> lstResult = new ArrayList<PersonAccountEntity>();
        for (PersonAccount obj : allAccounts) {
            lstResult.add(modelToPAEntity(obj));
        }
        return lstResult;
    }

    public static PersonAccountEntity modelToPAEntity(PersonAccount accountObj) {
        ModelMapper modelMapper = new ModelMapper();
        PersonAccountEntity accounts = modelMapper.map(accountObj, PersonAccountEntity.class);
        return accounts;
    }

	public List<PersonAccountEntity> getDeltas(Date fecha) {
		List<PersonAccountEntity> deltas = personAccountDao.getDeltas(fecha);
		for (PersonAccountEntity delt : deltas) {
			int length = delt.getSvoc_customerid__c().length();
			if (length != 12 && length < 13) {
				int num_0 = 12 - length;
				String txtNum = "";
				for (int i = 0; i < num_0; i++) {
					txtNum = txtNum.concat("0");
				}
				delt.setSvoc_customerid__c(txtNum.concat(delt.getSvoc_customerid__c()));				
			}
		}
		return deltas;
	}

    @Override
    public List<PersonAccountEntity> getDeltas() {

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.add(Calendar.DATE, -7);
            Date fecha = cal.getTime();

        System.out.println("Fecha a utilizar en deltas:  " + fecha);
        
        return getDeltas(fecha);
    }
    
    public List<FindAccount> findAccounts(List<FindAccount> fAccLts){
    	fAccLts = personAccountDao.findAccountsDao(fAccLts);
    	return fAccLts;
    }

	@Override
	public List<RegresoAltasAnalitica> getAltasAnalitica() {
		return personAccountDao.getAltasAnalitica();
	}
}
