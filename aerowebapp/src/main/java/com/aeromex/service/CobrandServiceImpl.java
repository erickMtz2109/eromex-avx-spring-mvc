package com.aeromex.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aeromex.dao.CobrandDao;
import com.aeromex.entity.CobrandEntity;
import com.aeromex.model.Cobrand;

@Service
public class CobrandServiceImpl implements CobrandService {

	@Autowired
	CobrandDao cobrandDao;

	public List<Cobrand> getAllCobrands() {
		return listCobrandsToModel(cobrandDao.cobrandList());
	}

	private Cobrand CobrandToModel(CobrandEntity cobrandEntity) {
		ModelMapper modelMapper = new ModelMapper();
		Cobrand CobrandLts = modelMapper.map(cobrandEntity, Cobrand.class);
		return CobrandLts;
	}

	private List<Cobrand> listCobrandsToModel(List<CobrandEntity> allCobrandEntities) {
		List<Cobrand> lstResult = new ArrayList<Cobrand>();
		for (CobrandEntity obj : allCobrandEntities) {
			lstResult.add(CobrandToModel(obj));
		}
		return lstResult;
	}

	public static CobrandEntity modelToCobrandEntity(Cobrand cobrandObj) {
		ModelMapper modelMapper = new ModelMapper();
		CobrandEntity cobrands = modelMapper.map(cobrandObj, CobrandEntity.class);
		return cobrands;
	}

}
