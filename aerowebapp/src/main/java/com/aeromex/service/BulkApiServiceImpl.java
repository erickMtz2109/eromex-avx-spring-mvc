package com.aeromex.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.aeromex.dao.Bulk_LogDao;
import com.aeromex.dao.PersonAccountDaoImpl;
import com.aeromex.entity.SvocLogEntity;
import com.aeromex.model.BigDeltaError;
import com.aeromex.utility.GenerateTokenUtil;

@Service
public class BulkApiServiceImpl implements BulkApiService {
	@Autowired
	Bulk_LogDao bulk_LogDao;

	private static String ERROR_BULK_URL = System.getenv("ERROR_BULK_URL");

	@Override
	@Async
	public void getErrorsBulkApi() {
		PersonAccountDaoImpl errorLogsAccount = new PersonAccountDaoImpl();
		List<BigDeltaError> listErrorLog = convertBigError(errorLogsAccount.getErrorLogs());
		List<BigDeltaError> errorLogs = bulk_LogDao.bulksErrors();
		List<BigDeltaError> errorBajas = bulk_LogDao.sfdcError();
		if (!listErrorLog.isEmpty()) {
			errorLogs.addAll(listErrorLog);
		}
		if (!errorBajas.isEmpty()) {
			errorLogs.addAll(errorBajas);
		}
		// ________-------------------------_____________
//		SvocLogEntity errorLog = new SvocLogEntity();
//		errorLog.setId("3422");
//		errorLog.setCustomer_id("customer2");
//		errorLog.setDatetime_error(new Timestamp(System.currentTimeMillis()));
//		errorLog.setDml_type("create");
//		errorLog.setError_code(500);
//		errorLog.setError_message("error test");
//		errorLog.setFolio("23423432");
//		errorLog.setObject_error("Account");
//		List<SvocLogEntity> errorLogs = new ArrayList<SvocLogEntity>();
//		errorLogs.add(errorLog);
		// __________-----------________

		System.out.println("Data returned, sending.. ");
		sendErrors(errorLogs);
	}

	public void sendErrors(List<BigDeltaError> errorLogs) {
		String token = GenerateTokenUtil.getSalesforceToken();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		headers.set("Authorization", token);
		HttpEntity<List<BigDeltaError>> requestDE = new HttpEntity<List<BigDeltaError>>(errorLogs, headers);
//		String url = "https://am-svoc-big-person-prod.us-e2.cloudhub.io/api/aeromexico/person-errores";
		RestTemplate restTemplate = new RestTemplate();
		Object response = new Object();
		System.out.println("records to send -> [" + errorLogs.size() + "]");
		try {
			System.out.println("Calling mule request..");
			response = restTemplate.postForObject(ERROR_BULK_URL, requestDE, Object.class);
//			response = restTemplate.postForObject(url, requestDE, Object.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("response-> " + response);
		System.out.println("[Finish bulk errors async]");
	}
	
	private List<BigDeltaError> convertBigError(List<SvocLogEntity> svocErrlts) {
		List<BigDeltaError> bigDelErrList = new ArrayList<BigDeltaError>();		
		for (SvocLogEntity svocErr : svocErrlts) {
			BigDeltaError bigErr = new BigDeltaError();
			bigErr.setId(svocErr.getId());
			bigErr.setObject_error(svocErr.getObject_error());
			bigErr.setCustomer_id(svocErr.getCustomer_id());
			bigErr.setDatetime_error(svocErr.getDatetime_error());
			bigErr.setDml_type(svocErr.getDml_type());
			bigErr.setError_message(svocErr.getError_message());
			bigErr.setError_code(svocErr.getError_code());			
			bigDelErrList.add(bigErr);
		}		
		return bigDelErrList;
	}
	

}
