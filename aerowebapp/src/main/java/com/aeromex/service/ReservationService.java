package com.aeromex.service;

import java.util.List;
import com.aeromex.model.Reservation;

public interface ReservationService {
	List<Reservation> getAllReservations();
}
