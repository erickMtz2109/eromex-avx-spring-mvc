package com.aeromex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aeromex.entity.PassengerEntity;
import com.aeromex.dao.PassengerDao;
import org.modelmapper.ModelMapper;
import com.aeromex.model.Passenger;
import java.util.ArrayList;
import java.util.List;

@Service
public class PassengerServiceImpl implements PassengerService{
	
	@Autowired
	PassengerDao PassengerDao;

	public List<Passenger> getAllPassengers() {
		return listPassengersToModel(PassengerDao.passengerList());
	}
	private static Passenger PassengerToModel(PassengerEntity PassengerEntity) {
		ModelMapper modelMapper = new ModelMapper();
		Passenger passengers = modelMapper.map(PassengerEntity, Passenger.class);
		return passengers;
	}
	public static List<Passenger> listPassengersToModel(List<PassengerEntity> allPassengerEntities) {
		List<Passenger> lstResult = new ArrayList<Passenger>();
		for (PassengerEntity obj : allPassengerEntities) {
			lstResult.add(PassengerToModel(obj));
		}
		return lstResult;
	}
}
