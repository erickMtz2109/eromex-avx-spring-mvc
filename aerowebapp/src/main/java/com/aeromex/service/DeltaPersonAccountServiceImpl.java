package com.aeromex.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.aeromex.dao.CobrandDao;
import com.aeromex.dao.DeltasDao;
import com.aeromex.dao.DeltasLogEntityDAO;
import com.aeromex.dao.ErrorLogDao;
import com.aeromex.dao.FrequentFlyerDao;
import com.aeromex.dao.HierarchyDao;
import com.aeromex.dao.PersonAccountDao;
import com.aeromex.dao.ReprocesoDao;
import com.aeromex.entity.BigCobrandEntity;
import com.aeromex.entity.BigFrequentFlyerEntity;
import com.aeromex.entity.BigPersonAccountEntity;
import com.aeromex.entity.CobrandEntity;
import com.aeromex.entity.FrequentFlyerEntity;
import com.aeromex.entity.PersonAccountEntity;
import com.aeromex.entity.SVOCDeltasLogEntity;
import com.aeromex.entity.SvocLogEntity;
import com.aeromex.model.Cobrand;
import com.aeromex.model.FrequentFlyer;
import com.aeromex.model.PersonAccount;
import com.aeromex.utility.ValidString;

@Service
public class DeltaPersonAccountServiceImpl implements DeltaPersonAccountService {

    @Autowired
    DeltasDao deltadao;
    @Autowired
    PersonAccountDao personAccountDao;
    @Autowired
    CobrandDao cobrandDao;
    @Autowired
    FrequentFlyerDao frequentFlyerDao;
    @Autowired
    HierarchyDao hierarchyDao;
    @Autowired
    ErrorLogDao errorLogDao;
    @Autowired
    DeltasLogEntityDAO deltasLogEntityDAO;
    @Autowired
    ReprocesoDao reprocesoDao;

    @Override
    public List<SvocLogEntity> getErrors(String folio) {
        return errorLogDao.getLogError(folio);
    }

    @Override
    public List<PersonAccountEntity> getCreatedDeltas() {
        List<PersonAccountEntity> personAccCreated = personAccountDao.getPersonAccCreated();
        return personAccCreated;
    }

    @Override
    public List<PersonAccountEntity> getUpdatedDeltas() {
        List<PersonAccountEntity> personAccCreated = personAccountDao.getPersonAccUpdated();
        return personAccCreated;
    }

    @Async
    public void deleteAccountDeltas(List<PersonAccountEntity> personToDelete) {
        SVOCDeltasLogEntity deltasLog = new SVOCDeltasLogEntity();
        deltasLog.setFolio("" + System.currentTimeMillis());
        deltasLog.setTotalCuentas(personToDelete.size());
        deltasLog.setOperacion("BAJAS");
        
        deltasLog.setInicio(Calendar.getInstance().getTime());
        personAccountDao.deleteDelta(personToDelete);
        
        deltasLog.setFin(Calendar.getInstance().getTime());
        
        try {
            deltasLogEntityDAO.save(deltasLog);
        } catch (Exception e) {
            System.out.println("Error al guardar Deltas");
        }
        
    }

    @Async
    @Override
    public void bulkDeltas(List<PersonAccount> personAccounts, boolean cOrUAcc, String hilo,String folio) {
        long inicio = System.currentTimeMillis();
//        List<SvocLogEntity> listErros = ErrorProcessDeltas.errorlogs;
        String typeDML = cOrUAcc ? "altas" : "cambios";
        System.out.println(typeDML + " ## COMIENZA [Deltas] ## " + hilo);
        List<PersonAccountEntity> palist = new ArrayList<PersonAccountEntity>();
        List<CobrandEntity> cobrandList = new ArrayList<CobrandEntity>();
        List<FrequentFlyerEntity> ffList = new ArrayList<FrequentFlyerEntity>();
        
        SVOCDeltasLogEntity deltasLog = new SVOCDeltasLogEntity();
        deltasLog.setFolio(folio);
           // deltasLog.setHilo(hilo);
        deltasLog.setInicio(Calendar.getInstance().getTime());

		try {

			System.out.println(typeDML + " Antes del for convertidor -> " + java.time.LocalTime.now() + " " + hilo);
			for (PersonAccount pa : personAccounts) {
				palist.add(convertPA(pa));
				for (Cobrand cobrand : pa.getCobrands()) {
					if (!ValidString.isNullOrEmpty(cobrand.getSvoc_bank__c())) {
						if (!ValidString.isNullOrEmpty(cobrand.getSvoc_cardtier__c())) {
							if (!cOrUAcc) {
								cobrand.setSvoc_cuenta__c(pa.getSfid());
							}
							cobrandList.add(convertCobrand(cobrand));
						}
					}
				}
				for (FrequentFlyer ff : pa.getFrequentFlyers()) {
					if (!ValidString.isNullOrEmpty(ff.getSvoc_ffn_external_id__c())) {
						if (!ValidString.isNullOrEmpty(ff.getName())) {
							if (!cOrUAcc) {
								ff.setSvoc_cuenta__c(pa.getSfid());
							}
							ffList.add(convertFF(ff));
						}
					}
				}
			}
			deltasLog.setHilo(hilo);
			deltasLog.setOperacion(typeDML);
			deltasLog.setTotalCuentas(palist.size());
			deltasLog.setTotalCobrand(cobrandList.size());
			deltasLog.setTotalFrequentFlyer(ffList.size());

			personAccountDao.cambioPARecurrentList(palist, hilo, cOrUAcc);
			cobrandDao.upsertCobrand(cobrandList, hilo);
			frequentFlyerDao.upsertff(ffList, hilo);

		} catch (Exception e) {
			System.out.println("### Error al procesar DELTAS --> Hilo: " + hilo);
			e.printStackTrace();
		}
        
        deltasLog.setFin(Calendar.getInstance().getTime());
        
        try {
            deltasLogEntityDAO.save(deltasLog);
            deltasLog = null;
        } catch (Exception e) {
            System.out.println("Error al registrar cifras control, " + e);
        }

        long fin = System.currentTimeMillis();
        System.out.println(typeDML + " ## TERMINA [Deltas] ## " + hilo + " Tiempo: " + (fin - inicio) + " ms");
        palist = null;
        cobrandList = null;
        ffList = null;
    }
  
    private PersonAccountEntity convertPA(PersonAccount accObj) {
        PersonAccountEntity paEntity = new PersonAccountEntity();
        paEntity.setSfid(accObj.getSfid());
        paEntity.setSvoc_customerid__c(accObj.getSvoc_customerid__c());
        paEntity.setName(accObj.getName());
        paEntity.setFirstname(accObj.getFirstname());
        paEntity.setMiddlename(accObj.getMiddlename());
        paEntity.setLastname(accObj.getLastname());
        paEntity.setSuffix(accObj.getSuffix());
        paEntity.setSvoc_gaid__c(accObj.getSvoc_gaid__c());
        paEntity.setSvoc_spid__c(accObj.getSvoc_spid__c());
        paEntity.setSvoc_company__c(accObj.getSvoc_company__c());
        paEntity.setSvoc_flowntrips__c(accObj.getSvoc_flowntrips__c());
        paEntity.setSvoc_rfmtotal__c(accObj.getSvoc_rfmtotal__c());
        paEntity.setSvoc_rfm_tier__c(accObj.getSvoc_rfm_tier__c());
        paEntity.setSvoc_nps__c(accObj.getSvoc_nps__c());
        paEntity.setSvoc_language__c(accObj.getSvoc_language__c());
        paEntity.setSvoc_gendercode__c(accObj.getSvoc_gendercode__c());
        paEntity.setSvoc_dominantdestination__c(accObj.getSvoc_dominantdestination__c());
        paEntity.setSvoc_customercategory__c(accObj.getSvoc_customercategory__c());
        paEntity.setSvoc_nationality__c(accObj.getSvoc_nationality__c());
        paEntity.setSvoc_country__c(accObj.getSvoc_country__c());
        paEntity.setPersonbirthdate(accObj.getPersonbirthdate());
        paEntity.setSvoc_countrycodemobile__c(accObj.getSvoc_countrycodemobile__c());
        paEntity.setPersonmobilephone(accObj.getPersonmobilephone());
        paEntity.setSvoc_optin_mobile__c(accObj.getSvoc_optin_mobile__c());
        paEntity.setPersonemail(accObj.getPersonemail());
        paEntity.setSvoc_optin_email__c(accObj.getSvoc_optin_email__c());
        paEntity.setSvoc_countrycode__c(accObj.getSvoc_countrycode__c());
        paEntity.setPhone(accObj.getPhone());
        paEntity.setSvoc_optin_email2__c(accObj.getSvoc_optin_email2__c());//
        paEntity.setSvoc_officecountrycode__c(accObj.getSvoc_officecountrycode__c());
        paEntity.setSvoc_officephone__c(accObj.getSvoc_officephone__c());
        paEntity.setSvoc_extension__c(accObj.getSvoc_extension__c());
        paEntity.setSvoc_email2__c(accObj.getSvoc_email2__c());
        paEntity.setSvoc_email3__c(accObj.getSvoc_email3__c());
        paEntity.setSvoc_optincompanyemail__c(accObj.getSvoc_optincompanyemail__c());
        paEntity.setSvoc_passport_number__c(accObj.getSvoc_passport_number__c());
        paEntity.setSvoc_city__c(accObj.getSvoc_city__c());
//        paEntity.setPersonassistantname(accObj.getPersonassistantname());
//        paEntity.setSvoc_countrycodeassistant__c(accObj.getSvoc_countrycodeassistant__c());
//        paEntity.setPersonassistantphone(accObj.getPersonassistantphone());
//        paEntity.setSvoc_assistantemail__c(accObj.getSvoc_assistantemail__c());
        paEntity.setPersonleadsource(accObj.getPersonleadsource());
        paEntity.setSalutation(accObj.getSalutation());
//        paEntity.setSvoc_employeenumber__c(accObj.getSvoc_employeenumber__c());
        paEntity.setSvoc_contactemergencyname__c(accObj.getSvoc_contactemergencyname__c());
        paEntity.setSvoc_emergencycountrycode__c(accObj.getSvoc_emergencycountrycode__c());
        paEntity.setSvoc_emergencyphone__c(accObj.getSvoc_emergencyphone__c());
        paEntity.setSvoc_emergencyemail__c(accObj.getSvoc_emergencyemail__c());
        paEntity.setSvoc_relationship__c(accObj.getSvoc_relationship__c());
//        paEntity.setPersontitle(accObj.getPersontitle());
//        paEntity.setSvoc_ismigrated__c(accObj.getSvoc_ismigrated__c());
        paEntity.setSvoc_paexternalid__c(accObj.getSvoc_paexternalid__c());
        paEntity.setRecordtypeid(accObj.getRecordtypeid());
        paEntity.setSvoc_purchasepropensity__c(accObj.getSvoc_purchasepropensity__c());
        paEntity.setSvoc_winbackpropensity__c(accObj.getSvoc_winbackpropensity__c());
        paEntity.setSvoc_dominantorigin__c(accObj.getSvoc_dominantorigin__c());
        return paEntity;
    }

    private CobrandEntity convertCobrand(Cobrand cobrand) {
        CobrandEntity cbrEntity = new CobrandEntity();
        cbrEntity.setId(cobrand.getId());
        cbrEntity.setSfid(cobrand.getSfid());
        cbrEntity.setName(cobrand.getName());
        cbrEntity.setSvoc_cuenta__c(cobrand.getSvoc_cuenta__c());
        cbrEntity.setSvoc_cardnumber__c(cobrand.getSvoc_cardnumber__c());
        cbrEntity.setSvoc_bank__c(cobrand.getSvoc_bank__c());
        cbrEntity.setSvoc_customerid__c(cobrand.getSvoc_customerid__c());
        cbrEntity.setSvoc_cobrandexternalid__c(cobrand.getSvoc_cobrandexternalid__c());
        cbrEntity.setSvoc_cardtier__c(cobrand.getSvoc_cardtier__c());
        cbrEntity.setSvoc_membersince__c(cobrand.getSvoc_membersince__c());
        cbrEntity.setSvoc_expirationdate__c(cobrand.getSvoc_expirationdate__c());
//        cbrEntity.setSvoc_ismigrated__c(cobrand.getSvoc_ismigrated__c());
        return cbrEntity;
    }

    private FrequentFlyerEntity convertFF(FrequentFlyer ff) {
        FrequentFlyerEntity ffEntity = new FrequentFlyerEntity();
        ffEntity.setSfid(ff.getSfid());
        ffEntity.setName(ff.getName());
        ffEntity.setSvoc_cuenta__c(ff.getSvoc_cuenta__c());
        ffEntity.setSvoc_customerid__c(ff.getSvoc_customerid__c());
        ffEntity.setSvoc_ffn_external_id__c(ff.getSvoc_ffn_external_id__c());
        ffEntity.setSvoc_frequentflyertier__c(ff.getSvoc_frequentflyertier__c());
        ffEntity.setSvoc_frequentflyerprogram__c(ff.getSvoc_frequentflyerprogram__c());
        ffEntity.setSvoc_pcemail__c(ff.getSvoc_pcemail__c());
        return ffEntity;
    }
    
    @Async
    @Override
    public void bulkBigDeltas(List<PersonAccount> personAccounts, String hilo ,String dml_type) {
        long inicio = System.currentTimeMillis();
//        String typeDML = cOrUAcc ? "altas" : "cambios";
        System.out.println("BIG ## COMIENZA [Deltas] ## " + hilo);
        List<BigPersonAccountEntity> palist = new ArrayList<BigPersonAccountEntity>();
        List<BigCobrandEntity> cobrandList = new ArrayList<BigCobrandEntity>();
        List<BigFrequentFlyerEntity> ffList = new ArrayList<BigFrequentFlyerEntity>();        
        SVOCDeltasLogEntity deltasLog = new SVOCDeltasLogEntity();
        deltasLog.setFolio(hilo);
        deltasLog.setInicio(Calendar.getInstance().getTime());
        
        if(dml_type.contains("create")) {
        	dml_type = "altas";
        }else if(dml_type.contains("update")){
        	dml_type = "cambios";
        }

		try {
			System.out.println("BIG Antes del for convertidor -> " + java.time.LocalTime.now() + " " + hilo);
			for (PersonAccount pa : personAccounts) {
				palist.add(convertBigAcc(pa,dml_type));
				for (Cobrand cobrand : pa.getCobrands()) {
					if (!ValidString.isNullOrEmpty(cobrand.getSvoc_bank__c())) {
						if (!ValidString.isNullOrEmpty(cobrand.getSvoc_cardtier__c())) {
							cobrandList.add(convertBigCobrand(cobrand,dml_type));
						}
					}
				}
				for (FrequentFlyer ff : pa.getFrequentFlyers()) {
					if (!ValidString.isNullOrEmpty(ff.getSvoc_ffn_external_id__c())) {
						if (!ValidString.isNullOrEmpty(ff.getName())) {
							ffList.add(convertBigFF(ff,dml_type));
						}
					}
				}
			}
			deltasLog.setTotalCuentas(palist.size());
			deltasLog.setTotalCobrand(cobrandList.size());
			deltasLog.setTotalFrequentFlyer(ffList.size());

			personAccountDao.insertBigAccount(palist, hilo);
			cobrandDao.insertBigCobrand(cobrandList, hilo);
			frequentFlyerDao.insertBigFf(ffList, hilo);

		} catch (Exception e) {
			System.out.println("BIG ### Error al procesar DELTAS --> Hilo: " + hilo);
			e.printStackTrace();
		}        
        deltasLog.setFin(Calendar.getInstance().getTime());
        try {
            deltasLogEntityDAO.save(deltasLog);
            deltasLog = null;
        } catch (Exception e) {
            System.out.println("Error al registrar cifras control, " + e);
        }
        long fin = System.currentTimeMillis();
        System.out.println("BIG ## TERMINA [Deltas] ## " + hilo + " Tiempo: " + (fin - inicio) + " ms");
//        if(dml_type.equals("altas") || dml_type.contains("altas")) {
//        	System.out.println("Altas logicas comienza");
//            reprocesoDao.bigBulkReproceso(palist);
//        }
        palist = null;
        cobrandList = null;
        ffList = null;
    }

    private BigPersonAccountEntity convertBigAcc(PersonAccount accObj,String dml_type) {
        BigPersonAccountEntity bigPAccEnt = new BigPersonAccountEntity();
        bigPAccEnt.setSfid(accObj.getSfid());
        bigPAccEnt.setSvoc_customerid__c(accObj.getSvoc_customerid__c());
        bigPAccEnt.setName(accObj.getName());
        bigPAccEnt.setFirstname(accObj.getFirstname());
        bigPAccEnt.setMiddlename(accObj.getMiddlename());
        bigPAccEnt.setLastname(accObj.getLastname());
        bigPAccEnt.setSuffix(accObj.getSuffix());
        bigPAccEnt.setSvoc_gaid__c(accObj.getSvoc_gaid__c());
        bigPAccEnt.setSvoc_spid__c(accObj.getSvoc_spid__c());
        bigPAccEnt.setSvoc_company__c(accObj.getSvoc_company__c());
        bigPAccEnt.setSvoc_flowntrips__c(accObj.getSvoc_flowntrips__c());
        bigPAccEnt.setSvoc_rfmtotal__c(accObj.getSvoc_rfmtotal__c());
        bigPAccEnt.setSvoc_winbackpropensity__c(accObj.getSvoc_winbackpropensity__c());
        bigPAccEnt.setSvoc_nps__c(accObj.getSvoc_nps__c());
        bigPAccEnt.setSvoc_language__c(accObj.getSvoc_language__c());
        bigPAccEnt.setSvoc_gendercode__c(accObj.getSvoc_gendercode__c());
        bigPAccEnt.setSvoc_dominantdestination__c(accObj.getSvoc_dominantdestination__c());
        bigPAccEnt.setSvoc_customercategory__c(accObj.getSvoc_customercategory__c());
        bigPAccEnt.setSvoc_nationality__c(accObj.getSvoc_nationality__c());
        bigPAccEnt.setSvoc_country__c(accObj.getSvoc_country__c());
        bigPAccEnt.setPersonbirthdate(accObj.getPersonbirthdate());
        bigPAccEnt.setSvoc_countrycodemobile__c(accObj.getSvoc_countrycodemobile__c());
        bigPAccEnt.setPersonmobilephone(accObj.getPersonmobilephone());
        bigPAccEnt.setSvoc_optin_mobile__c(accObj.getSvoc_optin_mobile__c());
        bigPAccEnt.setPersonemail(accObj.getPersonemail());
        bigPAccEnt.setSvoc_optin_email__c(accObj.getSvoc_optin_email__c());
        bigPAccEnt.setSvoc_countrycode__c(accObj.getSvoc_countrycode__c());
        bigPAccEnt.setPhone(accObj.getPhone());
        bigPAccEnt.setSvoc_optin_email2__c(accObj.getSvoc_optin_email2__c());//
        bigPAccEnt.setSvoc_officecountrycode__c(accObj.getSvoc_officecountrycode__c());
        bigPAccEnt.setSvoc_officephone__c(accObj.getSvoc_officephone__c());
        bigPAccEnt.setSvoc_extension__c(accObj.getSvoc_extension__c());
        bigPAccEnt.setSvoc_email2__c(accObj.getSvoc_email2__c());
        bigPAccEnt.setSvoc_email3__c(accObj.getSvoc_email3__c());
        bigPAccEnt.setSvoc_optincompanyemail__c(accObj.getSvoc_optincompanyemail__c());
        bigPAccEnt.setSvoc_passport_number__c(accObj.getSvoc_passport_number__c());
        bigPAccEnt.setSvoc_city__c(accObj.getSvoc_city__c());
//        bigPAccEnt.setPersonassistantname(accObj.getPersonassistantname());
//        bigPAccEnt.setSvoc_countrycodeassistant__c(accObj.getSvoc_countrycodeassistant__c());
//        bigPAccEnt.setPersonassistantphone(accObj.getPersonassistantphone());
//        bigPAccEnt.setSvoc_assistantemail__c(accObj.getSvoc_assistantemail__c());
        bigPAccEnt.setPersonleadsource(accObj.getPersonleadsource());
        bigPAccEnt.setSalutation(accObj.getSalutation());
//        bigPAccEnt.setSvoc_employeenumber__c(accObj.getSvoc_employeenumber__c());
        bigPAccEnt.setSvoc_contactemergencyname__c(accObj.getSvoc_contactemergencyname__c());
        bigPAccEnt.setSvoc_emergencycountrycode__c(accObj.getSvoc_emergencycountrycode__c());
        bigPAccEnt.setSvoc_emergencyphone__c(accObj.getSvoc_emergencyphone__c());
        bigPAccEnt.setSvoc_emergencyemail__c(accObj.getSvoc_emergencyemail__c());
        bigPAccEnt.setSvoc_relationship__c(accObj.getSvoc_relationship__c());
//        bigPAccEnt.setPersontitle(accObj.getPersontitle());
//        bigPAccEnt.setSvoc_ismigrated__c(accObj.getSvoc_ismigrated__c());
        bigPAccEnt.setSvoc_paexternalid__c(accObj.getSvoc_paexternalid__c());
        bigPAccEnt.setRecordtypeid(accObj.getRecordtypeid());
        bigPAccEnt.setSvoc_purchasepropensity__c(accObj.getSvoc_purchasepropensity__c());
        bigPAccEnt.setSvoc_rfm_tier__c(accObj.getSvoc_rfm_tier__c());
        bigPAccEnt.setSvoc_dominantorigin__c(accObj.getSvoc_dominantorigin__c());
        bigPAccEnt.setDml_type(dml_type);
        bigPAccEnt.setSvoc_frequent_flyer_number__c(accObj.getSvoc_frequent_flyer_number__c());
        return bigPAccEnt;
    }
    private BigCobrandEntity convertBigCobrand(Cobrand cobrand,String dml_type) {
    	BigCobrandEntity bigCbrEnt = new BigCobrandEntity();
        bigCbrEnt.setId(cobrand.getId());
        bigCbrEnt.setSfid(cobrand.getSfid());
        bigCbrEnt.setName(cobrand.getName());
        bigCbrEnt.setSvoc_cuenta__c(cobrand.getSvoc_cuenta__c());
        bigCbrEnt.setSvoc_cardnumber__c(cobrand.getSvoc_cardnumber__c());
        bigCbrEnt.setSvoc_bank__c(cobrand.getSvoc_bank__c());
        bigCbrEnt.setSvoc_customerid__c(cobrand.getSvoc_customerid__c());
        bigCbrEnt.setSvoc_cobrandexternalid__c(cobrand.getSvoc_cobrandexternalid__c());
        bigCbrEnt.setSvoc_cardtier__c(cobrand.getSvoc_cardtier__c());
        bigCbrEnt.setSvoc_membersince__c(cobrand.getSvoc_membersince__c());
        bigCbrEnt.setSvoc_expirationdate__c(cobrand.getSvoc_expirationdate__c());
//        bigCbrEnt.setSvoc_ismigrated__c(cobrand.getSvoc_ismigrated__c());
        bigCbrEnt.setDml_type(dml_type);
        return bigCbrEnt;
    }
    
    private BigFrequentFlyerEntity convertBigFF(FrequentFlyer ff,String dml_type) {
    	BigFrequentFlyerEntity bigFfEntity = new BigFrequentFlyerEntity();
        bigFfEntity.setSfid(ff.getSfid());
        bigFfEntity.setName(ff.getName());
        bigFfEntity.setSvoc_cuenta__c(ff.getSvoc_cuenta__c());
        bigFfEntity.setSvoc_customerid__c(ff.getSvoc_customerid__c());
        bigFfEntity.setSvoc_ffn_external_id__c(ff.getSvoc_ffn_external_id__c());
        bigFfEntity.setSvoc_frequentflyertier__c(ff.getSvoc_frequentflyertier__c());
        bigFfEntity.setSvoc_frequentflyerprogram__c(ff.getSvoc_frequentflyerprogram__c());
        bigFfEntity.setSvoc_pcemail__c(ff.getSvoc_pcemail__c());
        bigFfEntity.setDml_type(dml_type);
        return bigFfEntity;
    }
}
