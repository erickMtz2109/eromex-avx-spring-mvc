package com.aeromex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.aeromex.dao.ErrorLogDao;
import com.aeromex.entity.SvocLogEntity;

@Service
public class ErrorLogServiceImpl implements ErrorLogService{
	@Autowired
	ErrorLogDao errorLogDao;
	
	@Async
	public void insertErrorService(List<SvocLogEntity> logErrorLts, String folio) {
		errorLogDao.insertErrorFromSvoc(logErrorLts, folio);
	}	
}
