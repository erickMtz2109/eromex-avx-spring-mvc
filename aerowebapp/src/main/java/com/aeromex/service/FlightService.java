package com.aeromex.service;

import java.util.List;
import com.aeromex.model.Flight;

public interface FlightService {
	List<Flight> getAllFlights();
}
