package com.aeromex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aeromex.dao.PNR_AttributeDao;

@Service
public class PNR_AttributeServiceImpl implements PNR_AttributeService{
	
	@Autowired
	PNR_AttributeDao pnr_Att;
	
	public void callMethod() {
		pnr_Att.mapAttribute();
	}

}
