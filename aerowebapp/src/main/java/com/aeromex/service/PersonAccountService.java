package com.aeromex.service;

import java.util.Date;
import java.util.List;

import com.aeromex.entity.PersonAccountEntity;
import com.aeromex.entity.RegresoAltasAnalitica;
import com.aeromex.model.FindAccount;
import com.aeromex.model.PersonAccount;

public interface PersonAccountService {
	public List<PersonAccount> getAllPersonAccounts();

//	public List<PersonAccount> createPersonAccounts(List<PersonAccount> personAccLts);
//	PersonAccount getSpecificPersonAccount(String id_sfdc);
	List<PersonAccount> updatePersonAccounts(List<PersonAccount> personAccLts);
	List<PersonAccountEntity> getDeltas(Date fecha);
	List<PersonAccountEntity> getDeltas();
	List<FindAccount> findAccounts(List<FindAccount> fAccLts);
	List<RegresoAltasAnalitica> getAltasAnalitica();
}
