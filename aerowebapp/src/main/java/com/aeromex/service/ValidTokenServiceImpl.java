package com.aeromex.service;

import java.net.URI;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ValidTokenServiceImpl implements ValidTokenService {

	public boolean validSalesforceToken(String token) {
		if (token == null || token.isEmpty()) {
			return false;
		}
		try {
			String url = System.getenv("URL_VALIDATE_TOKEN");
			System.out.println("URL: " + url);

			RestTemplate restTemplate = new RestTemplate();
			URI uri = new URI(url);

			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", token);

			HttpEntity<String> request = new HttpEntity<>("", headers);
			ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);

			System.out.println("result.getStatusCodeValue(): " + result.getStatusCodeValue());

			boolean valid = false;
			valid = (200 == result.getStatusCodeValue());

			return valid;
		} catch (Exception ex) {
			System.out.println("Error call valid token msn: " + ex.getMessage());
		}
		return false;
	}
}
