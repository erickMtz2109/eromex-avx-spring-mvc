package com.aeromex.service;

public interface ValidTokenService {
	 public boolean validSalesforceToken(String token);
}
