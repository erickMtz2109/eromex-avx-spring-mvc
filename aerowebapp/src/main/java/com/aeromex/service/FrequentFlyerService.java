package com.aeromex.service;

import java.util.List;
import com.aeromex.model.FrequentFlyer;

public interface FrequentFlyerService {
	List<FrequentFlyer> getAllFrequentFlyers();
	List<FrequentFlyer> createffs(List<FrequentFlyer> cobrandLts);
	List<FrequentFlyer> updateffs(List<FrequentFlyer> cobrandLts);
}
