package com.aeromex.service;

import java.util.List;
import com.aeromex.model.Passenger;

public interface PassengerService{
	public List<Passenger> getAllPassengers();
}
