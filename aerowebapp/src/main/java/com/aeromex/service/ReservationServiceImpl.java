package com.aeromex.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aeromex.entity.ReservationEntity;
import com.aeromex.dao.ReservationDao;
import com.aeromex.model.Reservation;
import org.modelmapper.ModelMapper;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService{
	
	@Autowired
	ReservationDao reservationDao;

	public List<Reservation> getAllReservations() {
	  return listReservationsToModel(reservationDao.reservationList());
	}
	public static Reservation ReservationToModel(ReservationEntity ReservationEntity) {
	  ModelMapper modelMapper = new ModelMapper();
	  Reservation Reservations = modelMapper.map(ReservationEntity, Reservation.class);
	  return Reservations;
	}
	public static List<Reservation> listReservationsToModel(List<ReservationEntity> allReservationEntities) {
	  List<Reservation> lstResult = new ArrayList<Reservation>();
	  for (ReservationEntity obj : allReservationEntities) {
	    lstResult.add(ReservationToModel(obj));
	  }
	  return lstResult;
	}
}
