package com.aeromex.service;

import com.aeromex.entity.PersonAccountEntity;
import com.aeromex.entity.SvocLogEntity;
import java.util.List;

import com.aeromex.model.PersonAccount;

public interface DeltaPersonAccountService {
	List<SvocLogEntity> getErrors(String folio);
	List<PersonAccountEntity> getCreatedDeltas();
	List<PersonAccountEntity> getUpdatedDeltas();
	void deleteAccountDeltas(List<PersonAccountEntity> personToDelete);
	public void bulkDeltas(List<PersonAccount> personAccounts, boolean cOrUAcc, String hilo,String folio);
//	void bulkBigDeltas(List<PersonAccount> personAccounts, boolean cOrUAcc, String hilo);
//	void bulkBigDeltas(List<PersonAccount> personAccounts, String hilo);
	void bulkBigDeltas(List<PersonAccount> personAccounts, String hilo, String dml_type);
}
