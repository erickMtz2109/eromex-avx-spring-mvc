package com.aeromex.service;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aeromex.dao.FrequentFlyerDao;
import com.aeromex.entity.FrequentFlyerEntity;
import com.aeromex.model.FrequentFlyer;

@Service
public class FrequentFlyerServiceImpl implements FrequentFlyerService{
	@Autowired
	FrequentFlyerDao frequentFlyerDao;

	public List<FrequentFlyer> getAllFrequentFlyers(){
		return listEntityToModel(frequentFlyerDao.frequentFlyerList());
	}
	public List<FrequentFlyer> createffs(List<FrequentFlyer> cobrandLts) {
		List<FrequentFlyerEntity> result = frequentFlyerDao.createFrequentFlyers(modelToFFEntity(cobrandLts));
		return listEntityToModel(result);
	}
	public List<FrequentFlyer> updateffs(List<FrequentFlyer> cobrandLts) {
		List<FrequentFlyerEntity> result = frequentFlyerDao.updateFrequentFlyers(modelToFFEntity(cobrandLts));
		return listEntityToModel(result);
	}
	
	
	

	private FrequentFlyer entityToModel(FrequentFlyerEntity FrequentFlyerEntity) {
		ModelMapper modelMapper = new ModelMapper();
		FrequentFlyer PersonAccount = modelMapper.map(FrequentFlyerEntity, FrequentFlyer.class);
		return PersonAccount;
	}

	private List<FrequentFlyer> listEntityToModel(List<FrequentFlyerEntity> allFrequentFlyerEntities) {
		List<FrequentFlyer> lstResult = new ArrayList<FrequentFlyer>();
		for (FrequentFlyerEntity obj : allFrequentFlyerEntities) {
			lstResult.add(entityToModel(obj));
		}
		return lstResult;
	}


	private List<FrequentFlyerEntity> modelToFFEntity(List<FrequentFlyer> ffLts) {
		List<FrequentFlyerEntity> lstResult = new ArrayList<FrequentFlyerEntity>();
		for (FrequentFlyer objFF : ffLts) {
			lstResult.add(modelToFrequentFlyerEntity(objFF));
		}
		return lstResult;
	}
	public static FrequentFlyerEntity modelToFrequentFlyerEntity(FrequentFlyer ffObj) {
		ModelMapper modelMapper = new ModelMapper();
		FrequentFlyerEntity ffs = modelMapper.map(ffObj, FrequentFlyerEntity.class);
		return ffs;
	}
}
