package com.aeromex.service;

import java.util.List;
import com.aeromex.model.Aircraft;

public interface AircraftService {
	List<Aircraft> getAllAircrafts();
}
