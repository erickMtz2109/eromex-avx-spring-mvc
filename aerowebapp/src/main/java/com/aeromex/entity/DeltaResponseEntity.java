package com.aeromex.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "account", schema = "salesforce")
public class DeltaResponseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(insertable = false, updatable = false)
	private String sfid;
	@Id
	@Column(name = "svoc_customerid__c", unique = true, nullable = false)
	private String svoc_customerid__c;
	private String firstname;
	private String middlename;
	private String lastname;
	private String suffix;
	public String getSfid() {
		return sfid;
	}
	public void setSfid(String sfid) {
		this.sfid = sfid;
	}
	public String getSvoc_customerid__c() {
		return svoc_customerid__c;
	}
	public void setSvoc_customerid__c(String svoc_customerid__c) {
		this.svoc_customerid__c = svoc_customerid__c;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	@Override
	public String toString() {
		return "DeltaResponse [sfid=" + sfid + ", svoc_customerid__c=" + svoc_customerid__c + ", firstname=" + firstname
				+ ", middlename=" + middlename + ", lastname=" + lastname + ", suffix=" + suffix + "]";
	}
}
