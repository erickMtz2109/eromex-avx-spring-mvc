package com.aeromex.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "svoc_aircraft__c", schema = "salesforce")
public class AircraftEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	private String sfid;
	private String createdbyid;
	private Timestamp createddate;
	private Boolean isdeleted;
	private String lastmodifiedbyid;
	private Timestamp lastmodifieddate;
	private String name;
	private String ownerid;
	private Boolean svoc_active__c;
	private String svoc_code__c;
	private String svoc_externalid__c;
	private String svoc_fleet__c;
	private String svoc_iata_code__c;
	private String svoc_icao_code__c;
	private String svoc_operator__c;
	private Double svoc_physical_capacity__c;
	private String svoc_subfleet__c;
	private String svoc_tail_number__c;
	private Timestamp systemmodstamp;
	private String _hc_lastop;
	private String _hc_err;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSfid() {
		return sfid;
	}
	public void setSfid(String sfid) {
		this.sfid = sfid;
	}
	public String getCreatedbyid() {
		return createdbyid;
	}
	public void setCreatedbyid(String createdbyid) {
		this.createdbyid = createdbyid;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Boolean getIsdeleted() {
		return isdeleted;
	}
	public void setIsdeleted(Boolean isdeleted) {
		this.isdeleted = isdeleted;
	}
	public String getLastmodifiedbyid() {
		return lastmodifiedbyid;
	}
	public void setLastmodifiedbyid(String lastmodifiedbyid) {
		this.lastmodifiedbyid = lastmodifiedbyid;
	}
	public Timestamp getLastmodifieddate() {
		return lastmodifieddate;
	}
	public void setLastmodifieddate(Timestamp lastmodifieddate) {
		this.lastmodifieddate = lastmodifieddate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOwnerid() {
		return ownerid;
	}
	public void setOwnerid(String ownerid) {
		this.ownerid = ownerid;
	}
	public Boolean getSvoc_active__c() {
		return svoc_active__c;
	}
	public void setSvoc_active__c(Boolean svoc_active__c) {
		this.svoc_active__c = svoc_active__c;
	}
	public String getSvoc_code__c() {
		return svoc_code__c;
	}
	public void setSvoc_code__c(String svoc_code__c) {
		this.svoc_code__c = svoc_code__c;
	}
	public String getSvoc_externalid__c() {
		return svoc_externalid__c;
	}
	public void setSvoc_externalid__c(String svoc_externalid__c) {
		this.svoc_externalid__c = svoc_externalid__c;
	}
	public String getSvoc_fleet__c() {
		return svoc_fleet__c;
	}
	public void setSvoc_fleet__c(String svoc_fleet__c) {
		this.svoc_fleet__c = svoc_fleet__c;
	}
	public String getSvoc_iata_code__c() {
		return svoc_iata_code__c;
	}
	public void setSvoc_iata_code__c(String svoc_iata_code__c) {
		this.svoc_iata_code__c = svoc_iata_code__c;
	}
	public String getSvoc_icao_code__c() {
		return svoc_icao_code__c;
	}
	public void setSvoc_icao_code__c(String svoc_icao_code__c) {
		this.svoc_icao_code__c = svoc_icao_code__c;
	}
	public String getSvoc_operator__c() {
		return svoc_operator__c;
	}
	public void setSvoc_operator__c(String svoc_operator__c) {
		this.svoc_operator__c = svoc_operator__c;
	}
	public Double getSvoc_physical_capacity__c() {
		return svoc_physical_capacity__c;
	}
	public void setSvoc_physical_capacity__c(Double svoc_physical_capacity__c) {
		this.svoc_physical_capacity__c = svoc_physical_capacity__c;
	}
	public String getSvoc_subfleet__c() {
		return svoc_subfleet__c;
	}
	public void setSvoc_subfleet__c(String svoc_subfleet__c) {
		this.svoc_subfleet__c = svoc_subfleet__c;
	}
	public String getSvoc_tail_number__c() {
		return svoc_tail_number__c;
	}
	public void setSvoc_tail_number__c(String svoc_tail_number__c) {
		this.svoc_tail_number__c = svoc_tail_number__c;
	}
	public Timestamp getSystemmodstamp() {
		return systemmodstamp;
	}
	public void setSystemmodstamp(Timestamp systemmodstamp) {
		this.systemmodstamp = systemmodstamp;
	}
	public String get_hc_lastop() {
		return _hc_lastop;
	}
	public void set_hc_lastop(String _hc_lastop) {
		this._hc_lastop = _hc_lastop;
	}
	public String get_hc_err() {
		return _hc_err;
	}
	public void set_hc_err(String _hc_err) {
		this._hc_err = _hc_err;
	}
	
	@Override
	public String toString() {
		return "AircraftEntity [id=" + id + ", sfid=" + sfid + ", createdbyid=" + createdbyid + ", createddate="
				+ createddate + ", isdeleted=" + isdeleted + ", lastmodifiedbyid=" + lastmodifiedbyid
				+ ", lastmodifieddate=" + lastmodifieddate + ", name=" + name + ", ownerid=" + ownerid
				+ ", svoc_active__c=" + svoc_active__c + ", svoc_code__c=" + svoc_code__c + ", svoc_externalid__c="
				+ svoc_externalid__c + ", svoc_fleet__c=" + svoc_fleet__c + ", svoc_iata_code__c=" + svoc_iata_code__c
				+ ", svoc_icao_code__c=" + svoc_icao_code__c + ", svoc_operator__c=" + svoc_operator__c
				+ ", svoc_physical_capacity__c=" + svoc_physical_capacity__c + ", svoc_subfleet__c=" + svoc_subfleet__c
				+ ", svoc_tail_number__c=" + svoc_tail_number__c + ", systemmodstamp=" + systemmodstamp
				+ ", _hc_lastop=" + _hc_lastop + ", _hc_err=" + _hc_err + "]";
	}
}
