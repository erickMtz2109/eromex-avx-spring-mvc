package com.aeromex.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "frequentflyernumber", schema = "sync_svoc")
public class BigFrequentFlyerEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(insertable = false, updatable = false)
	private Integer id;
	@Transient
	private String sfid;
	private String name;
	@Transient
	private String svoc_cuenta__c;
	private String svoc_customerid__c;
	@Id
	@Column(name = "svoc_ffn_external_id__c", unique = true, nullable = false)
	private String svoc_ffn_external_id__c;
	private String svoc_frequentflyertier__c;
	private String svoc_frequentflyerprogram__c;
	private String Svoc_pcemail__c;
	private Timestamp CreatedDate;
	private Timestamp LastModifiedDate;
	private String dml_type;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSfid() {
		return sfid;
	}
	public void setSfid(String sfid) {
		this.sfid = sfid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSvoc_cuenta__c() {
		return svoc_cuenta__c;
	}
	public void setSvoc_cuenta__c(String svoc_cuenta__c) {
		this.svoc_cuenta__c = svoc_cuenta__c;
	}
	public String getSvoc_customerid__c() {
		return svoc_customerid__c;
	}
	public void setSvoc_customerid__c(String svoc_customerid__c) {
		this.svoc_customerid__c = svoc_customerid__c;
	}
	public String getSvoc_ffn_external_id__c() {
		return svoc_ffn_external_id__c;
	}
	public void setSvoc_ffn_external_id__c(String svoc_ffn_external_id__c) {
		this.svoc_ffn_external_id__c = svoc_ffn_external_id__c;
	}
	public String getSvoc_frequentflyertier__c() {
		return svoc_frequentflyertier__c;
	}
	public void setSvoc_frequentflyertier__c(String svoc_frequentflyertier__c) {
		this.svoc_frequentflyertier__c = svoc_frequentflyertier__c;
	}
	public String getSvoc_frequentflyerprogram__c() {
		return svoc_frequentflyerprogram__c;
	}
	public void setSvoc_frequentflyerprogram__c(String svoc_frequentflyerprogram__c) {
		this.svoc_frequentflyerprogram__c = svoc_frequentflyerprogram__c;
	}
	public String getSvoc_pcemail__c() {
		return Svoc_pcemail__c;
	}
	public void setSvoc_pcemail__c(String svoc_pcemail__c) {
		Svoc_pcemail__c = svoc_pcemail__c;
	}
	public Timestamp getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		CreatedDate = createdDate;
	}
	public Timestamp getLastModifiedDate() {
		return LastModifiedDate;
	}
	public void setLastModifiedDate(Timestamp lastModifiedDate) {
		LastModifiedDate = lastModifiedDate;
	}
	public String getDml_type() {
		return dml_type;
	}
	public void setDml_type(String dml_type) {
		this.dml_type = dml_type;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "BigFrequentFlyerEntity [id=" + id + ", sfid=" + sfid + ", name=" + name + ", svoc_cuenta__c="
				+ svoc_cuenta__c + ", svoc_customerid__c=" + svoc_customerid__c + ", svoc_ffn_external_id__c="
				+ svoc_ffn_external_id__c + ", svoc_frequentflyertier__c=" + svoc_frequentflyertier__c
				+ ", svoc_frequentflyerprogram__c=" + svoc_frequentflyerprogram__c + ", Svoc_pcemail__c="
				+ Svoc_pcemail__c + ", CreatedDate=" + CreatedDate + ", LastModifiedDate=" + LastModifiedDate
				+ ", dml_type=" + dml_type + "]";
	}
}
