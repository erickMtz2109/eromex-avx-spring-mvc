package com.aeromex.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;

@Entity
@Table(name = "svoc_reservation__c")
public class ReservationEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	private String sfdc_id;
	private String svoc_account__c;
	private String svoc_pnr_id__c;
	private Date svoc_pnrcreatedate__c;
	private String svoc_flighttype__c;
	@Column(columnDefinition = "numeric")
	private Integer svoc_numberinparty__c;
	@Column(columnDefinition = "numeric")
	private Double svoc_numberofadults__c;
	@Column(columnDefinition = "numeric")
	private Integer svoc_numberofinfants__c;
	@Column(columnDefinition = "numeric")
	private Integer svoc_numberofminors__c;
	private String svoc_country__c;
	private String svoc_saleschannel__c;
	private String svoc_phone__c;
	private String svoc_email__c;
	@Column(columnDefinition = "numeric")
	private Double svoc_totalamount__c;
	private String svoc_currency__c;
	private String svoc_status__c;
	private String svoc_timelimit__c;
	private String svoc_spid__c;
	private String svoc_group_name__c;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSfdc_id() {
		return sfdc_id;
	}
	public void setSfdc_id(String sfdc_id) {
		this.sfdc_id = sfdc_id;
	}
	public String getSvoc_account__c() {
		return svoc_account__c;
	}
	public void setSvoc_account__c(String svoc_account__c) {
		this.svoc_account__c = svoc_account__c;
	}
	public String getSvoc_pnr_id__c() {
		return svoc_pnr_id__c;
	}
	public void setSvoc_pnr_id__c(String svoc_pnr_id__c) {
		this.svoc_pnr_id__c = svoc_pnr_id__c;
	}
	public Date getSvoc_pnrcreatedate__c() {
		return svoc_pnrcreatedate__c;
	}
	public void setSvoc_pnrcreatedate__c(Date svoc_pnrcreatedate__c) {
		this.svoc_pnrcreatedate__c = svoc_pnrcreatedate__c;
	}
	public String getSvoc_flighttype__c() {
		return svoc_flighttype__c;
	}
	public void setSvoc_flighttype__c(String svoc_flighttype__c) {
		this.svoc_flighttype__c = svoc_flighttype__c;
	}
	public Integer getSvoc_numberinparty__c() {
		return svoc_numberinparty__c;
	}
	public void setSvoc_numberinparty__c(Integer svoc_numberinparty__c) {
		this.svoc_numberinparty__c = svoc_numberinparty__c;
	}
	public Double getSvoc_numberofadults__c() {
		return svoc_numberofadults__c;
	}
	public void setSvoc_numberofadults__c(Double svoc_numberofadults__c) {
		this.svoc_numberofadults__c = svoc_numberofadults__c;
	}
	public Integer getSvoc_numberofinfants__c() {
		return svoc_numberofinfants__c;
	}
	public void setSvoc_numberofinfants__c(Integer svoc_numberofinfants__c) {
		this.svoc_numberofinfants__c = svoc_numberofinfants__c;
	}
	public Integer getSvoc_numberofminors__c() {
		return svoc_numberofminors__c;
	}
	public void setSvoc_numberofminors__c(Integer svoc_numberofminors__c) {
		this.svoc_numberofminors__c = svoc_numberofminors__c;
	}
	public String getSvoc_country__c() {
		return svoc_country__c;
	}
	public void setSvoc_country__c(String svoc_country__c) {
		this.svoc_country__c = svoc_country__c;
	}
	public String getSvoc_saleschannel__c() {
		return svoc_saleschannel__c;
	}
	public void setSvoc_saleschannel__c(String svoc_saleschannel__c) {
		this.svoc_saleschannel__c = svoc_saleschannel__c;
	}
	public String getSvoc_phone__c() {
		return svoc_phone__c;
	}
	public void setSvoc_phone__c(String svoc_phone__c) {
		this.svoc_phone__c = svoc_phone__c;
	}
	public String getSvoc_email__c() {
		return svoc_email__c;
	}
	public void setSvoc_email__c(String svoc_email__c) {
		this.svoc_email__c = svoc_email__c;
	}
	public Double getSvoc_totalamount__c() {
		return svoc_totalamount__c;
	}
	public void setSvoc_totalamount__c(Double svoc_totalamount__c) {
		this.svoc_totalamount__c = svoc_totalamount__c;
	}
	public String getSvoc_currency__c() {
		return svoc_currency__c;
	}
	public void setSvoc_currency__c(String svoc_currency__c) {
		this.svoc_currency__c = svoc_currency__c;
	}
	public String getSvoc_status__c() {
		return svoc_status__c;
	}
	public void setSvoc_status__c(String svoc_status__c) {
		this.svoc_status__c = svoc_status__c;
	}
	public String getSvoc_timelimit__c() {
		return svoc_timelimit__c;
	}
	public void setSvoc_timelimit__c(String svoc_timelimit__c) {
		this.svoc_timelimit__c = svoc_timelimit__c;
	}
	public String getSvoc_spid__c() {
		return svoc_spid__c;
	}
	public void setSvoc_spid__c(String svoc_spid__c) {
		this.svoc_spid__c = svoc_spid__c;
	}
	public String getSvoc_group_name__c() {
		return svoc_group_name__c;
	}
	public void setSvoc_group_name__c(String svoc_group_name__c) {
		this.svoc_group_name__c = svoc_group_name__c;
	}
	@Override
	public String toString() {
		return "ReservationEntity [id=" + id + ", sfdc_id=" + sfdc_id + ", svoc_account__c=" + svoc_account__c
				+ ", svoc_pnr_id__c=" + svoc_pnr_id__c + ", svoc_pnrcreatedate__c=" + svoc_pnrcreatedate__c
				+ ", svoc_flighttype__c=" + svoc_flighttype__c + ", svoc_numberinparty__c=" + svoc_numberinparty__c
				+ ", svoc_numberofadults__c=" + svoc_numberofadults__c + ", svoc_numberofinfants__c="
				+ svoc_numberofinfants__c + ", svoc_numberofminors__c=" + svoc_numberofminors__c + ", svoc_country__c="
				+ svoc_country__c + ", svoc_saleschannel__c=" + svoc_saleschannel__c + ", svoc_phone__c="
				+ svoc_phone__c + ", svoc_email__c=" + svoc_email__c + ", svoc_totalamount__c=" + svoc_totalamount__c
				+ ", svoc_currency__c=" + svoc_currency__c + ", svoc_status__c=" + svoc_status__c
				+ ", svoc_timelimit__c=" + svoc_timelimit__c + ", svoc_spid__c=" + svoc_spid__c
				+ ", svoc_group_name__c=" + svoc_group_name__c + "]";
	}
}
