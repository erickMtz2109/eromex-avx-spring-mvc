package com.aeromex.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "svoc_passenger__c")
public class PassengerEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	private String passenger_id;
	private String name;
	private String svoc_passengetype__c;
	private String svoc_reservation__c;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPassenger_id() {
		return passenger_id;
	}
	public void setPassenger_id(String passenger_id) {
		this.passenger_id = passenger_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSvoc_passengetype__c() {
		return svoc_passengetype__c;
	}
	public void setSvoc_passengetype__c(String svoc_passengetype__c) {
		this.svoc_passengetype__c = svoc_passengetype__c;
	}
	public String getSvoc_reservation__c() {
		return svoc_reservation__c;
	}
	public void setSvoc_reservation__c(String svoc_reservation__c) {
		this.svoc_reservation__c = svoc_reservation__c;
	}
	@Override
	public String toString() {
		return "PassengerEntity [id=" + id + ", passenger_id=" + passenger_id + ", name=" + name
				+ ", svoc_passengetype__c=" + svoc_passengetype__c + ", svoc_reservation__c=" + svoc_reservation__c
				+ "]";
	}	
}
