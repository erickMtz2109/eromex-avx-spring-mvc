package com.aeromex.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "account", schema = "salesforce")
public class PersonAccountEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(insertable = false, updatable = false)
	private Long id;
	private String sfid;
	@Id
	@Column(name = "svoc_customerid__c", unique = true, nullable = false)
	private String svoc_customerid__c;
//	private String svoc_reservation__c;
	@Transient
    private String name;
	private String firstname;
	private String middlename;
	private String lastname;
	private String suffix;
	private String svoc_gaid__c;
	private String svoc_spid__c;
	private String svoc_company__c;
	@Column(columnDefinition = "numeric")
	private Integer svoc_flowntrips__c;
	@Column(columnDefinition = "numeric")
	private Integer svoc_rfmtotal__c;
	private String svoc_rfm_tier__c;
	private String svoc_nps__c;
	private String svoc_language__c;
	private String svoc_gendercode__c;
	private String svoc_dominantdestination__c;
	private String svoc_customercategory__c;
	private String svoc_nationality__c;
	private String svoc_country__c;
	private Date personbirthdate;
	private String svoc_countrycodemobile__c;
	private String personmobilephone;
	private Boolean svoc_optin_mobile__c;
	private String personemail;
	private Boolean svoc_optin_email__c;
	private String svoc_countrycode__c;
	private String phone;
	private String svoc_email2__c;
	private Boolean svoc_optin_email2__c;
	private String svoc_officecountrycode__c;
	private String svoc_officephone__c;
	private String svoc_extension__c;
	private String svoc_email3__c;
	private Boolean svoc_optincompanyemail__c;
	private String svoc_passport_number__c;
	private String svoc_city__c;
	private String personleadsource;
	private String salutation;
	private String svoc_contactemergencyname__c;
	private String svoc_emergencycountrycode__c;
	private String svoc_emergencyphone__c;
	private String svoc_emergencyemail__c;
	private String svoc_relationship__c;
	private String svoc_paexternalid__c;
	private String recordtypeid;
	private String svoc_purchasepropensity__c;
	private String svoc_winbackpropensity__c;
	private String svoc_dominantorigin__c;
	private Timestamp lastmodifieddate;
	private Timestamp createddate;
	private Timestamp SystemModstamp;
	private String _hc_err;
	private String _hc_lastop;
	private Boolean svoc_isactive__c;
//	SVOC_BirthdayDateText__c
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSfid() {
		return sfid;
	}
	public void setSfid(String sfid) {
		this.sfid = sfid;
	}
	public String getSvoc_customerid__c() {
		return svoc_customerid__c;
	}
	public void setSvoc_customerid__c(String svoc_customerid__c) {
		this.svoc_customerid__c = svoc_customerid__c;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getSvoc_gaid__c() {
		return svoc_gaid__c;
	}
	public void setSvoc_gaid__c(String svoc_gaid__c) {
		this.svoc_gaid__c = svoc_gaid__c;
	}
	public String getSvoc_spid__c() {
		return svoc_spid__c;
	}
	public void setSvoc_spid__c(String svoc_spid__c) {
		this.svoc_spid__c = svoc_spid__c;
	}
	public String getSvoc_company__c() {
		return svoc_company__c;
	}
	public void setSvoc_company__c(String svoc_company__c) {
		this.svoc_company__c = svoc_company__c;
	}
	public Integer getSvoc_flowntrips__c() {
		return svoc_flowntrips__c;
	}
	public void setSvoc_flowntrips__c(Integer svoc_flowntrips__c) {
		this.svoc_flowntrips__c = svoc_flowntrips__c;
	}
	public Integer getSvoc_rfmtotal__c() {
		return svoc_rfmtotal__c;
	}
	public void setSvoc_rfmtotal__c(Integer svoc_rfmtotal__c) {
		this.svoc_rfmtotal__c = svoc_rfmtotal__c;
	}
	public String getSvoc_rfm_tier__c() {
		return svoc_rfm_tier__c;
	}
	public void setSvoc_rfm_tier__c(String svoc_rfm_tier__c) {
		this.svoc_rfm_tier__c = svoc_rfm_tier__c;
	}
	public String getSvoc_nps__c() {
		return svoc_nps__c;
	}
	public void setSvoc_nps__c(String svoc_nps__c) {
		this.svoc_nps__c = svoc_nps__c;
	}
	public String getSvoc_language__c() {
		return svoc_language__c;
	}
	public void setSvoc_language__c(String svoc_language__c) {
		this.svoc_language__c = svoc_language__c;
	}
	public String getSvoc_gendercode__c() {
		return svoc_gendercode__c;
	}
	public void setSvoc_gendercode__c(String svoc_gendercode__c) {
		this.svoc_gendercode__c = svoc_gendercode__c;
	}
	public String getSvoc_dominantdestination__c() {
		return svoc_dominantdestination__c;
	}
	public void setSvoc_dominantdestination__c(String svoc_dominantdestination__c) {
		this.svoc_dominantdestination__c = svoc_dominantdestination__c;
	}
	public String getSvoc_customercategory__c() {
		return svoc_customercategory__c;
	}
	public void setSvoc_customercategory__c(String svoc_customercategory__c) {
		this.svoc_customercategory__c = svoc_customercategory__c;
	}
	public String getSvoc_nationality__c() {
		return svoc_nationality__c;
	}
	public void setSvoc_nationality__c(String svoc_nationality__c) {
		this.svoc_nationality__c = svoc_nationality__c;
	}
	public String getSvoc_country__c() {
		return svoc_country__c;
	}
	public void setSvoc_country__c(String svoc_country__c) {
		this.svoc_country__c = svoc_country__c;
	}
	public Date getPersonbirthdate() {
		return personbirthdate;
	}
	public void setPersonbirthdate(Date personbirthdate) {
		this.personbirthdate = personbirthdate;
	}
	public String getSvoc_countrycodemobile__c() {
		return svoc_countrycodemobile__c;
	}
	public void setSvoc_countrycodemobile__c(String svoc_countrycodemobile__c) {
		this.svoc_countrycodemobile__c = svoc_countrycodemobile__c;
	}
	public String getPersonmobilephone() {
		return personmobilephone;
	}
	public void setPersonmobilephone(String personmobilephone) {
		this.personmobilephone = personmobilephone;
	}
	public Boolean getSvoc_optin_mobile__c() {
		return svoc_optin_mobile__c;
	}
	public void setSvoc_optin_mobile__c(Boolean svoc_optin_mobile__c) {
		this.svoc_optin_mobile__c = svoc_optin_mobile__c;
	}
	public String getPersonemail() {
		return personemail;
	}
	public void setPersonemail(String personemail) {
		this.personemail = personemail;
	}
	public Boolean getSvoc_optin_email__c() {
		return svoc_optin_email__c;
	}
	public void setSvoc_optin_email__c(Boolean svoc_optin_email__c) {
		this.svoc_optin_email__c = svoc_optin_email__c;
	}
	public String getSvoc_countrycode__c() {
		return svoc_countrycode__c;
	}
	public void setSvoc_countrycode__c(String svoc_countrycode__c) {
		this.svoc_countrycode__c = svoc_countrycode__c;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getSvoc_email2__c() {
		return svoc_email2__c;
	}
	public void setSvoc_email2__c(String svoc_email2__c) {
		this.svoc_email2__c = svoc_email2__c;
	}
	public Boolean getSvoc_optin_email2__c() {
		return svoc_optin_email2__c;
	}
	public void setSvoc_optin_email2__c(Boolean svoc_optin_email2__c) {
		this.svoc_optin_email2__c = svoc_optin_email2__c;
	}
	public String getSvoc_officecountrycode__c() {
		return svoc_officecountrycode__c;
	}
	public void setSvoc_officecountrycode__c(String svoc_officecountrycode__c) {
		this.svoc_officecountrycode__c = svoc_officecountrycode__c;
	}
	public String getSvoc_officephone__c() {
		return svoc_officephone__c;
	}
	public void setSvoc_officephone__c(String svoc_officephone__c) {
		this.svoc_officephone__c = svoc_officephone__c;
	}
	public String getSvoc_extension__c() {
		return svoc_extension__c;
	}
	public void setSvoc_extension__c(String svoc_extension__c) {
		this.svoc_extension__c = svoc_extension__c;
	}
	public String getSvoc_email3__c() {
		return svoc_email3__c;
	}
	public void setSvoc_email3__c(String svoc_email3__c) {
		this.svoc_email3__c = svoc_email3__c;
	}
	public Boolean getSvoc_optincompanyemail__c() {
		return svoc_optincompanyemail__c;
	}
	public void setSvoc_optincompanyemail__c(Boolean svoc_optincompanyemail__c) {
		this.svoc_optincompanyemail__c = svoc_optincompanyemail__c;
	}
	public String getSvoc_passport_number__c() {
		return svoc_passport_number__c;
	}
	public void setSvoc_passport_number__c(String svoc_passport_number__c) {
		this.svoc_passport_number__c = svoc_passport_number__c;
	}
	public String getSvoc_city__c() {
		return svoc_city__c;
	}
	public void setSvoc_city__c(String svoc_city__c) {
		this.svoc_city__c = svoc_city__c;
	}
	public String getPersonleadsource() {
		return personleadsource;
	}
	public void setPersonleadsource(String personleadsource) {
		this.personleadsource = personleadsource;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getSvoc_contactemergencyname__c() {
		return svoc_contactemergencyname__c;
	}
	public void setSvoc_contactemergencyname__c(String svoc_contactemergencyname__c) {
		this.svoc_contactemergencyname__c = svoc_contactemergencyname__c;
	}
	public String getSvoc_emergencycountrycode__c() {
		return svoc_emergencycountrycode__c;
	}
	public void setSvoc_emergencycountrycode__c(String svoc_emergencycountrycode__c) {
		this.svoc_emergencycountrycode__c = svoc_emergencycountrycode__c;
	}
	public String getSvoc_emergencyphone__c() {
		return svoc_emergencyphone__c;
	}
	public void setSvoc_emergencyphone__c(String svoc_emergencyphone__c) {
		this.svoc_emergencyphone__c = svoc_emergencyphone__c;
	}
	public String getSvoc_emergencyemail__c() {
		return svoc_emergencyemail__c;
	}
	public void setSvoc_emergencyemail__c(String svoc_emergencyemail__c) {
		this.svoc_emergencyemail__c = svoc_emergencyemail__c;
	}
	public String getSvoc_relationship__c() {
		return svoc_relationship__c;
	}
	public void setSvoc_relationship__c(String svoc_relationship__c) {
		this.svoc_relationship__c = svoc_relationship__c;
	}
	public String getSvoc_paexternalid__c() {
		return svoc_paexternalid__c;
	}
	public void setSvoc_paexternalid__c(String svoc_paexternalid__c) {
		this.svoc_paexternalid__c = svoc_paexternalid__c;
	}
	public String getRecordtypeid() {
		return recordtypeid;
	}
	public void setRecordtypeid(String recordtypeid) {
		this.recordtypeid = recordtypeid;
	}
	public String getSvoc_purchasepropensity__c() {
		return svoc_purchasepropensity__c;
	}
	public void setSvoc_purchasepropensity__c(String svoc_purchasepropensity__c) {
		this.svoc_purchasepropensity__c = svoc_purchasepropensity__c;
	}
	public String getSvoc_winbackpropensity__c() {
		return svoc_winbackpropensity__c;
	}
	public void setSvoc_winbackpropensity__c(String svoc_winbackpropensity__c) {
		this.svoc_winbackpropensity__c = svoc_winbackpropensity__c;
	}
	public String getSvoc_dominantorigin__c() {
		return svoc_dominantorigin__c;
	}
	public void setSvoc_dominantorigin__c(String svoc_dominantorigin__c) {
		this.svoc_dominantorigin__c = svoc_dominantorigin__c;
	}
	public Timestamp getLastmodifieddate() {
		return lastmodifieddate;
	}
	public void setLastmodifieddate(Timestamp lastmodifieddate) {
		this.lastmodifieddate = lastmodifieddate;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getSystemModstamp() {
		return SystemModstamp;
	}
	public void setSystemModstamp(Timestamp systemModstamp) {
		SystemModstamp = systemModstamp;
	}
	public String get_hc_err() {
		return _hc_err;
	}
	public void set_hc_err(String _hc_err) {
		this._hc_err = _hc_err;
	}
	public String get_hc_lastop() {
		return _hc_lastop;
	}
	public void set_hc_lastop(String _hc_lastop) {
		this._hc_lastop = _hc_lastop;
	}
	public Boolean getSvoc_isactive__c() {
		return svoc_isactive__c;
	}
	public void setSvoc_isactive__c(Boolean svoc_isactive__c) {
		this.svoc_isactive__c = svoc_isactive__c;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public String toString() {
		return "PersonAccountEntity [id=" + id + ", sfid=" + sfid + ", svoc_customerid__c=" + svoc_customerid__c
				+ ", name=" + name + ", firstname=" + firstname + ", middlename=" + middlename + ", lastname="
				+ lastname + ", suffix=" + suffix + ", svoc_gaid__c=" + svoc_gaid__c + ", svoc_spid__c=" + svoc_spid__c
				+ ", svoc_company__c=" + svoc_company__c + ", svoc_flowntrips__c=" + svoc_flowntrips__c
				+ ", svoc_rfmtotal__c=" + svoc_rfmtotal__c + ", svoc_rfm_tier__c=" + svoc_rfm_tier__c + ", svoc_nps__c="
				+ svoc_nps__c + ", svoc_language__c=" + svoc_language__c + ", svoc_gendercode__c=" + svoc_gendercode__c
				+ ", svoc_dominantdestination__c=" + svoc_dominantdestination__c + ", svoc_customercategory__c="
				+ svoc_customercategory__c + ", svoc_nationality__c=" + svoc_nationality__c + ", svoc_country__c="
				+ svoc_country__c + ", personbirthdate=" + personbirthdate + ", svoc_countrycodemobile__c="
				+ svoc_countrycodemobile__c + ", personmobilephone=" + personmobilephone + ", svoc_optin_mobile__c="
				+ svoc_optin_mobile__c + ", personemail=" + personemail + ", svoc_optin_email__c=" + svoc_optin_email__c
				+ ", svoc_countrycode__c=" + svoc_countrycode__c + ", phone=" + phone + ", svoc_email2__c="
				+ svoc_email2__c + ", svoc_optin_email2__c=" + svoc_optin_email2__c + ", svoc_officecountrycode__c="
				+ svoc_officecountrycode__c + ", svoc_officephone__c=" + svoc_officephone__c + ", svoc_extension__c="
				+ svoc_extension__c + ", svoc_email3__c=" + svoc_email3__c + ", svoc_optincompanyemail__c="
				+ svoc_optincompanyemail__c + ", svoc_passport_number__c=" + svoc_passport_number__c + ", svoc_city__c="
				+ svoc_city__c + ", personleadsource=" + personleadsource + ", salutation=" + salutation
				+ ", svoc_contactemergencyname__c=" + svoc_contactemergencyname__c + ", svoc_emergencycountrycode__c="
				+ svoc_emergencycountrycode__c + ", svoc_emergencyphone__c=" + svoc_emergencyphone__c
				+ ", svoc_emergencyemail__c=" + svoc_emergencyemail__c + ", svoc_relationship__c="
				+ svoc_relationship__c + ", svoc_paexternalid__c=" + svoc_paexternalid__c + ", recordtypeid="
				+ recordtypeid + ", svoc_purchasepropensity__c=" + svoc_purchasepropensity__c
				+ ", svoc_winbackpropensity__c=" + svoc_winbackpropensity__c + ", svoc_dominantorigin__c="
				+ svoc_dominantorigin__c + ", lastmodifieddate=" + lastmodifieddate + ", createddate=" + createddate
				+ ", SystemModstamp=" + SystemModstamp + ", _hc_err=" + _hc_err + ", _hc_lastop=" + _hc_lastop
				+ ", svoc_isactive__c=" + svoc_isactive__c + "]";
	}
}
