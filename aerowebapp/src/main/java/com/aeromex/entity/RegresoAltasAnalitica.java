package com.aeromex.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "account", schema = "salesforce")
public class RegresoAltasAnalitica implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "svoc_customerid__c", unique = true, nullable = false)
	private String svoc_customerid__c;	
	private String sfid;

	public String getSfid() {
		return sfid;
	}
	public void setSfid(String sfid) {
		this.sfid = sfid;
	}
	public String getSvoc_customerid__c() {
		return svoc_customerid__c;
	}
	public void setSvoc_customerid__c(String svoc_customerid__c) {
		this.svoc_customerid__c = svoc_customerid__c;
	}
	
	@Override
	public String toString() {
		return "RegresoAltasAnalitica [sfid=" + sfid + ", svoc_customerid__c=" + svoc_customerid__c + "]";
	}	
}
