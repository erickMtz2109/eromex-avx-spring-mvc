package com.aeromex.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "log_svoc", schema = "svoc")
public class SvocLogEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private String id;
	private String object_error;
	private String customer_id;
	private String dml_type;
	private String error_message;
	private String record;
	private Timestamp datetime_error;
	private String folio;
	@Column(columnDefinition = "numeric")
	private Integer error_code;
	
	public SvocLogEntity() {
	}

	public SvocLogEntity(String object_error, String customer_id, String dml_type, String error_message, String record,
			Timestamp datetime_error) {
		super();
		this.object_error = object_error;
		this.customer_id = customer_id;
		this.dml_type = dml_type;
		this.error_message = error_message;
		this.record = record;
		this.datetime_error = datetime_error;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getObject_error() {
		return object_error;
	}
	public void setObject_error(String object_error) {
		this.object_error = object_error;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getDml_type() {
		return dml_type;
	}
	public void setDml_type(String dml_type) {
		this.dml_type = dml_type;
	}
	public String getError_message() {
		return error_message;
	}
	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
	public String getRecord() {
		return record;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public Timestamp getDatetime_error() {
		return datetime_error;
	}
	public void setDatetime_error(Timestamp datetime_error) {
		this.datetime_error = datetime_error;
	}
    public String getFolio() {
        return folio;
    }
    public void setFolio(String folio) {
        this.folio = folio;
    }	
	public Integer getError_code() {
		return error_code;
	}
	public void setError_code(Integer error_code) {
		this.error_code = error_code;
	}

	@Override
	public String toString() {
		return "SvocLogEntity [id=" + id + ", object_error=" + object_error + ", customer_id=" + customer_id
				+ ", dml_type=" + dml_type + ", error_message=" + error_message + ", record=" + record
				+ ", datetime_error=" + datetime_error + ", folio=" + folio + ", error_code=" + error_code + "]";
	}
}
