package com.aeromex.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Ivan Emilio Garcia Cifuentes
 */
@Entity
@Table(name = "deltas_log_svoc", schema = "svoc")
public class SVOCDeltasLogEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "folio")
    private String folio;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "inicio")
    private Date inicio;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fin")
    private Date fin;
    @Column(name = "total_cuentas")
    private int totalCuentas;
    @Column(name = "total_cobrand")
    private int totalCobrand;
    @Column(name = "total_fflyer")
    private int totalFrequentFlyer;
    @Column(name = "total_cobrand_ok")
    private int totalCobrandOK;
    @Column(name = "total_cobrand_error")
    private int totalCobrandError;
    @Column(name = "total_fflyer_ok")
    private int totalFrequentFlyerdOK;
    @Column(name = "total_fflyer_error")
    private int totalFrequentFlyerError;
    @Column(name = "total_cuentas_ok")
    private int totalCuentasOK;
    @Column(name = "total_cuentas_error")
    private int totalCuentasError;
    @Column(name = "operacion")
    private String operacion;
    @Column(name = "hilo")
    private String hilo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public int getTotalCuentas() {
        return totalCuentas;
    }

    public void setTotalCuentas(int totalCuentas) {
        this.totalCuentas = totalCuentas;
    }

    public int getTotalCobrand() {
        return totalCobrand;
    }

    public void setTotalCobrand(int totalCobrand) {
        this.totalCobrand = totalCobrand;
    }

    public int getTotalFrequentFlyer() {
        return totalFrequentFlyer;
    }

    public void setTotalFrequentFlyer(int totalFrequentFlyer) {
        this.totalFrequentFlyer = totalFrequentFlyer;
    }

    public int getTotalCobrandOK() {
        return totalCobrandOK;
    }

    public void setTotalCobrandOK(int totalCobrandOK) {
        this.totalCobrandOK = totalCobrandOK;
    }

    public int getTotalCobrandError() {
        return totalCobrandError;
    }

    public void setTotalCobrandError(int totalCobrandError) {
        this.totalCobrandError = totalCobrandError;
    }

    public int getTotalFrequentFlyerdOK() {
        return totalFrequentFlyerdOK;
    }

    public void setTotalFrequentFlyerdOK(int totalFrequentFlyerdOK) {
        this.totalFrequentFlyerdOK = totalFrequentFlyerdOK;
    }

    public int getTotalFrequentFlyerError() {
        return totalFrequentFlyerError;
    }

    public void setTotalFrequentFlyerError(int totalFrequentFlyerError) {
        this.totalFrequentFlyerError = totalFrequentFlyerError;
    }

    public int getTotalCuentasOK() {
        return totalCuentasOK;
    }

    public void setTotalCuentasOK(int totalCuentasOK) {
        this.totalCuentasOK = totalCuentasOK;
    }

    public int getTotalCuentasError() {
        return totalCuentasError;
    }

    public void setTotalCuentasError(int totalCuentasError) {
        this.totalCuentasError = totalCuentasError;
    }

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getHilo() {
		return hilo;
	}

	public void setHilo(String hilo) {
		this.hilo = hilo;
	}
    
	
}
