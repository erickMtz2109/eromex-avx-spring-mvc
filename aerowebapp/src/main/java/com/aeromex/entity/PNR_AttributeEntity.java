package com.aeromex.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pnr_prioritization")
public class PNR_AttributeEntity {

	@Id
	@Column(name = "prn_attribute")
	private String prn_attribute;
	private Integer points;	

	public String getPrn_attribute() {
		return prn_attribute;
	}
	public void setPrn_attribute(String prn_attribute) {
		this.prn_attribute = prn_attribute;
	}
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}

	@Override
	public String toString() {
		return "PNR_AttributeEntity [prn_attribute=" + prn_attribute + ", points=" + points + "]";
	}
}
