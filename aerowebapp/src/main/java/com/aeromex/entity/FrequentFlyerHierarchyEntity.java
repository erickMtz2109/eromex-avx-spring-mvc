package com.aeromex.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "frequent_flyer_gerarchy")
public class FrequentFlyerHierarchyEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	private String fqtv;
	private Integer priority;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFqtv() {
		return fqtv;
	}
	public void setFqtv(String fqtv) {
		this.fqtv = fqtv;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	
	@Override
	public String toString() {
		return "FrequentFlyerGerarchyEntity [id=" + id + ", fqtv=" + fqtv + ", priority=" + priority + "]";
	}
}
