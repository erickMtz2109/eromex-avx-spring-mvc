package com.aeromex.utility;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ConnectionUtil {

    private static SessionFactory factory;

    private ConnectionUtil() {

    }

    @SuppressWarnings("deprecation")
    public static SessionFactory getSessionFactory() {
        if (factory == null) {
            Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml");
            configuration.getProperties().replace("hibernate.connection.url", System.getenv("JDBC_DATABASE_URL"));
            configuration.getProperties().replace("hibernate.connection.username", System.getenv("JDBC_DATABASE_USERNAME"));
            configuration.getProperties().replace("hibernate.connection.password", System.getenv("JDBC_DATABASE_PASSWORD"));  
            
//          full
//	        configuration.getProperties().replace("hibernate.connection.url", "jdbc:postgresql://ec2-18-204-129-65.compute-1.amazonaws.com:5432/d2gm4ah6rf8ukh?sslmode=require&cachePrepStmts=true&reWriteBatchedInserts=true");
//	        configuration.getProperties().replace("hibernate.connection.username", "am_deltas_full");
//	        configuration.getProperties().replace("hibernate.connection.password", "p48ff2f67a565ca9934feb522a6cc20b8e36d2ede31149d8682330ae4db9cda1c");
          
//			new full P6
//     		configuration.getProperties().replace("hibernate.connection.url", "jdbc:postgresql://ec2-18-235-29-28.compute-1.amazonaws.com:5432/d2gm4ah6rf8ukh?sslmode=require&cachePrepStmts=true&reWriteBatchedInserts=true");
//     		configuration.getProperties().replace("hibernate.connection.username", "ubkln3gblt50c7");
//     		configuration.getProperties().replace("hibernate.connection.password", "p242bd94854d0609fbada333e917913945e59d945f8b7f58c2fff573ed941f457");

//          prod            
//          configuration.getProperties().replace("hibernate.connection.url", "jdbc:postgresql://ec2-52-45-210-246.compute-1.amazonaws.com:5432/d39jn2um3lqvfo?sslmode=require&cachePrepStmts=true&reWriteBatchedInserts=true");
//          configuration.getProperties().replace("hibernate.connection.username", "am_account_prod");
//          configuration.getProperties().replace("hibernate.connection.password", "pe1424bcba623fa3b815ee10fad9563a5f621484d5801b0ce1e37b7d4900fa13c");

            
            factory = configuration.buildSessionFactory();
        }

        return factory;
    }

}
