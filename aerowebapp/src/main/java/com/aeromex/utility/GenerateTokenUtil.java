package com.aeromex.utility;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.aeromex.model.TokenResponse;

public class GenerateTokenUtil {

	public static String getSalesforceToken() {
		RestTemplate restTemplate = new RestTemplate();
		
		String url = System.getenv("SFDC_TOKEN_URL");
		String grant_type = System.getenv("GRANT_TYPE");
		String client_id = System.getenv("CLIENT_ID");
		String client_secret = System.getenv("CLIENT_SECRET");
		String username = System.getenv("USERNAME");
		String password = System.getenv("PASSWORD");

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/x-www-form-urlencoded");

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("grant_type", grant_type);
		map.add("client_id", client_id);
		map.add("client_secret", client_secret);
		map.add("username", username);
		map.add("password", password);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

		try {
			ResponseEntity<TokenResponse> response = restTemplate.postForEntity(url, request, TokenResponse.class);
			System.out.println("boject response -> " + response.getBody().getAccess_token());
			 String token = response.getBody().getToken_type() + " " + response.getBody().getAccess_token();
			return token;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
