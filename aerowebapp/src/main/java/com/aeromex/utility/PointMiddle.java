package com.aeromex.utility;

public class PointMiddle {
	public static int middlePoint(int size_) {
		int middleP = 0;
		if (size_ % 2 == 1) {
			size_ += 1;
			middleP = size_ / 2 - 1;
		} else {
			middleP = size_ / 2;
		}
		return middleP;
	}

}
