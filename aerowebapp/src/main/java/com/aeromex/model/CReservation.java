package com.aeromex.model;

import java.sql.Date;

public class CReservation{
	Reserv reservation = new Reserv();	
	

	public Reserv getReservation() {
		return reservation;
	}
	public void setReservation(Reserv reservation) {
		this.reservation = reservation;
	}

	public class Reserv {
		private String svocNumberofadultsC;
		private String svocSpidC;
		private String svocSaleschannelC;
		private String svocStatusC;
		private String svocCurrencyC;
		private Date svocPnrcreatedateC;
		private String svocEmailC;
		private String svocPhoneC;
		private String svocGroupNameC;
		private String svocFlighttypeC;
		private String svocNumberinpartyC;
		private String svocPnrIdC;
		private String svocCountryC;
		private String svocTimelimitC;
		private String svocNumberofminorsC;
		private String svocNumberofinfantsC;
		
		

		public String getSvocNumberofadultsC() {
			return svocNumberofadultsC;
		}
		public void setSvocNumberofadultsC(String svocNumberofadultsC) {
			this.svocNumberofadultsC = svocNumberofadultsC;
		}
		public String getSvocSpidC() {
			return svocSpidC;
		}
		public void setSvocSpidC(String svocSpidC) {
			this.svocSpidC = svocSpidC;
		}
		public String getSvocSaleschannelC() {
			return svocSaleschannelC;
		}
		public void setSvocSaleschannelC(String svocSaleschannelC) {
			this.svocSaleschannelC = svocSaleschannelC;
		}
		public String getSvocStatusC() {
			return svocStatusC;
		}
		public void setSvocStatusC(String svocStatusC) {
			this.svocStatusC = svocStatusC;
		}
		public String getSvocCurrencyC() {
			return svocCurrencyC;
		}
		public void setSvocCurrencyC(String svocCurrencyC) {
			this.svocCurrencyC = svocCurrencyC;
		}
		public Date getSvocPnrcreatedateC() {
			return svocPnrcreatedateC;
		}
		public void setSvocPnrcreatedateC(Date svocPnrcreatedateC) {
			this.svocPnrcreatedateC = svocPnrcreatedateC;
		}
		public String getSvocEmailC() {
			return svocEmailC;
		}
		public void setSvocEmailC(String svocEmailC) {
			this.svocEmailC = svocEmailC;
		}
		public String getSvocPhoneC() {
			return svocPhoneC;
		}
		public void setSvocPhoneC(String svocPhoneC) {
			this.svocPhoneC = svocPhoneC;
		}
		public String getSvocGroupNameC() {
			return svocGroupNameC;
		}
		public void setSvocGroupNameC(String svocGroupNameC) {
			this.svocGroupNameC = svocGroupNameC;
		}
		public String getSvocFlighttypeC() {
			return svocFlighttypeC;
		}
		public void setSvocFlighttypeC(String svocFlighttypeC) {
			this.svocFlighttypeC = svocFlighttypeC;
		}
		public String getSvocNumberinpartyC() {
			return svocNumberinpartyC;
		}
		public void setSvocNumberinpartyC(String svocNumberinpartyC) {
			this.svocNumberinpartyC = svocNumberinpartyC;
		}
		public String getSvocPnrIdC() {
			return svocPnrIdC;
		}
		public void setSvocPnrIdC(String svocPnrIdC) {
			this.svocPnrIdC = svocPnrIdC;
		}
		public String getSvocCountryC() {
			return svocCountryC;
		}
		public void setSvocCountryC(String svocCountryC) {
			this.svocCountryC = svocCountryC;
		}
		public String getSvocTimelimitC() {
			return svocTimelimitC;
		}
		public void setSvocTimelimitC(String svocTimelimitC) {
			this.svocTimelimitC = svocTimelimitC;
		}
		public String getSvocNumberofminorsC() {
			return svocNumberofminorsC;
		}
		public void setSvocNumberofminorsC(String svocNumberofminorsC) {
			this.svocNumberofminorsC = svocNumberofminorsC;
		}
		public String getSvocNumberofinfantsC() {
			return svocNumberofinfantsC;
		}
		public void setSvocNumberofinfantsC(String svocNumberofinfantsC) {
			this.svocNumberofinfantsC = svocNumberofinfantsC;
		}
	
	}
	
}