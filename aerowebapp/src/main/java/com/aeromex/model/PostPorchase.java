package com.aeromex.model;

import java.sql.Date;

public class PostPorchase {	
	private String gender;
	private String namefirst;
	private String namelast;
	private String frequenttravelernbr;
	private String vendorcode;
	private Date vcrcreatedate;
	private String pnrlocatorid;
	private String marketingflightnbr;
	private String equipo;
	private String goldenw;
	private String pais_compra;
	private String categoria;
	private String lenguaje;
	private String basefareamt;
	private String classofservice;
	private String basefarecurrcode;
	private String cabina;
	private String servicestartcity;
	private String servicestartcity_desc;
	private String serviceendcityfinal;
	private String serviceendcityfinal_desc;
	private String fecha_actual;
	private Date servicestartdatetime;
	private Date serviceenddatetimefinal;
	private String horas;
	private String ow_rt;
	private String emailaddress;
	private String celular;
	private String go_back;
	private String region;
	private String operatingairlinecode;
	private String upgrade_ind;
	private String premium_seat_ind;
	private String seguro_ind;
	private String concierge_ind;
	private String disc_pass_ind;
	private String kit_experto;
	private String primaleta_ind;
	private String segmaleta_ind;
	private String cbx_ind;
	private String cuponeras_ind;
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNamefirst() {
		return namefirst;
	}
	public void setNamefirst(String namefirst) {
		this.namefirst = namefirst;
	}
	public String getNamelast() {
		return namelast;
	}
	public void setNamelast(String namelast) {
		this.namelast = namelast;
	}
	public String getFrequenttravelernbr() {
		return frequenttravelernbr;
	}
	public void setFrequenttravelernbr(String frequenttravelernbr) {
		this.frequenttravelernbr = frequenttravelernbr;
	}
	public String getVendorcode() {
		return vendorcode;
	}
	public void setVendorcode(String vendorcode) {
		this.vendorcode = vendorcode;
	}
	public Date getVcrcreatedate() {
		return vcrcreatedate;
	}
	public void setVcrcreatedate(Date vcrcreatedate) {
		this.vcrcreatedate = vcrcreatedate;
	}
	public String getPnrlocatorid() {
		return pnrlocatorid;
	}
	public void setPnrlocatorid(String pnrlocatorid) {
		this.pnrlocatorid = pnrlocatorid;
	}
	public String getMarketingflightnbr() {
		return marketingflightnbr;
	}
	public void setMarketingflightnbr(String marketingflightnbr) {
		this.marketingflightnbr = marketingflightnbr;
	}
	public String getEquipo() {
		return equipo;
	}
	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}
	public String getGoldenw() {
		return goldenw;
	}
	public void setGoldenw(String goldenw) {
		this.goldenw = goldenw;
	}
	public String getPais_compra() {
		return pais_compra;
	}
	public void setPais_compra(String pais_compra) {
		this.pais_compra = pais_compra;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getLenguaje() {
		return lenguaje;
	}
	public void setLenguaje(String lenguaje) {
		this.lenguaje = lenguaje;
	}
	public String getBasefareamt() {
		return basefareamt;
	}
	public void setBasefareamt(String basefareamt) {
		this.basefareamt = basefareamt;
	}
	public String getClassofservice() {
		return classofservice;
	}
	public void setClassofservice(String classofservice) {
		this.classofservice = classofservice;
	}
	public String getBasefarecurrcode() {
		return basefarecurrcode;
	}
	public void setBasefarecurrcode(String basefarecurrcode) {
		this.basefarecurrcode = basefarecurrcode;
	}
	public String getCabina() {
		return cabina;
	}
	public void setCabina(String cabina) {
		this.cabina = cabina;
	}
	public String getServicestartcity() {
		return servicestartcity;
	}
	public void setServicestartcity(String servicestartcity) {
		this.servicestartcity = servicestartcity;
	}
	public String getServicestartcity_desc() {
		return servicestartcity_desc;
	}
	public void setServicestartcity_desc(String servicestartcity_desc) {
		this.servicestartcity_desc = servicestartcity_desc;
	}
	public String getServiceendcityfinal() {
		return serviceendcityfinal;
	}
	public void setServiceendcityfinal(String serviceendcityfinal) {
		this.serviceendcityfinal = serviceendcityfinal;
	}
	public String getServiceendcityfinal_desc() {
		return serviceendcityfinal_desc;
	}
	public void setServiceendcityfinal_desc(String serviceendcityfinal_desc) {
		this.serviceendcityfinal_desc = serviceendcityfinal_desc;
	}
	public String getFecha_actual() {
		return fecha_actual;
	}
	public void setFecha_actual(String fecha_actual) {
		this.fecha_actual = fecha_actual;
	}
	public Date getServicestartdatetime() {
		return servicestartdatetime;
	}
	public void setServicestartdatetime(Date servicestartdatetime) {
		this.servicestartdatetime = servicestartdatetime;
	}
	public Date getServiceenddatetimefinal() {
		return serviceenddatetimefinal;
	}
	public void setServiceenddatetimefinal(Date serviceenddatetimefinal) {
		this.serviceenddatetimefinal = serviceenddatetimefinal;
	}
	public String getHoras() {
		return horas;
	}
	public void setHoras(String horas) {
		this.horas = horas;
	}
	public String getOw_rt() {
		return ow_rt;
	}
	public void setOw_rt(String ow_rt) {
		this.ow_rt = ow_rt;
	}
	public String getEmailaddress() {
		return emailaddress;
	}
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getGo_back() {
		return go_back;
	}
	public void setGo_back(String go_back) {
		this.go_back = go_back;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getOperatingairlinecode() {
		return operatingairlinecode;
	}
	public void setOperatingairlinecode(String operatingairlinecode) {
		this.operatingairlinecode = operatingairlinecode;
	}
	public String getUpgrade_ind() {
		return upgrade_ind;
	}
	public void setUpgrade_ind(String upgrade_ind) {
		this.upgrade_ind = upgrade_ind;
	}
	public String getPremium_seat_ind() {
		return premium_seat_ind;
	}
	public void setPremium_seat_ind(String premium_seat_ind) {
		this.premium_seat_ind = premium_seat_ind;
	}
	public String getSeguro_ind() {
		return seguro_ind;
	}
	public void setSeguro_ind(String seguro_ind) {
		this.seguro_ind = seguro_ind;
	}
	public String getConcierge_ind() {
		return concierge_ind;
	}
	public void setConcierge_ind(String concierge_ind) {
		this.concierge_ind = concierge_ind;
	}
	public String getDisc_pass_ind() {
		return disc_pass_ind;
	}
	public void setDisc_pass_ind(String disc_pass_ind) {
		this.disc_pass_ind = disc_pass_ind;
	}
	public String getKit_experto() {
		return kit_experto;
	}
	public void setKit_experto(String kit_experto) {
		this.kit_experto = kit_experto;
	}
	public String getPrimaleta_ind() {
		return primaleta_ind;
	}
	public void setPrimaleta_ind(String primaleta_ind) {
		this.primaleta_ind = primaleta_ind;
	}
	public String getSegmaleta_ind() {
		return segmaleta_ind;
	}
	public void setSegmaleta_ind(String segmaleta_ind) {
		this.segmaleta_ind = segmaleta_ind;
	}
	public String getCbx_ind() {
		return cbx_ind;
	}
	public void setCbx_ind(String cbx_ind) {
		this.cbx_ind = cbx_ind;
	}
	public String getCuponeras_ind() {
		return cuponeras_ind;
	}
	public void setCuponeras_ind(String cuponeras_ind) {
		this.cuponeras_ind = cuponeras_ind;
	}
	
	@Override
	public String toString() {
		return "PostPorchase [gender=" + gender + ", namefirst=" + namefirst + ", namelast=" + namelast
				+ ", frequenttravelernbr=" + frequenttravelernbr + ", vendorcode=" + vendorcode + ", vcrcreatedate="
				+ vcrcreatedate + ", pnrlocatorid=" + pnrlocatorid + ", marketingflightnbr=" + marketingflightnbr
				+ ", equipo=" + equipo + ", goldenw=" + goldenw + ", pais_compra=" + pais_compra + ", categoria="
				+ categoria + ", lenguaje=" + lenguaje + ", basefareamt=" + basefareamt + ", classofservice="
				+ classofservice + ", basefarecurrcode=" + basefarecurrcode + ", cabina=" + cabina
				+ ", servicestartcity=" + servicestartcity + ", servicestartcity_desc=" + servicestartcity_desc
				+ ", serviceendcityfinal=" + serviceendcityfinal + ", serviceendcityfinal_desc="
				+ serviceendcityfinal_desc + ", fecha_actual=" + fecha_actual + ", servicestartdatetime="
				+ servicestartdatetime + ", serviceenddatetimefinal=" + serviceenddatetimefinal + ", horas=" + horas
				+ ", ow_rt=" + ow_rt + ", emailaddress=" + emailaddress + ", celular=" + celular + ", go_back="
				+ go_back + ", region=" + region + ", operatingairlinecode=" + operatingairlinecode + ", upgrade_ind="
				+ upgrade_ind + ", premium_seat_ind=" + premium_seat_ind + ", seguro_ind=" + seguro_ind
				+ ", concierge_ind=" + concierge_ind + ", disc_pass_ind=" + disc_pass_ind + ", kit_experto="
				+ kit_experto + ", primaleta_ind=" + primaleta_ind + ", segmaleta_ind=" + segmaleta_ind + ", cbx_ind="
				+ cbx_ind + ", cuponeras_ind=" + cuponeras_ind + "]";
	}
}
