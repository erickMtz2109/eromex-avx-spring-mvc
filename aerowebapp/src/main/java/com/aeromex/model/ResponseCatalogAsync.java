package com.aeromex.model;

public class ResponseCatalogAsync {
	
	private String status;
	private String message;
	private Integer aircraft;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getAircraft() {
		return aircraft;
	}
	public void setAircraft(Integer aircraft) {
		this.aircraft = aircraft;
	}
	
	@Override
	public String toString() {
		return "ResponseCatalogAsync [status=" + status + ", message=" + message + ", aircraft=" + aircraft + "]";
	}
}
