package com.aeromex.model;

public class RequestSales {

	private String account_id;
	private Boolean activate;
	private String pnr_locator;

	public String getAccount_id() {
		return account_id;
	}

	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}

	public Boolean getActivate() {
		return activate;
	}

	public void setActivate(Boolean activate) {
		this.activate = activate;
	}

	public String getPnr_locator() {
		return pnr_locator;
	}

	public void setPnr_locator(String pnr_locator) {
		this.pnr_locator = pnr_locator;
	}

	@Override
	public String toString() {
		return "RequestSales [account_id=" + account_id + ", activate=" + activate + ", pnr_locator=" + pnr_locator
				+ "]";
	}
}
