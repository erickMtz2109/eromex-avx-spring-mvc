package com.aeromex.model;

import java.sql.Date;
import java.sql.Timestamp;

public class Flight {
	
	private Integer id;
	private String marketing_flight_number;
	private String operating_flight_number;
	private String service_start_city;
	private String service_end_city;
	private Timestamp service_start_date;
	private Date service_end_date;
	private Date service_start_time;
	private Timestamp service_end_time;
	private Timestamp ETD;
	private Timestamp ETA;
	private String operating_airline_code;
	private String marketing_airline_code;
	private String tail_number;
	private String aircraft;
	private String status;
	private String IROP_description;
	private String IROP_call_action;
	private String boarding_gates;
	private String conveyor;
	private String departure_terminal;
	private String arrival_terminal;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMarketing_flight_number() {
		return marketing_flight_number;
	}
	public void setMarketing_flight_number(String marketing_flight_number) {
		this.marketing_flight_number = marketing_flight_number;
	}
	public String getOperating_flight_number() {
		return operating_flight_number;
	}
	public void setOperating_flight_number(String operating_flight_number) {
		this.operating_flight_number = operating_flight_number;
	}
	public String getService_start_city() {
		return service_start_city;
	}
	public void setService_start_city(String service_start_city) {
		this.service_start_city = service_start_city;
	}
	public String getService_end_city() {
		return service_end_city;
	}
	public void setService_end_city(String service_end_city) {
		this.service_end_city = service_end_city;
	}
	public Timestamp getService_start_date() {
		return service_start_date;
	}
	public void setService_start_date(Timestamp service_start_date) {
		this.service_start_date = service_start_date;
	}
	public Date getService_end_date() {
		return service_end_date;
	}
	public void setService_end_date(Date service_end_date) {
		this.service_end_date = service_end_date;
	}
	public Date getService_start_time() {
		return service_start_time;
	}
	public void setService_start_time(Date service_start_time) {
		this.service_start_time = service_start_time;
	}
	public Timestamp getService_end_time() {
		return service_end_time;
	}
	public void setService_end_time(Timestamp service_end_time) {
		this.service_end_time = service_end_time;
	}
	public Timestamp getETD() {
		return ETD;
	}
	public void setETD(Timestamp eTD) {
		ETD = eTD;
	}
	public Timestamp getETA() {
		return ETA;
	}
	public void setETA(Timestamp eTA) {
		ETA = eTA;
	}
	public String getOperating_airline_code() {
		return operating_airline_code;
	}
	public void setOperating_airline_code(String operating_airline_code) {
		this.operating_airline_code = operating_airline_code;
	}
	public String getMarketing_airline_code() {
		return marketing_airline_code;
	}
	public void setMarketing_airline_code(String marketing_airline_code) {
		this.marketing_airline_code = marketing_airline_code;
	}
	public String getTail_number() {
		return tail_number;
	}
	public void setTail_number(String tail_number) {
		this.tail_number = tail_number;
	}
	public String getAircraft() {
		return aircraft;
	}
	public void setAircraft(String aircraft) {
		this.aircraft = aircraft;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIROP_description() {
		return IROP_description;
	}
	public void setIROP_description(String iROP_description) {
		IROP_description = iROP_description;
	}
	public String getIROP_call_action() {
		return IROP_call_action;
	}
	public void setIROP_call_action(String iROP_call_action) {
		IROP_call_action = iROP_call_action;
	}
	public String getBoarding_gates() {
		return boarding_gates;
	}
	public void setBoarding_gates(String boarding_gates) {
		this.boarding_gates = boarding_gates;
	}
	public String getConveyor() {
		return conveyor;
	}
	public void setConveyor(String conveyor) {
		this.conveyor = conveyor;
	}
	public String getDeparture_terminal() {
		return departure_terminal;
	}
	public void setDeparture_terminal(String departure_terminal) {
		this.departure_terminal = departure_terminal;
	}
	public String getArrival_terminal() {
		return arrival_terminal;
	}
	public void setArrival_terminal(String arrival_terminal) {
		this.arrival_terminal = arrival_terminal;
	}
	
	@Override
	public String toString() {
		return "Flight [id=" + id + ", marketing_flight_number=" + marketing_flight_number
				+ ", operating_flight_number=" + operating_flight_number + ", service_start_city=" + service_start_city
				+ ", service_end_city=" + service_end_city + ", service_start_date=" + service_start_date
				+ ", service_end_date=" + service_end_date + ", service_start_time=" + service_start_time
				+ ", service_end_time=" + service_end_time + ", ETD=" + ETD + ", ETA=" + ETA
				+ ", operating_airline_code=" + operating_airline_code + ", marketing_airline_code="
				+ marketing_airline_code + ", tail_number=" + tail_number + ", aircraft=" + aircraft + ", status="
				+ status + ", IROP_description=" + IROP_description + ", IROP_call_action=" + IROP_call_action
				+ ", boarding_gates=" + boarding_gates + ", conveyor=" + conveyor + ", departure_terminal="
				+ departure_terminal + ", arrival_terminal=" + arrival_terminal + "]";
	}
}
