package com.aeromex.model;

import java.util.ArrayList;
import java.util.List;

public class ResponseReservation {
	Reservation Reservation = new Reservation();
	List<PersonAccount> Accounts = new ArrayList<PersonAccount>();
	List<Passenger> Passengers = new ArrayList<Passenger>();
	
	public Reservation getReservation() {
		return Reservation;
	}
	public void setReservation(Reservation reservation) {
		Reservation = reservation;
	}
	public List<Passenger> getPassengers() {
		return Passengers;
	}
	public void setPassengers(List<Passenger> passengers) {
		Passengers = passengers;
	}

	public List<PersonAccount> getAccounts() {
		return Accounts;
	}
	public void setAccounts(List<PersonAccount> accounts) {
		Accounts = accounts;
	}
	
	@Override
	public String toString() {
		return "ResponseReservation [Reservation=" + Reservation + ", Passengers=" + Passengers + ", Accounts=" + Accounts + "]";
	}	
}
