package com.aeromex.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProcesoDeltas implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String folio;
	public String fechaInicio;
	public String fechaFin;
	public Date inicio;
	public Date fin;
        public int totalCuentas;
        public int totalCobrand;
        public int totalFrequentFlyer;
        public int totalCobrandOK;
	public int totalCobrandError;
        public int totalFrequentFlyerdOK;
	public int totalFrequentFlyerError;
	public int totalProcesadosOK;
	public int totalProcesadosError;
	public List<String> errores = new ArrayList<String>();
	public long tiempoProceso;
	
	
	
	
}
