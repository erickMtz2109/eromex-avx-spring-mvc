package com.aeromex.model;

import java.sql.Date;

public class PassengerFind {
	
	private String firstname;
	private String lastname;
	private String middlename;
	private String suffix;
	private String email;
	private String phone;
	private String frequentflyernumber;
	private Date birthdate;
	private String account;
	private Integer rfmTotal;
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFrequentflyernumber() {
		return frequentflyernumber;
	}
	public void setFrequentflyernumber(String frequentflyernumber) {
		this.frequentflyernumber = frequentflyernumber;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public Integer getRfmTotal() {
		return rfmTotal;
	}
	public void setRfmTotal(Integer rfmTotal) {
		this.rfmTotal = rfmTotal;
	}
	
	
}
