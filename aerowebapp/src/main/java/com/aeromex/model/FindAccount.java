package com.aeromex.model;

import java.util.ArrayList;
import java.util.List;

public class FindAccount {
	private String pnr = new String();
	private List<PassengerFind> passengers = new ArrayList<PassengerFind>();
	
	String getPnr() {
		return pnr;
	}
	void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public List<PassengerFind> getPassengers() {
		return passengers;
	}
	void setPassengers(List<PassengerFind> passengers) {
		this.passengers = passengers;
	}
	@Override
	public String toString() {
		return "FindAccount [pnr=" + pnr + ", passengers=" + passengers + "]";
	}
}
