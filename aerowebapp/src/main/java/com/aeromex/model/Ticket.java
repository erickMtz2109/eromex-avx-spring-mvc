package com.aeromex.model;

import java.sql.Date;

public class Ticket {
	private Integer id;
	private String svoc_client__c;
	private String svoc_passenger__c;
	private String svoc_reservation__c;
	private String svoc_pnrlocatorid__c;
	private Date svoc_pnrcreatedate__c;
	private String svoc_primarydocnbr__c;
	private Date svoc_vcrcreatedate__c;
	private String tour_code_account_code__c;
	private String svoc_spid__c;
	private Double svoc_totalpaid__c;
	private String currency__c;
	private String svoc_paymentmethod__c;
	private String credit_card_01__c;
	private String svoc_creditcard02__c;
	private String svoc_doctypecode__c;
	private String svoc_status__c;
	private Double segment_number__c;
	private String svoc_emdtypecode__c;
	private String svoc_rficode__c;
	private String svoc_rfisubcode__c;
	private String svoc_comercialname__c;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSvoc_client__c() {
		return svoc_client__c;
	}
	public void setSvoc_client__c(String svoc_client__c) {
		this.svoc_client__c = svoc_client__c;
	}
	public String getSvoc_passenger__c() {
		return svoc_passenger__c;
	}
	public void setSvoc_passenger__c(String svoc_passenger__c) {
		this.svoc_passenger__c = svoc_passenger__c;
	}
	public String getSvoc_reservation__c() {
		return svoc_reservation__c;
	}
	public void setSvoc_reservation__c(String svoc_reservation__c) {
		this.svoc_reservation__c = svoc_reservation__c;
	}
	public String getSvoc_pnrlocatorid__c() {
		return svoc_pnrlocatorid__c;
	}
	public void setSvoc_pnrlocatorid__c(String svoc_pnrlocatorid__c) {
		this.svoc_pnrlocatorid__c = svoc_pnrlocatorid__c;
	}
	public Date getSvoc_pnrcreatedate__c() {
		return svoc_pnrcreatedate__c;
	}
	public void setSvoc_pnrcreatedate__c(Date svoc_pnrcreatedate__c) {
		this.svoc_pnrcreatedate__c = svoc_pnrcreatedate__c;
	}
	public String getSvoc_primarydocnbr__c() {
		return svoc_primarydocnbr__c;
	}
	public void setSvoc_primarydocnbr__c(String svoc_primarydocnbr__c) {
		this.svoc_primarydocnbr__c = svoc_primarydocnbr__c;
	}
	public Date getSvoc_vcrcreatedate__c() {
		return svoc_vcrcreatedate__c;
	}
	public void setSvoc_vcrcreatedate__c(Date svoc_vcrcreatedate__c) {
		this.svoc_vcrcreatedate__c = svoc_vcrcreatedate__c;
	}
	public String getTour_code_account_code__c() {
		return tour_code_account_code__c;
	}
	public void setTour_code_account_code__c(String tour_code_account_code__c) {
		this.tour_code_account_code__c = tour_code_account_code__c;
	}
	public String getSvoc_spid__c() {
		return svoc_spid__c;
	}
	public void setSvoc_spid__c(String svoc_spid__c) {
		this.svoc_spid__c = svoc_spid__c;
	}
	public Double getSvoc_totalpaid__c() {
		return svoc_totalpaid__c;
	}
	public void setSvoc_totalpaid__c(Double svoc_totalpaid__c) {
		this.svoc_totalpaid__c = svoc_totalpaid__c;
	}
	public String getCurrency__c() {
		return currency__c;
	}
	public void setCurrency__c(String currency__c) {
		this.currency__c = currency__c;
	}
	public String getSvoc_paymentmethod__c() {
		return svoc_paymentmethod__c;
	}
	public void setSvoc_paymentmethod__c(String svoc_paymentmethod__c) {
		this.svoc_paymentmethod__c = svoc_paymentmethod__c;
	}
	public String getCredit_card_01__c() {
		return credit_card_01__c;
	}
	public void setCredit_card_01__c(String credit_card_01__c) {
		this.credit_card_01__c = credit_card_01__c;
	}
	public String getSvoc_creditcard02__c() {
		return svoc_creditcard02__c;
	}
	public void setSvoc_creditcard02__c(String svoc_creditcard02__c) {
		this.svoc_creditcard02__c = svoc_creditcard02__c;
	}
	public String getSvoc_doctypecode__c() {
		return svoc_doctypecode__c;
	}
	public void setSvoc_doctypecode__c(String svoc_doctypecode__c) {
		this.svoc_doctypecode__c = svoc_doctypecode__c;
	}
	public String getSvoc_status__c() {
		return svoc_status__c;
	}
	public void setSvoc_status__c(String svoc_status__c) {
		this.svoc_status__c = svoc_status__c;
	}
	public Double getSegment_number__c() {
		return segment_number__c;
	}
	public void setSegment_number__c(Double segment_number__c) {
		this.segment_number__c = segment_number__c;
	}
	public String getSvoc_emdtypecode__c() {
		return svoc_emdtypecode__c;
	}
	public void setSvoc_emdtypecode__c(String svoc_emdtypecode__c) {
		this.svoc_emdtypecode__c = svoc_emdtypecode__c;
	}
	public String getSvoc_rficode__c() {
		return svoc_rficode__c;
	}
	public void setSvoc_rficode__c(String svoc_rficode__c) {
		this.svoc_rficode__c = svoc_rficode__c;
	}
	public String getSvoc_rfisubcode__c() {
		return svoc_rfisubcode__c;
	}
	public void setSvoc_rfisubcode__c(String svoc_rfisubcode__c) {
		this.svoc_rfisubcode__c = svoc_rfisubcode__c;
	}
	public String getSvoc_comercialname__c() {
		return svoc_comercialname__c;
	}
	public void setSvoc_comercialname__c(String svoc_comercialname__c) {
		this.svoc_comercialname__c = svoc_comercialname__c;
	}
	
	@Override
	public String toString() {
		return "Ticket [id=" + id + ", svoc_client__c=" + svoc_client__c + ", svoc_passenger__c=" + svoc_passenger__c
				+ ", svoc_reservation__c=" + svoc_reservation__c + ", svoc_pnrlocatorid__c=" + svoc_pnrlocatorid__c
				+ ", svoc_pnrcreatedate__c=" + svoc_pnrcreatedate__c + ", svoc_primarydocnbr__c="
				+ svoc_primarydocnbr__c + ", svoc_vcrcreatedate__c=" + svoc_vcrcreatedate__c
				+ ", tour_code_account_code__c=" + tour_code_account_code__c + ", svoc_spid__c=" + svoc_spid__c
				+ ", svoc_totalpaid__c=" + svoc_totalpaid__c + ", currency__c=" + currency__c
				+ ", svoc_paymentmethod__c=" + svoc_paymentmethod__c + ", credit_card_01__c=" + credit_card_01__c
				+ ", svoc_creditcard02__c=" + svoc_creditcard02__c + ", svoc_doctypecode__c=" + svoc_doctypecode__c
				+ ", svoc_status__c=" + svoc_status__c + ", segment_number__c=" + segment_number__c
				+ ", svoc_emdtypecode__c=" + svoc_emdtypecode__c + ", svoc_rficode__c=" + svoc_rficode__c
				+ ", svoc_rfisubcode__c=" + svoc_rfisubcode__c + ", svoc_comercialname__c=" + svoc_comercialname__c
				+ "]";
	}
}
