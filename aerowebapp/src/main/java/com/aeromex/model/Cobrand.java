package com.aeromex.model;

import java.sql.Timestamp;

public class Cobrand {
	private Integer id;
	private String sfid;
	private String name;
	private String svoc_cuenta__c;
	private String svoc_cardnumber__c;
	private String svoc_bank__c;
	private String svoc_customerid__c;
	private String svoc_cobrandexternalid__c;	
	private String svoc_cardtier__c;	
	private String svoc_membersince__c;
	private String svoc_expirationdate__c;
	private Timestamp createddate;
	private Timestamp lastmodifieddate;
	private String svoc_cuenta__r__svoc_customerid__c;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSfid() {
		return sfid;
	}
	public void setSfid(String sfid) {
		this.sfid = sfid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSvoc_cuenta__c() {
		return svoc_cuenta__c;
	}
	public void setSvoc_cuenta__c(String svoc_cuenta__c) {
		this.svoc_cuenta__c = svoc_cuenta__c;
	}
	public String getSvoc_cardnumber__c() {
		return svoc_cardnumber__c;
	}
	public void setSvoc_cardnumber__c(String svoc_cardnumber__c) {
		this.svoc_cardnumber__c = svoc_cardnumber__c;
	}
	public String getSvoc_bank__c() {
		return svoc_bank__c;
	}
	public void setSvoc_bank__c(String svoc_bank__c) {
		this.svoc_bank__c = svoc_bank__c;
	}
	public String getSvoc_customerid__c() {
		return svoc_customerid__c;
	}
	public void setSvoc_customerid__c(String svoc_customerid__c) {
		this.svoc_customerid__c = svoc_customerid__c;
	}
	public String getSvoc_cobrandexternalid__c() {
		return svoc_cobrandexternalid__c;
	}
	public void setSvoc_cobrandexternalid__c(String svoc_cobrandexternalid__c) {
		this.svoc_cobrandexternalid__c = svoc_cobrandexternalid__c;
	}
	public String getSvoc_cardtier__c() {
		return svoc_cardtier__c;
	}
	public void setSvoc_cardtier__c(String svoc_cardtier__c) {
		this.svoc_cardtier__c = svoc_cardtier__c;
	}
	public String getSvoc_membersince__c() {
		return svoc_membersince__c;
	}
	public void setSvoc_membersince__c(String svoc_membersince__c) {
		this.svoc_membersince__c = svoc_membersince__c;
	}
	public String getSvoc_expirationdate__c() {
		return svoc_expirationdate__c;
	}
	public void setSvoc_expirationdate__c(String svoc_expirationdate__c) {
		this.svoc_expirationdate__c = svoc_expirationdate__c;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getLastmodifieddate() {
		return lastmodifieddate;
	}
	public void setLastmodifieddate(Timestamp lastmodifieddate) {
		this.lastmodifieddate = lastmodifieddate;
	}
	public String getSvoc_cuenta__r__svoc_customerid__c() {
		return svoc_cuenta__r__svoc_customerid__c;
	}
	public void setSvoc_cuenta__r__svoc_customerid__c(String svoc_cuenta__r__svoc_customerid__c) {
		this.svoc_cuenta__r__svoc_customerid__c = svoc_cuenta__r__svoc_customerid__c;
	}
	
	@Override
	public String toString() {
		return "Cobrand [id=" + id + ", sfid=" + sfid + ", name=" + name + ", svoc_cuenta__c=" + svoc_cuenta__c
				+ ", svoc_cardnumber__c=" + svoc_cardnumber__c + ", svoc_bank__c=" + svoc_bank__c
				+ ", svoc_customerid__c=" + svoc_customerid__c + ", svoc_cobrandexternalid__c="
				+ svoc_cobrandexternalid__c + ", svoc_cardtier__c=" + svoc_cardtier__c + ", svoc_membersince__c="
				+ svoc_membersince__c + ", svoc_expirationdate__c=" + svoc_expirationdate__c + ", createddate="
				+ createddate + ", lastmodifieddate=" + lastmodifieddate + ", svoc_cuenta__r__svoc_customerid__c="
				+ svoc_cuenta__r__svoc_customerid__c + "]";
	}

}
