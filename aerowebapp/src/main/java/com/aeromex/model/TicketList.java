package com.aeromex.model;

import java.sql.Date;

public class TicketList {
	private String svocComercialnameC;
	private String svocSpidC;
	private String svocRfisubcodeC;
	private String svocPrimarydocnbrC;
	private String svocPaymentmethodC;
	private String svocEmdtypecodeC;
	private String svocStatusC;
	private String svocRficodeC;
	private String svocCurrencyC;
	private String svocDoctypecodeC;
	private String segmentNumberC;
	private String svocTourCodeAccountCodeC;
	private String svocTotalpaidC;
	private Date svocVcrcreatedateC;

	public String getSvocComercialnameC() {
		return svocComercialnameC;
	}

	public void setSvocComercialnameC(String svocComercialnameC) {
		this.svocComercialnameC = svocComercialnameC;
	}

	public String getSvocSpidC() {
		return svocSpidC;
	}

	public void setSvocSpidC(String svocSpidC) {
		this.svocSpidC = svocSpidC;
	}

	public String getSvocRfisubcodeC() {
		return svocRfisubcodeC;
	}

	public void setSvocRfisubcodeC(String svocRfisubcodeC) {
		this.svocRfisubcodeC = svocRfisubcodeC;
	}

	public String getSvocPrimarydocnbrC() {
		return svocPrimarydocnbrC;
	}

	public void setSvocPrimarydocnbrC(String svocPrimarydocnbrC) {
		this.svocPrimarydocnbrC = svocPrimarydocnbrC;
	}

	public String getSvocPaymentmethodC() {
		return svocPaymentmethodC;
	}

	public void setSvocPaymentmethodC(String svocPaymentmethodC) {
		this.svocPaymentmethodC = svocPaymentmethodC;
	}

	public String getSvocEmdtypecodeC() {
		return svocEmdtypecodeC;
	}

	public void setSvocEmdtypecodeC(String svocEmdtypecodeC) {
		this.svocEmdtypecodeC = svocEmdtypecodeC;
	}

	public String getSvocStatusC() {
		return svocStatusC;
	}

	public void setSvocStatusC(String svocStatusC) {
		this.svocStatusC = svocStatusC;
	}

	public String getSvocRficodeC() {
		return svocRficodeC;
	}

	public void setSvocRficodeC(String svocRficodeC) {
		this.svocRficodeC = svocRficodeC;
	}

	public String getSvocCurrencyC() {
		return svocCurrencyC;
	}

	public void setSvocCurrencyC(String svocCurrencyC) {
		this.svocCurrencyC = svocCurrencyC;
	}

	public String getSvocDoctypecodeC() {
		return svocDoctypecodeC;
	}

	public void setSvocDoctypecodeC(String svocDoctypecodeC) {
		this.svocDoctypecodeC = svocDoctypecodeC;
	}

	public String getSegmentNumberC() {
		return segmentNumberC;
	}

	public void setSegmentNumberC(String segmentNumberC) {
		this.segmentNumberC = segmentNumberC;
	}

	public String getSvocTourCodeAccountCodeC() {
		return svocTourCodeAccountCodeC;
	}

	public void setSvocTourCodeAccountCodeC(String svocTourCodeAccountCodeC) {
		this.svocTourCodeAccountCodeC = svocTourCodeAccountCodeC;
	}

	public String getSvocTotalpaidC() {
		return svocTotalpaidC;
	}

	public void setSvocTotalpaidC(String svocTotalpaidC) {
		this.svocTotalpaidC = svocTotalpaidC;
	}

	public Date getSvocVcrcreatedateC() {
		return svocVcrcreatedateC;
	}

	public void setSvocVcrcreatedateC(Date svocVcrcreatedateC) {
		this.svocVcrcreatedateC = svocVcrcreatedateC;
	}

	@Override
	public String toString() {
		return "Ticket [svocComercialnameC=" + svocComercialnameC + ", svocSpidC=" + svocSpidC + ", svocRfisubcodeC="
				+ svocRfisubcodeC + ", svocPrimarydocnbrC=" + svocPrimarydocnbrC + ", svocPaymentmethodC="
				+ svocPaymentmethodC + ", svocEmdtypecodeC=" + svocEmdtypecodeC + ", svocStatusC=" + svocStatusC
				+ ", svocRficodeC=" + svocRficodeC + ", svocCurrencyC=" + svocCurrencyC + ", svocDoctypecodeC="
				+ svocDoctypecodeC + ", segmentNumberC=" + segmentNumberC + ", svocTourCodeAccountCodeC="
				+ svocTourCodeAccountCodeC + ", svocTotalpaidC=" + svocTotalpaidC + ", svocVcrcreatedateC="
				+ svocVcrcreatedateC + "]";
	}

}
