package com.aeromex.model;

import java.util.ArrayList;
import java.util.List;

public class ResponseSales {

	PersonAccount Account = new PersonAccount();
	List<Reservation> SVOC_Reservation__c = new ArrayList<Reservation>();
	
	public PersonAccount getAccount() {
		return Account;
	}
	public void setAccount(PersonAccount account) {
		Account = account;
	}
	public List<Reservation> getSVOC_Reservation__c() {
		return SVOC_Reservation__c;
	}
	public void setSVOC_Reservation__c(List<Reservation> sVOC_Reservation__c) {
		SVOC_Reservation__c = sVOC_Reservation__c;
	}
	
	@Override
	public String toString() {
		return "SalesResponse [Account=" + Account + ", SVOC_Reservation__c=" + SVOC_Reservation__c + "]";
	}
}
