package com.aeromex.model;

import java.util.ArrayList;
import java.util.List;

public class RequestDelta {	
	List<PersonAccount> accounts = new ArrayList<PersonAccount>();

	public List<PersonAccount> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<PersonAccount> accounts) {
		this.accounts = accounts;
	}

	@Override
	public String toString() {
		return "RequestDelta [accounts=" + accounts + "]";
	}
}
