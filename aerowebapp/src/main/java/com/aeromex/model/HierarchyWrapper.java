package com.aeromex.model;

import java.util.ArrayList;
import java.util.List;
import com.aeromex.entity.CobrandHierarchyEntity;
import com.aeromex.entity.FrequentFlyerHierarchyEntity;

public class HierarchyWrapper {
	List<CobrandHierarchyEntity> cobrandhierarchy = new ArrayList<CobrandHierarchyEntity>();
	List<FrequentFlyerHierarchyEntity> frequentFlyerHierarchy = new ArrayList<FrequentFlyerHierarchyEntity>();
	
	public List<CobrandHierarchyEntity> getCobrandhierarchy() {
		return cobrandhierarchy;
	}
	public void setCobrandhierarchy(List<CobrandHierarchyEntity> cobrandhierarchy) {
		this.cobrandhierarchy = cobrandhierarchy;
	}
	public List<FrequentFlyerHierarchyEntity> getFrequentFlyerHierarchy() {
		return frequentFlyerHierarchy;
	}
	public void setFrequentFlyerHierarchy(List<FrequentFlyerHierarchyEntity> frequentFlyerHierarchy) {
		this.frequentFlyerHierarchy = frequentFlyerHierarchy;
	}
	
	@Override
	public String toString() {
		return "HierarchyWrapper [cobrandhierarchy=" + cobrandhierarchy + ", frequentFlyerHierarchy="
				+ frequentFlyerHierarchy + "]";
	}
}
