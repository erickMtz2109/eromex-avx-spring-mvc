package com.aeromex.model;

public class ResponseDeltaAsync {
	private String folio;
	private Integer records;
	
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public Integer getRecords() {
		return records;
	}
	public void setRecords(Integer records) {
		this.records = records;
	}
	@Override
	public String toString() {
		return "ResponseDeltaAsync [folio=" + folio + ", records=" + records + "]";
	}
}
