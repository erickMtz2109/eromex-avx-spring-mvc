package com.aeromex.model;

import java.util.ArrayList;
import java.util.List;

public class RequestCatalogs {	
	List<Aircraft> Aircrafts = new ArrayList<Aircraft>();

	public List<Aircraft> getAircrafts() {
		return Aircrafts;
	}
	public void setAircrafts(List<Aircraft> aircrafts) {
		Aircrafts = aircrafts;
	}

	@Override
	public String toString() {
		return "RequestCatalogs [Aircrafts=" + Aircrafts + "]";
	}
}
