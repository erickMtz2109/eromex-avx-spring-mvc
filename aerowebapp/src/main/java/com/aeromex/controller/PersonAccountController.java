package com.aeromex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aeromex.dao.PersonAccountDao;
import com.aeromex.entity.PersonAccountEntity;
import com.aeromex.entity.RegresoAltasAnalitica;
import com.aeromex.model.FindAccount;
import com.aeromex.service.PersonAccountService;

@Controller
public class PersonAccountController {
	@Autowired
	PersonAccountService personAccountService;
	@Autowired
	PersonAccountDao personAccDao;

	@RequestMapping(value = "/api/deltas", method = RequestMethod.GET)
	public ResponseEntity<List<PersonAccountEntity>> getAccountsDeltas() {
		List<PersonAccountEntity> accounts = personAccountService.getDeltas();
		return new ResponseEntity<List<PersonAccountEntity>>(accounts, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/deltasFromSFDC", method = RequestMethod.GET)
	public ResponseEntity<List<PersonAccountEntity>> getAccountsDeltasFromSFDC() {
		List<PersonAccountEntity> accounts = personAccDao.getCreatedDeltasInSFDC();
		return new ResponseEntity<List<PersonAccountEntity>>(accounts, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/accounts/find", method = RequestMethod.POST)
	public ResponseEntity<List<FindAccount>> createAccount(@RequestBody List<FindAccount> passenger) {
		List<FindAccount> findAccObj = personAccountService.findAccounts(passenger);
	  return new  ResponseEntity<List<FindAccount>>(findAccObj, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/altasAnalitica", method = RequestMethod.GET)
	public ResponseEntity<List<RegresoAltasAnalitica>> getaltasAnalitica() {
		List<RegresoAltasAnalitica> accounts = personAccountService.getAltasAnalitica();
		return new ResponseEntity<List<RegresoAltasAnalitica>>(accounts, HttpStatus.OK);
	}


}
