package com.aeromex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.aeromex.entity.ErrorProcessDeltas;
import com.aeromex.model.PersonAccount;
import com.aeromex.model.ResponseDeltaAsync;
import com.aeromex.service.DeltaPersonAccountService;

@EnableAsync
@Controller
public class BigDeltaController {
	@Autowired
	DeltaPersonAccountService deltPAccountService;

	@RequestMapping(value = "/api/createBigDelta", method = RequestMethod.POST)
	public ResponseEntity<ResponseDeltaAsync> createBigDelta(@RequestParam String dml_type, @RequestBody List<PersonAccount> personAccountList) {
		ProcessDeltasUtil.buildProcessUtil();
		ErrorProcessDeltas.buildProcessError();
		ResponseDeltaAsync response = new ResponseDeltaAsync();
		response.setFolio(ProcessDeltasUtil.procesoDeltas.folio);
		deltPAccountService.bulkBigDeltas(personAccountList, "Hilo 01", dml_type);
		response.setRecords(personAccountList.size());
		return new ResponseEntity<ResponseDeltaAsync>(response, HttpStatus.OK);
	}

	
	
//	@RequestMapping(value = "/api/createBigDelta", method = RequestMethod.POST)
//	public ResponseEntity<ResponseDeltaAsync> createBigDeltaHilos(@RequestParam String dml_type, @RequestBody List<PersonAccount> personAccountList) {
//		ProcessDeltasUtil.buildProcessUtil();
//		ErrorProcessDeltas.buildProcessError();
//		ResponseDeltaAsync response = new ResponseDeltaAsync();
//		response.setFolio(ProcessDeltasUtil.procesoDeltas.folio);
//		response.setRecords(personAccountList.size());		
//		
//		if (personAccountList != null && !personAccountList.isEmpty()) {
//			if (personAccountList.size() > 3) {
//				int middlePoint_a = middlePoint(personAccountList.size());
//				List<PersonAccount> sub_a = personAccountList.subList(0, middlePoint_a);
//				List<PersonAccount> sub_b = personAccountList.subList(middlePoint_a, personAccountList.size());
//				int middlePoint_1 = middlePoint(sub_a.size());
//				int middlePoint_2 = middlePoint(sub_b.size());
//				List<PersonAccount> sub_1 = sub_a.subList(0, middlePoint_1);
//				List<PersonAccount> sub_2 = sub_a.subList(middlePoint_1, sub_a.size());
//				List<PersonAccount> sub_3 = sub_b.subList(0, middlePoint_2);
//				List<PersonAccount> sub_4 = sub_b.subList(middlePoint_2, sub_b.size());
//
//				deltPAccountService.bulkDeltas(sub_1, true, "Hilo 01",dml_type);
//				deltPAccountService.bulkDeltas(sub_2, true, "Hilo 02",dml_type);
//				deltPAccountService.bulkDeltas(sub_3, true, "Hilo 03",dml_type);
//				deltPAccountService.bulkDeltas(sub_4, true, "Hilo 04",dml_type);
//			} else {
//				deltPAccountService.bulkDeltas(personAccountList, true, "Hilo 05", dml_type);
//			}
//
//		} else {
//			System.out.println("Por procesar: 0 ");
//		}
//		return new ResponseEntity<ResponseDeltaAsync>(response, HttpStatus.OK);
//	}
//	
//	private static int middlePoint(int size_) {
//		int middleP = 0;
//		if (size_ % 2 == 1) {
//			size_ += 1;
//			middleP = size_ / 2 - 1;
//		} else {
//			middleP = size_ / 2;
//		}
//		return middleP;
//	}
}
