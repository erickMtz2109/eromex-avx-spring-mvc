package com.aeromex.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.aeromex.model.FrequentFlyer;
import com.aeromex.service.FrequentFlyerService;

//@Controller
public class FrequentFlyerController {
	@Autowired
	FrequentFlyerService frequentFlyerService;

	@RequestMapping(value = "/api/getAllFrequentFlyers", method = RequestMethod.GET)
	public ResponseEntity<List<FrequentFlyer>> getFrequentFlyerLst() {
		List<FrequentFlyer> frequentFlyerLst = frequentFlyerService.getAllFrequentFlyers();
		return new ResponseEntity<List<FrequentFlyer>>(frequentFlyerLst, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/createFrequentFlyers", method = RequestMethod.POST)
	public ResponseEntity<List<FrequentFlyer>> createFrequentFlyers(@RequestBody List<FrequentFlyer> ffs) {
		List<FrequentFlyer> result = frequentFlyerService.createffs(ffs);
		return new ResponseEntity<List<FrequentFlyer>>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/updateFrequentFlyers", method = RequestMethod.PUT)
	public ResponseEntity<List<FrequentFlyer>> updateFrequentFlyers(@RequestBody List<FrequentFlyer> ffs) {
		List<FrequentFlyer> result = frequentFlyerService.updateffs(ffs);
		return new ResponseEntity<List<FrequentFlyer>>(result, HttpStatus.OK);
	}
}
