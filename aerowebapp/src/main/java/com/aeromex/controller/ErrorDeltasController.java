package com.aeromex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aeromex.dao.Bulk_LogDao;
import com.aeromex.dao.PersonAccountDao;
import com.aeromex.entity.SvocLogEntity;
import com.aeromex.model.BigDeltaError;
import com.aeromex.model.ResponseDeltaAsync;
import com.aeromex.service.BulkApiService;
import com.aeromex.service.ErrorsDeltasService;

@EnableAsync
@Controller
public class ErrorDeltasController {
	@Autowired
	PersonAccountDao personAccDao;
	@Autowired
	ErrorsDeltasService errorsServie;
	@Autowired
	Bulk_LogDao bulkLog;
	@Autowired
	BulkApiService bulkService;
	
	@RequestMapping(value = "/api/deltas/error", method = RequestMethod.GET)
	public ResponseEntity<List<SvocLogEntity>> getErrorDeltas() {
		List<SvocLogEntity> errorsLog = personAccDao.getErrorLogs();
		return new ResponseEntity<List<SvocLogEntity>>(errorsLog, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/clearAccountErrors", method = RequestMethod.GET)
	public ResponseEntity<ResponseDeltaAsync> getErorrsclearAcc() {
		ResponseDeltaAsync response = new ResponseDeltaAsync();
//		response.setRecords(0);
		response.setFolio("Comienza ");
		errorsServie.processErorrsSync();
		return new ResponseEntity<ResponseDeltaAsync>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/bulk/error", method = RequestMethod.GET)
	public ResponseEntity<List<BigDeltaError>> getbulkDeltas() {
		List<BigDeltaError> errorLogs = bulkLog.bulksErrors();
		return new ResponseEntity<List<BigDeltaError>>(errorLogs, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/bulk_api/error", method = RequestMethod.GET)
	public ResponseEntity<ResponseDeltaAsync> getErrorBulkApi() {
		ResponseDeltaAsync response = new ResponseDeltaAsync();
		response.setRecords(0);
		response.setFolio("Proces started");
		bulkService.getErrorsBulkApi();
//		List<SvocLogEntity> errorsLog = personAccDao.getErrorLogs();
		return new ResponseEntity<ResponseDeltaAsync>(response, HttpStatus.OK);
	}
}
