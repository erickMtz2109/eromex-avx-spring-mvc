package com.aeromex.controller;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aeromex.model.CReservation;

@Controller
public class ReservationController {

	@RequestMapping(value = "/api/reservation", method = RequestMethod.POST)
	public ResponseEntity<List<CReservation>> createReservetion(@RequestBody List<CReservation> ReserveRequest) {
		return new ResponseEntity<List<CReservation>>(ReserveRequest, HttpStatus.OK);
	}
}
