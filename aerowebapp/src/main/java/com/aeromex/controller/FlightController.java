package com.aeromex.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aeromex.model.Flight;
import com.aeromex.service.FlightService;

//@Controller
public class FlightController {

	@Autowired
	FlightService flightService;

	@RequestMapping(value = "/api/getAllFlights", method = RequestMethod.GET)
	public ResponseEntity<List<Flight>> getFlights() {
		List<Flight> flights = flightService.getAllFlights();
		return new ResponseEntity<List<Flight>>(flights, HttpStatus.OK);
	}

}
