package com.aeromex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.aeromex.entity.SvocLogEntity;
import com.aeromex.service.ErrorLogService;

@EnableAsync
@Controller
public class LogController {
	@Autowired
	ErrorLogService errorLogService;
	
	@RequestMapping(value = "/api/saveLogs", method = RequestMethod.POST)
	public ResponseEntity<String> createAccount(@RequestBody List<SvocLogEntity> logErrorLts) {
		errorLogService.insertErrorService(logErrorLts, null);
	  return new  ResponseEntity<String>("ok", HttpStatus.OK);
	}

}
