package com.aeromex.controller;

import com.aeromex.model.ProcesoDeltas;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ProcessDeltasUtil {

    public static ProcesoDeltas procesoDeltas;

    public static void buildProcessUtil() {
        procesoDeltas = new ProcesoDeltas();
        procesoDeltas.inicio = Calendar.getInstance().getTime();
        procesoDeltas.errores = new ArrayList<>();
        procesoDeltas.folio = String.valueOf(System.currentTimeMillis());
        procesoDeltas.totalProcesadosOK = 0;
        procesoDeltas.totalProcesadosError = 0;

        procesoDeltas.totalCuentas = 0;
        procesoDeltas.totalCobrand = 0;
        procesoDeltas.totalFrequentFlyer = 0;
        procesoDeltas.totalCobrandOK = 0;
        procesoDeltas.totalCobrandError = 0;
        procesoDeltas.totalFrequentFlyerdOK = 0;
        procesoDeltas.totalFrequentFlyerError = 0;
        procesoDeltas.totalProcesadosOK = 0;
        procesoDeltas.totalProcesadosError = 0;

    }

    public static void buildEstadisticas() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY hh:mm:ss");

        if (procesoDeltas.inicio != null && procesoDeltas.fin != null) {
            procesoDeltas.fechaInicio = sdf.format(procesoDeltas.inicio);
            procesoDeltas.fechaFin = sdf.format(procesoDeltas.fin);
            procesoDeltas.tiempoProceso = (procesoDeltas.fin.getTime() - procesoDeltas.inicio.getTime()) / 1000;

        }
    }

}
