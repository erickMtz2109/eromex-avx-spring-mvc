package com.aeromex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aeromex.service.ConnectSalesforce;


@Controller
public class SalesforceTestLoginController {
	
	@Autowired
	ConnectSalesforce connectSfdc;
	
	@RequestMapping(value = "/api/runSalesforceCall", method = RequestMethod.GET)
	public ResponseEntity<String> getCall() {		
		connectSfdc.callLogin();
	  return new  ResponseEntity<>("ok", HttpStatus.OK);
	}

}
