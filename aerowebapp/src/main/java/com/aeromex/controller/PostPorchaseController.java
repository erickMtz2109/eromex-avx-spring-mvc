package com.aeromex.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aeromex.model.PostPorchase;

@Controller
public class PostPorchaseController {
	
	@RequestMapping(value = "/api/postPorchase/create", method = RequestMethod.POST)
	public ResponseEntity<List<PostPorchase>> createAccount(@RequestBody List<PostPorchase> postPorchase) {
	  return new  ResponseEntity<List<PostPorchase>>(postPorchase, HttpStatus.OK);
	}

}
