package com.aeromex.controller;
//import java.util.List;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
 
@EnableAsync
//@Controller
public class SayHelloController {
	@RequestMapping(value = "/api/sayHello", method = RequestMethod.GET)
	public ResponseEntity<String> getAllContacts(@RequestParam String id) {
		String sayHello = "hola " + id + ", buen d�a";
	  return new  ResponseEntity<String>(sayHello, HttpStatus.OK);
	}

}
