package com.aeromex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aeromex.model.Passenger;
import com.aeromex.service.PassengerService;

//@Controller
public class PassengerController {

	@Autowired
	PassengerService passengerService;

	@RequestMapping(value = "/api/getAllPassengers", method = RequestMethod.GET)
	public ResponseEntity<List<Passenger>> getPassengers() {
	  List<Passenger> Passengers = passengerService.getAllPassengers();
	  return new  ResponseEntity<List<Passenger>>(Passengers, HttpStatus.OK);
	}
}
