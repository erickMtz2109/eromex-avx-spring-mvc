package com.aeromex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aeromex.model.Cobrand;
import com.aeromex.service.CobrandService;

//@Controller
public class CobrandController {
	@Autowired
	CobrandService CobrandService;
	
	@RequestMapping(value = "/api/getAllCobrands", method = RequestMethod.GET)
	public ResponseEntity<List<Cobrand>> getCobrands() {		
	  List<Cobrand> cobrands = CobrandService.getAllCobrands();
	  return new  ResponseEntity<List<Cobrand>>(cobrands, HttpStatus.OK);
	}
}
