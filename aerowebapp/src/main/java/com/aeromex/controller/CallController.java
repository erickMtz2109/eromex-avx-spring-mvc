package com.aeromex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aeromex.dao.PersonAccountDao;
import com.aeromex.model.PersonAccount;
import com.aeromex.service.DeltaPersonAccountService;
import com.aeromex.service.PNR_AttributeService;

@Controller
public class CallController {
	@Autowired
	PNR_AttributeService pnr_AttService;
	@Autowired
	PersonAccountDao pesonAccDao;
	@Autowired
	DeltaPersonAccountService deltaService;

	@RequestMapping(value = "/api/test", method = RequestMethod.GET)
//	public ResponseEntity<String> getAllCall(@RequestParam int id) {
	public ResponseEntity<String> getAllCall(@RequestBody List<PersonAccount>  personAccountList) {
//		pnr_AttService.callMethod();
		//pesonAccDao.getCreatedDeltasInSFDC();
//		pesonAccDao.getErrorLogs();
//		pesonAccDao.updateRange(id);
//		deltaService.upsertDeltas(personAccountList, false);
	  return new  ResponseEntity<String>("ok", HttpStatus.OK);
	}

}
