package com.aeromex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aeromex.entity.ErrorProcessDeltas;
import com.aeromex.entity.PersonAccountEntity;
import com.aeromex.entity.SvocLogEntity;
import com.aeromex.model.PersonAccount;
import com.aeromex.model.ProcesoDeltas;
import com.aeromex.model.ResponseDeltaAsync;
import com.aeromex.service.DeltaPersonAccountService;

@EnableAsync
@Controller
public class DeltaPersonAccountController {
	@Autowired
	DeltaPersonAccountService deltPAccountService;
	@Autowired
	DeltaPersonAccountService deltaService;

	@RequestMapping(value = "/api/monitor", method = RequestMethod.GET)
	public ResponseEntity<ProcesoDeltas> getMonitorProcess() {
		return new ResponseEntity<ProcesoDeltas>(ProcessDeltasUtil.procesoDeltas, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/monitor/{folio}", method = RequestMethod.GET)
	public ResponseEntity<List<SvocLogEntity>> getErrores(@PathVariable String folio) {
		System.out.println("Recibiendo: " + folio);

		List<SvocLogEntity> errors = deltPAccountService.getErrors(folio);
		return new ResponseEntity<List<SvocLogEntity>>(errors, HttpStatus.OK);
	}

//	@RequestMapping(value = "/api/createDeltas", method = RequestMethod.POST)
//	public ResponseEntity<ResponseDeltaAsync> createDeltas(@RequestBody List<PersonAccount> personAccountList) {
//		ProcessDeltasUtil.buildProcessUtil();
//		ErrorProcessDeltas.buildProcessError();
//		ResponseDeltaAsync response = new ResponseDeltaAsync();
//		response.setFolio(ProcessDeltasUtil.procesoDeltas.folio);
//		deltPAccountService.createOrUpdateDeltas(personAccountList, true);
//		response.setRecords(personAccountList.size());
//		return new ResponseEntity<ResponseDeltaAsync>(response, HttpStatus.OK);
//	}
	
	@RequestMapping(value = "/api/createDeltas", method = RequestMethod.POST)
	public ResponseEntity<ResponseDeltaAsync> createDeltas(@RequestBody List<PersonAccount> personAccountList) {
		ProcessDeltasUtil.buildProcessUtil();
		ErrorProcessDeltas.buildProcessError();
		ResponseDeltaAsync response = new ResponseDeltaAsync();
		String folio = "" + System.currentTimeMillis();
		response.setFolio(folio);

		if (personAccountList != null && !personAccountList.isEmpty()) {
			if (personAccountList.size() > 3) {
				int middlePoint_a = middlePoint(personAccountList.size());
				List<PersonAccount> sub_a = personAccountList.subList(0, middlePoint_a);
				List<PersonAccount> sub_b = personAccountList.subList(middlePoint_a, personAccountList.size());
				int middlePoint_1 = middlePoint(sub_a.size());
				int middlePoint_2 = middlePoint(sub_b.size());
				List<PersonAccount> sub_1 = sub_a.subList(0, middlePoint_1);
				List<PersonAccount> sub_2 = sub_a.subList(middlePoint_1, sub_a.size());
				List<PersonAccount> sub_3 = sub_b.subList(0, middlePoint_2);
				List<PersonAccount> sub_4 = sub_b.subList(middlePoint_2, sub_b.size());

				deltPAccountService.bulkDeltas(sub_1, true, "Hilo 01",folio);
				deltPAccountService.bulkDeltas(sub_2, true, "Hilo 02",folio);
				deltPAccountService.bulkDeltas(sub_3, true, "Hilo 03",folio);
				deltPAccountService.bulkDeltas(sub_4, true, "Hilo 04",folio);
			} else {
				deltPAccountService.bulkDeltas(personAccountList, true, "Hilo 05", folio);
			}

		} else {
			System.out.println("Por procesar: 0 ");
		}
		response.setRecords(personAccountList.size());
		return new ResponseEntity<ResponseDeltaAsync>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/updateDeltas", method = RequestMethod.POST)
	public ResponseEntity<ResponseDeltaAsync> updateDeltas(@RequestBody List<PersonAccount> personAccountList) {
		ProcessDeltasUtil.buildProcessUtil();
		ErrorProcessDeltas.buildProcessError();
		ResponseDeltaAsync response = new ResponseDeltaAsync();
		String folio = "" + System.currentTimeMillis();
		response.setFolio(folio);

		if (personAccountList != null && !personAccountList.isEmpty()) {
			if (personAccountList.size() > 3) {
				int middlePoint_a = middlePoint(personAccountList.size());
				List<PersonAccount> sub_a = personAccountList.subList(0, middlePoint_a);
				List<PersonAccount> sub_b = personAccountList.subList(middlePoint_a, personAccountList.size());
				int middlePoint_1 = middlePoint(sub_a.size());
				int middlePoint_2 = middlePoint(sub_b.size());
				List<PersonAccount> sub_1 = sub_a.subList(0, middlePoint_1);
				List<PersonAccount> sub_2 = sub_a.subList(middlePoint_1, sub_a.size());
				List<PersonAccount> sub_3 = sub_b.subList(0, middlePoint_2);
				List<PersonAccount> sub_4 = sub_b.subList(middlePoint_2, sub_b.size());

				deltPAccountService.bulkDeltas(sub_1, false, "Hilo 01",folio);
				deltPAccountService.bulkDeltas(sub_2, false, "Hilo 02",folio);
				deltPAccountService.bulkDeltas(sub_3, false, "Hilo 03",folio);
				deltPAccountService.bulkDeltas(sub_4, false, "Hilo 04",folio);
			} else {
				deltPAccountService.bulkDeltas(personAccountList, false, "Hilo 05",folio);
			}

		} else {
			System.out.println("Por procesar: 0 ");
		}
		response.setRecords(personAccountList.size());
		return new ResponseEntity<ResponseDeltaAsync>(response, HttpStatus.OK);
	}
        
	private static int middlePoint(int size_) {
		int middleP = 0;
		if (size_ % 2 == 1) {
			size_ += 1;
			middleP = size_ / 2 - 1;
		} else {
			middleP = size_ / 2;
		}
		return middleP;
	}
        
	
	@RequestMapping(value = "/api/updatedDeltas", method = RequestMethod.GET)
	public ResponseEntity<List<PersonAccountEntity>> updatedDeltas() {
		List<PersonAccountEntity> personAccountList = deltPAccountService.getUpdatedDeltas();
                
		return new ResponseEntity<List<PersonAccountEntity>>(personAccountList, HttpStatus.OK);
	}
	@RequestMapping(value = "/api/deleteAccount", method = RequestMethod.POST)
	public ResponseEntity<ResponseDeltaAsync> downAccount(@RequestBody List<PersonAccountEntity> paLstToDelete) {
		ProcessDeltasUtil.buildProcessUtil();
		ErrorProcessDeltas.buildProcessError();
		ResponseDeltaAsync response = new ResponseDeltaAsync();
		response.setRecords(paLstToDelete.size());
		deltaService.deleteAccountDeltas(paLstToDelete);
	  return new  ResponseEntity<ResponseDeltaAsync>(response, HttpStatus.OK);
	}
}
