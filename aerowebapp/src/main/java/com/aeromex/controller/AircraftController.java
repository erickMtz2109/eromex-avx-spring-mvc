package com.aeromex.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aeromex.model.Aircraft;
import com.aeromex.service.AircraftService;


//@Controller
public class AircraftController {
	@Autowired
	AircraftService AircraftService;
	
	@RequestMapping(value = "/api/getAllAircraft", method = RequestMethod.GET)
	public ResponseEntity<List<Aircraft>> getAncillaries() {		
	  List<Aircraft> ancillaries = AircraftService.getAllAircrafts();
	  return new  ResponseEntity<List<Aircraft>>(ancillaries, HttpStatus.OK);
	}
}