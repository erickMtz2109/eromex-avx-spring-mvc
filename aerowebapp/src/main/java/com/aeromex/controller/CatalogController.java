package com.aeromex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

//import com.aeromex.model.Aircraft;
import com.aeromex.model.RequestCatalogs;
import com.aeromex.model.ResponseCatalogAsync;
import com.aeromex.service.CatalogsService;

@EnableAsync
@Controller
public class CatalogController {
	@Autowired
	CatalogsService catalogsService;
	
	@RequestMapping(value = "/api/upsertCatalogs", method = RequestMethod.POST)
	public ResponseEntity<ResponseCatalogAsync> upsertDeltas(@RequestBody RequestCatalogs RstCat) {
		catalogsService.upsertCatalogs(RstCat);
		ResponseCatalogAsync res = new ResponseCatalogAsync();
		res.setStatus("200");
		res.setMessage("ok");
		res.setAircraft(RstCat.getAircrafts().size());		
		return new ResponseEntity<ResponseCatalogAsync>(res, HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/api/getCatalog", method = RequestMethod.GET)
//	public ResponseEntity<RequestCatalogs> upsertDeltas() {
//		RequestCatalogs requestCat = new RequestCatalogs();
//		List<Aircraft> rCraftLts = new ArrayList<Aircraft>();
//		Aircraft rCraft = new Aircraft();
//		rCraft.setId(1);
//		rCraft.setName("Name"); 
//		rCraftLts.add(rCraft);
//		requestCat.setAircrafts(rCraftLts);
//		return new ResponseEntity<RequestCatalogs>(requestCat, HttpStatus.OK);
//	}
}