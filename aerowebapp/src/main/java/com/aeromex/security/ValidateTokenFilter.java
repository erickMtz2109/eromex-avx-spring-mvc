/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeromex.security;

import com.aeromex.service.ValidTokenService;
import com.aeromex.service.ValidTokenServiceImpl;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author AVX_Egarcia
 */
@WebFilter(urlPatterns = {"/api/*"})
public class ValidateTokenFilter implements Filter {

    private ServletContext context;

    public void init(FilterConfig fConfig) throws ServletException {
        this.context = fConfig.getServletContext();
        this.context.log("RequestLoggingFilter initialized");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChanin) throws IOException, ServletException {
        String seguridad = System.getenv("AUTH_STATUS");

        if ("TRUE".equals(seguridad)) {

            HttpServletRequest req = (HttpServletRequest) request;
            String token = req.getHeader("Authorization");
            System.out.println("Auth:::::  " + token);

            boolean tokenOK = false;
            if (token != null) {
                ValidTokenService validateTokenService = new ValidTokenServiceImpl();
                tokenOK = validateTokenService.validSalesforceToken(token);
            }

            if (tokenOK) {
                filterChanin.doFilter(request, response);
            } else {
                System.out.println("Token incorrecto");
                System.out.println("Token: " + token);
                HttpServletResponse res = (HttpServletResponse) response;
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Acceso no autorizado");

            }
        } else {
            filterChanin.doFilter(request, response);
        }
    }

    public void destroy() {

    }

}
