package com.aeromex.dao;

import java.util.List;

import com.aeromex.entity.BigFrequentFlyerEntity;
import com.aeromex.entity.FrequentFlyerEntity;

public interface FrequentFlyerDao {
	List<FrequentFlyerEntity> frequentFlyerList();
	List<FrequentFlyerEntity> createFrequentFlyers(List<FrequentFlyerEntity> frequentFlyerTls);
	List<FrequentFlyerEntity> updateFrequentFlyers(List<FrequentFlyerEntity> frequentFlyerTls);
	void upsertPersonAccountDelta(String customerid, FrequentFlyerEntity frequentEntity);	
//	void upsertff(List<FrequentFlyerEntity> fFEntity, int hilo);
	void upsertff(List<FrequentFlyerEntity> fFEntity, String hilo);
	void insertBigFf(List<BigFrequentFlyerEntity> fFEntity, String hilo);
}
