package com.aeromex.dao;

import com.aeromex.entity.SVOCDeltasLogEntity;
import java.util.List;

/**
 *
 * @author AVX_Egarcia
 */
public interface DeltasLogEntityDAO {
    void save(SVOCDeltasLogEntity a);
    List<SVOCDeltasLogEntity> getHistoricoDeltas();
    SVOCDeltasLogEntity getByFolio(String folio);
    SVOCDeltasLogEntity getLastRecordDeltas();
    
}
