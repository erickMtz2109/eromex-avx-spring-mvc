package com.aeromex.dao;

import java.util.List;

import com.aeromex.entity.AircraftEntity;

public interface AircraftDao {
	List<AircraftEntity> AircraftList();
}
