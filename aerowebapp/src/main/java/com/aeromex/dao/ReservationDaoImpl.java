package com.aeromex.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.aeromex.entity.ReservationEntity;
import com.aeromex.model.RequestSales;

import org.springframework.stereotype.Repository;

@Repository
public class ReservationDaoImpl implements ReservationDao{
	
	public List<ReservationEntity> reservationList(){
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();	
		Session session = factory.openSession();
		session.beginTransaction();		
		@SuppressWarnings("unchecked")
		List<ReservationEntity> reservations = (List<ReservationEntity>) session.createQuery("from ReservationEntity").list();		
		session.getTransaction().commit();
		session.close();
		return reservations;
	}
	
	public List<ReservationEntity> getReservRelatedToAccount(String idSfdc) {
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		Session session = factory.openSession();
		@SuppressWarnings("unchecked")
		List<ReservationEntity> reservList = (List<ReservationEntity>) session.createQuery("from ReservationEntity r where r.svoc_account__c=:idSfdc").setParameter("idSfdc", idSfdc).list();
		session.close();
		return reservList;
	}
	
	public ReservationEntity getReservForIntiner(RequestSales resquest) {
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		Session session = factory.openSession();
		ReservationEntity reservList = (ReservationEntity) 
		session.createQuery("from ReservationEntity r where r.svoc_account__c=:account_id and r.svoc_pnr_id__c=:pnr_id")
		.setParameter("account_id", resquest.getAccount_id())
		.setParameter("pnr_id", resquest.getPnr_locator()).uniqueResult();
		session.close();
		return reservList;
	}
}
