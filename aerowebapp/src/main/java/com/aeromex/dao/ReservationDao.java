package com.aeromex.dao;

import java.util.List;
import com.aeromex.entity.ReservationEntity;
import com.aeromex.model.RequestSales;

public interface ReservationDao {
	List<ReservationEntity> reservationList();
	List<ReservationEntity> getReservRelatedToAccount(String idSfdc);
	ReservationEntity getReservForIntiner(RequestSales resquest);
}
