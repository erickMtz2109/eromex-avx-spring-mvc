package com.aeromex.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

import com.aeromex.entity.AircraftEntity;

@Repository
public class AircraftDaoImpl implements AircraftDao{
	
	public List<AircraftEntity> AircraftList(){
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		
		@SuppressWarnings("unchecked")
		List<AircraftEntity> Aircraftes = (List<AircraftEntity>) session.createQuery("from AircraftEntity").list();		
		session.getTransaction().commit();
		session.close();
		return Aircraftes;
	}
	
	
}
