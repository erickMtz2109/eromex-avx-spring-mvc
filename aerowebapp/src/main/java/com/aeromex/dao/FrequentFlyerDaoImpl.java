package com.aeromex.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aeromex.controller.ProcessDeltasUtil;
import com.aeromex.entity.BigFrequentFlyerEntity;
import com.aeromex.entity.ErrorProcessDeltas;
import com.aeromex.entity.FrequentFlyerEntity;
import com.aeromex.entity.SvocLogEntity;
import com.aeromex.utility.ConnectionUtil;

@Repository
public class FrequentFlyerDaoImpl implements FrequentFlyerDao {
        @Autowired
	ErrorLogDao errorLogDao;

	public List<FrequentFlyerEntity> frequentFlyerList() {
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
//		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<FrequentFlyerEntity> frequentFlyerEntityLts = (List<FrequentFlyerEntity>) session
				.createQuery("from FrequentFlyerEntity").list();
		session.getTransaction().commit();
		session.close();

		System.out.println("FriquentF :\n" + frequentFlyerEntityLts.toString());
		return frequentFlyerEntityLts;
	}

	public List<FrequentFlyerEntity> createFrequentFlyers(List<FrequentFlyerEntity> frequentFlyerTls) {
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		for (FrequentFlyerEntity frequentEntityity : frequentFlyerTls) {
			session.save(frequentEntityity);
		}
		session.getTransaction().commit();
		session.close();
		return frequentFlyerTls;
	}

	public List<FrequentFlyerEntity> updateFrequentFlyers(List<FrequentFlyerEntity> frequentFlyerTls) {
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		List<FrequentFlyerEntity> ffobjList = new ArrayList<FrequentFlyerEntity>();
		for (FrequentFlyerEntity ffobj : frequentFlyerTls) {
			FrequentFlyerEntity frequentEntityity = (FrequentFlyerEntity) session
					.createQuery("from FrequentFlyerEntity ff where ff.name=:name")
					.setParameter("name", ffobj.getName()).uniqueResult();
			ffobjList.add(updateffObj(frequentEntityity, ffobj));
		}
		session.update(ffobjList);
		session.getTransaction().commit();
		session.close();
		return frequentFlyerTls;
	}

	private static FrequentFlyerEntity updateffObj(FrequentFlyerEntity frequentEntityity, FrequentFlyerEntity ffobj) {
		frequentEntityity.setSvoc_frequentflyerprogram__c(ffobj.getSvoc_frequentflyerprogram__c());
//		frequentEntityity.setSvoc_expirationDate__c(ffobj.getSvoc_expirationDate__c());
//		frequentEntityity.setSvoc_clubpremierpoints__c(ffobj.getSvoc_clubpremierpoints__c());
//		frequentEntityity.setSvoc_nextlevelpoints__c(ffobj.getSvoc_nextlevelpoints__c());
		frequentEntityity.setSvoc_frequentflyertier__c(ffobj.getSvoc_frequentflyertier__c());
//		frequentEntityity.setSvoc_membersince__c(ffobj.getSvoc_membersince__c());
//		frequentEntityity.setSovc_email__c(ffobj.getSovc_email__c());
		frequentEntityity.setSvoc_cuenta__c(ffobj.getSvoc_cuenta__c());
//		frequentEntityity.setSvoc_ismigrated__c(ffobj.getSvoc_ismigrated__c());
		frequentEntityity.setSvoc_ffn_external_id__c(ffobj.getSvoc_ffn_external_id__c());
		return frequentEntityity;
	}

	public void upsertPersonAccountDelta(String customerid, FrequentFlyerEntity frequentEntity) {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

                try {
                 frequentEntity.setSvoc_cuenta__r__svoc_customerid__c(customerid);
    //                 System.out.println("Customer ID: " +  customerid + " frequentEntity: " + frequentEntity);
                    session.saveOrUpdate(frequentEntity);
                    session.getTransaction().commit();
                    session.close();

                    ProcessDeltasUtil.procesoDeltas.totalFrequentFlyerdOK++;
            } catch (HibernateException hex) {
				ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
				String msgError = new StringBuilder().append(hex.getCause()).toString();
				ProcessDeltasUtil.procesoDeltas.errores.add(msgError);
				System.out.println(msgError);
				session.getTransaction().rollback();

				SvocLogEntity log = new SvocLogEntity();
				log.setObject_error("FrequentFlyer");
				log.setCustomer_id(customerid);
				log.setDml_type("create");
				log.setError_message(msgError);
				log.setRecord(frequentEntity.toString());
				log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
				ErrorProcessDeltas.errorlogs.add(log);

				session.close();

                                ProcessDeltasUtil.procesoDeltas.totalFrequentFlyerError++;
			}
        if (session.isConnected()) {
			session.close();
		}
	//	factory.close();
	}

	@Override
	public void upsertff(List<FrequentFlyerEntity> fFEntity, String hilo) {
		List<SvocLogEntity> errorlogs = new ArrayList<>();
		System.out.println("Inicia ff h" + hilo + " " + java.time.LocalTime.now());
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<FrequentFlyerEntity> paList1 = new ArrayList<FrequentFlyerEntity>();
		boolean error = true;
		SvocLogEntity log = new SvocLogEntity();
		int countToCommit = 0;
		for (FrequentFlyerEntity ffEn : fFEntity) {
			if (!error) {
				session.disconnect();
				session.close(); // IEGC
				session = factory.openSession();
				session.beginTransaction();
			}
			countToCommit++;
			try {
				ffEn.setSvoc_cuenta__r__svoc_customerid__c(ffEn.getSvoc_customerid__c());
				session.saveOrUpdate(ffEn);
				paList1.add(ffEn);
				if (paList1.size() > 4999 || countToCommit == fFEntity.size()) {
//					System.out.println("time fush " + System.currentTimeMillis());
					session.flush();
					session.clear();
					session.getTransaction().commit();
					paList1.clear();
					session.beginTransaction();
//					System.out.println("updated frequent -> h" + hilo + " " + paList1.size());
					System.out.println("time fin flush " + System.currentTimeMillis());
				}
				error = true;
//				ProcessDeltasUtil.procesoDeltas.totalProcesadosOK++;
			} catch (Exception e) {
//				System.err.println("Error #:\n\n" + e.getMessage());

				error = false;
				if (tx != null) {
					session.getTransaction().rollback();
					session.disconnect();
					session.close(); // IEGC
					session = factory.openSession();
					session.beginTransaction();
				}
				for (FrequentFlyerEntity ffCorrected : paList1) {
					try {
						session.saveOrUpdate(ffCorrected);
						session.flush();
						session.clear();
						session.getTransaction().commit();
					} catch (Exception ex) {
						String msgError = e.toString();
						// ProcessDeltasUtil.procesoDeltas.errores.add(msgError);
						Integer code_error = null;
						if (msgError.contains("given object has a null identifier")) {
							code_error = 102;// "Customer_id duplicado";
						}
//						System.err.println("Error #:\n\n" + ex.getMessage());
						log = new SvocLogEntity();
						log.setObject_error("frequent f");
						log.setCustomer_id(ffCorrected.getSvoc_customerid__c());
//						log.setDml_type("ff");
						log.setError_message("Frequent no encontrada");
						log.setError_code(code_error);
						log.setRecord("");
						// log.setRecord(ffCorrected.toString());
						log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
						ErrorProcessDeltas.errorlogs.add(log);
					}
				}
				System.out.println("upsert frequent ex-> h" + hilo + " " + paList1.size());
				paList1.clear();
			}
		}
		if (session.isConnected()) {
			session.disconnect();
			session.close();
		}
		errorLogDao.insertErrorFromSvoc(errorlogs, ProcessDeltasUtil.procesoDeltas.folio);
		errorlogs = null;
		System.out.println("Termina ff h" + hilo + " " + java.time.LocalTime.now());
	}

	@Override
	public void insertBigFf(List<BigFrequentFlyerEntity> bigFFEntity, String hilo) {
		List<SvocLogEntity> errorlogs = new ArrayList<>();
		
		System.out.println("Inicia ff h" + hilo + " " + java.time.LocalTime.now());
		Timestamp timeStp = new Timestamp(System.currentTimeMillis());
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<BigFrequentFlyerEntity> paList1 = new ArrayList<BigFrequentFlyerEntity>();
		boolean error = true;
		SvocLogEntity log = new SvocLogEntity();
		int countToCommit = 0;
		for (BigFrequentFlyerEntity bigffEn : bigFFEntity) {
			if (!error) {
				session.disconnect();
				session.close();
				session = factory.openSession();
				session.beginTransaction();
			}
			countToCommit++;
			try {
				bigffEn.setCreatedDate(timeStp);
				bigffEn.setLastModifiedDate(timeStp);
				session.save(bigffEn);
				paList1.add(bigffEn);
				if (paList1.size() > 4999 || countToCommit == bigFFEntity.size()) {
					session.flush();
					session.clear();
					session.getTransaction().commit();
					paList1.clear();
					session.beginTransaction();
					System.out.println("time fin flush " + System.currentTimeMillis());
				}
				error = true;
			} catch (Exception e) {
				error = false;
				if (tx != null) {
					session.getTransaction().rollback();
					session.disconnect();
					session.close();
					session = factory.openSession();
					session.beginTransaction();
				}
				for (BigFrequentFlyerEntity bigFfCorrected : paList1) {
					try {
						session.save(bigFfCorrected);
						session.flush();
						session.clear();
						session.getTransaction().commit();
					} catch (Exception ex) {
						String msgError = e.toString();
						Integer code_error = null;
						if (msgError.contains("given object has a null identifier")) {
							code_error = 102;
						}
						log = new SvocLogEntity();
						log.setObject_error("frequent f");
						log.setCustomer_id(bigFfCorrected.getSvoc_customerid__c());
						log.setDml_type("big_alta");
						log.setError_message("Frequent no encontrada");
						log.setError_code(code_error);
						log.setRecord("");
						log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
						ErrorProcessDeltas.errorlogs.add(log);
					}
				}
				System.out.println("upsert frequent ex-> h" + hilo + " " + paList1.size());
				paList1.clear();
			}
		}
		if (session.isConnected()) {
			session.disconnect();
			session.close();
		}
		if(!errorlogs.isEmpty()) {
			errorLogDao.insertErrorFromSvoc(errorlogs, ProcessDeltasUtil.procesoDeltas.folio);
			errorlogs = null;
		}
		System.out.println("Termina ff h" + hilo + " " + java.time.LocalTime.now());
	}
}
