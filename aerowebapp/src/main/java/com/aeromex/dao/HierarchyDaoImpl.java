package com.aeromex.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.aeromex.entity.CobrandHierarchyEntity;
import com.aeromex.entity.FrequentFlyerHierarchyEntity;
import com.aeromex.model.HierarchyWrapper;
import com.aeromex.utility.ConnectionUtil;

@Repository
public class HierarchyDaoImpl implements HierarchyDao{
	public HierarchyWrapper getHierarchies() {
		HierarchyWrapper obj = new HierarchyWrapper();
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		
		@SuppressWarnings("unchecked")
		List<CobrandHierarchyEntity> cobrandHierarchy = (List<CobrandHierarchyEntity>) session.createQuery("from CobrandHierarchyEntity order by priority ASC").list();
		@SuppressWarnings("unchecked")
		List<FrequentFlyerHierarchyEntity> frequentFlyerHierarchy = (List<FrequentFlyerHierarchyEntity>) session.createQuery("from FrequentFlyerHierarchyEntity order by priority ASC").list();
		session.close();
		obj.setCobrandhierarchy(cobrandHierarchy);
		obj.setFrequentFlyerHierarchy(frequentFlyerHierarchy);
		return obj;
	}
}
