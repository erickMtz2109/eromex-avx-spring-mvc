package com.aeromex.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aeromex.controller.ProcessDeltasUtil;
import com.aeromex.entity.BigCobrandEntity;
import com.aeromex.entity.CobrandEntity;
import com.aeromex.entity.ErrorProcessDeltas;
import com.aeromex.entity.SvocLogEntity;
import com.aeromex.utility.ConnectionUtil;

@Repository
public class CobrandDaoImpl implements CobrandDao {
        @Autowired
	ErrorLogDao errorLogDao;

	public List<CobrandEntity> cobrandList() {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<CobrandEntity> cobrandLts = (List<CobrandEntity>) session.createQuery("from CobrandEntity").list();
		session.getTransaction().commit();
		session.close();
		return cobrandLts;
	}

	public void upsertPersonAccountDelta(String customerid, CobrandEntity cobrandEntity) {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		
		try {
			cobrandEntity.setSvoc_cuenta__r__svoc_customerid__c(customerid);
			session.saveOrUpdate(cobrandEntity);
			session.getTransaction().commit();
			session.close();

			ProcessDeltasUtil.procesoDeltas.totalCobrandOK++;
		} catch (HibernateException hex) {
			ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
			String msgError = new StringBuilder().append(hex.getCause()).toString();
			ProcessDeltasUtil.procesoDeltas.errores.add(msgError);
			System.out.println(msgError);
			session.getTransaction().rollback();

			SvocLogEntity log = new SvocLogEntity();
			log.setObject_error("Cobrand");
			log.setCustomer_id(customerid);
			log.setDml_type("createOrUpdate");
			log.setError_message(msgError);
			log.setRecord(cobrandEntity.toString());
			log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
			ErrorProcessDeltas.errorlogs.add(log);
			ProcessDeltasUtil.procesoDeltas.totalCobrandError++;
			session.close();
		}
		if (session.isConnected()) {
			session.close();
		}

	}

	@Override
	public void upsertCobrand(List<CobrandEntity> cobrandEntityList, String hilo) {
		List<SvocLogEntity> errorlogs = new ArrayList<>();
		System.out.println("Inicia cobrand h" + hilo + " " + System.currentTimeMillis());
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<CobrandEntity> paList1 = new ArrayList<CobrandEntity>();
		boolean error = true;
		SvocLogEntity log = new SvocLogEntity();
		int countToCommit = 0;
		for (CobrandEntity cobEn : cobrandEntityList) {
			if (!error) {
				session.disconnect();
				session.close();
				session = factory.openSession();
				session.beginTransaction();
			}
			countToCommit++;
			try {
				cobEn.setSvoc_cuenta__r__svoc_customerid__c(cobEn.getSvoc_customerid__c());
				session.saveOrUpdate(cobEn);
				paList1.add(cobEn);
				if (paList1.size() > 4999 || countToCommit == cobrandEntityList.size()) {
					session.flush();
					session.clear();
					session.getTransaction().commit();
					paList1.clear();
					session.beginTransaction();
					System.out.println("updated cobrand -> h" + hilo + " " + paList1.size());
				}
				error = true;
			} catch (Exception e) {
				error = false;
				if (tx != null) {
					session.getTransaction().rollback();
					session.disconnect();
					session.close();
					session = factory.openSession();
					session.beginTransaction();
				}
				for (CobrandEntity cobrandEN : paList1) {
					try {
						session.saveOrUpdate(cobrandEN);
						session.flush();
						session.clear();
						session.getTransaction().commit();
					} catch (Exception ex) {
						String msgError = e.toString();

						Integer code_error = null;
						if (msgError.contains("given object has a null identifier")) {
							code_error = 102;
						}
						log = new SvocLogEntity();
						log.setObject_error("cobrand");
						log.setCustomer_id(cobrandEN.getSvoc_customerid__c());
//						log.setDml_type("ff");
						log.setError_message("Cobrand no encontrada");
						log.setError_code(code_error);
						// log.setRecord(cobrandEN.toString());
						log.setRecord("");
						log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
						errorlogs.add(log);
					}
				}
				System.out.println("upsert cobrand ex-> h" + hilo + " " + paList1.size());
				paList1.clear();
			}
		}
		if (session.isConnected()) {
			session.disconnect();
			session.close();
		}
		if (!errorlogs.isEmpty()) {
			errorLogDao.insertErrorFromSvoc(errorlogs, ProcessDeltasUtil.procesoDeltas.folio);
			errorlogs = null;
		}
		System.out.println("Termina cobrand h" + hilo + " " + System.currentTimeMillis());
	}
	
	@Override
	public void insertBigCobrand(List<BigCobrandEntity> cbrEntList, String hilo) {
		List<SvocLogEntity> errorlogs = new ArrayList<>();
		Timestamp timeStp = new Timestamp(System.currentTimeMillis());
		System.out.println("BIG Inicia cobrand h" + hilo + " " + java.time.LocalTime.now());
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<BigCobrandEntity> paList1 = new ArrayList<BigCobrandEntity>();
		boolean error = true;
		SvocLogEntity log = new SvocLogEntity();
		int countToCommit = 0;
		for (BigCobrandEntity bigCobEn : cbrEntList) {
			if (!error) {
				session.disconnect();
				session.close();
				session = factory.openSession();
				session.beginTransaction();
			}
			countToCommit++;
			try {
				bigCobEn.setCreateddate(timeStp);
				bigCobEn.setLastmodifieddate(timeStp);
				session.save(bigCobEn);
				paList1.add(bigCobEn);
				if (paList1.size() > 4999 || countToCommit == cbrEntList.size()) {
					session.flush();
					session.clear();
					session.getTransaction().commit();
					session.beginTransaction();
					System.out.println("BIG insert cobrand -> h" + hilo + " " + paList1.size());
					paList1.clear();
				}
				error = true;
			} catch (Exception e) {
				error = false;
				if (tx != null) {
					session.getTransaction().rollback();
					session.disconnect();
					session.close();
					session = factory.openSession();
					session.beginTransaction();
				}
				for (BigCobrandEntity bigCbrEnt : paList1) {
					try {
						session.save(bigCbrEnt);
						session.flush();
						session.clear();
						session.getTransaction().commit();
					} catch (Exception ex) {
						String msgError = e.toString();

						Integer code_error = null;
						if (msgError.contains("given object has a null identifier")) {
							code_error = 102;
						}
						log = new SvocLogEntity();
						log.setObject_error("cobrand");
						log.setCustomer_id(bigCbrEnt.getSvoc_customerid__c());
						log.setDml_type("big_alta");
						log.setError_message("Cobrand no encontrada");
						log.setError_code(code_error);
						// log.setRecord(cobrandEN.toString());
						log.setRecord("");
						log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
						errorlogs.add(log);
					}
				}
				System.out.println("BIG insert ex-> h" + hilo + " " + paList1.size());
				paList1.clear();
			}
		}
		if (session.isConnected()) {
			session.disconnect();
			session.close();
		}
		if (!errorlogs.isEmpty()) {
			errorLogDao.insertErrorFromSvoc(errorlogs, ProcessDeltasUtil.procesoDeltas.folio);
			errorlogs = null;
		}
		System.out.println("BIG  Termina cobrand h" + hilo + " " + java.time.LocalTime.now());
	}

}
