package com.aeromex.dao;

import com.aeromex.entity.SVOCDeltasLogEntity;
import com.aeromex.utility.ConnectionUtil;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

/**
 *
 * @author AVX_Egarcia
 */
@Repository
public class DeltasLogEntityDAOImpl implements DeltasLogEntityDAO {

    @Override
    public void save(SVOCDeltasLogEntity a) {
        System.out.println("Save [SVOCDeltasLogEntity]");
        SessionFactory factory = ConnectionUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();

        try {
            session.persist(a);
            tx.commit();
            session.close();
        } catch (Exception e) {
            System.out.println("Error al registrar SVOCDeltasLogEntity: " + e.getMessage());
        }

    }


	@SuppressWarnings("unchecked")
	@Override
    public List<SVOCDeltasLogEntity> getHistoricoDeltas() {
        SessionFactory factory = ConnectionUtil.getSessionFactory();
        Session session = factory.openSession();

        try {
            Query sql = session.createQuery("from SVOCDeltasLogEntity order by inicio DESC");
            
            
            return (List <SVOCDeltasLogEntity>) sql.list();
        } catch (Exception e) {
            System.out.println("Error al obtener getHistoricoDeltas SVOCDeltasLogEntity: " + e.getMessage());
        }

        return new ArrayList<>();

    }

    @Override
    public SVOCDeltasLogEntity getByFolio(String folio) {
        SessionFactory factory = ConnectionUtil.getSessionFactory();
        Session session = factory.openSession();

        try {
            Query sql = session.createQuery("from SVOCDeltasLogEntity where folio = :folio");
            sql.setParameter("folio", folio);

            return (SVOCDeltasLogEntity) sql.uniqueResult();
        } catch (Exception e) {
            System.out.println("Error al obtener getByFolio SVOCDeltasLogEntity: " + e.getMessage());
        }

        return null;
    }

    @Override
    public SVOCDeltasLogEntity getLastRecordDeltas() {
        SessionFactory factory = ConnectionUtil.getSessionFactory();
        Session session = factory.openSession();

        try {
            Query sql = session.createQuery("from SVOCDeltasLogEntity order by inicio DESC");
            sql.setMaxResults(1);
            
            @SuppressWarnings("unchecked")
			List <SVOCDeltasLogEntity> deltas =  sql.list();
            if(deltas != null && !deltas.isEmpty()){
                return deltas.get(0);
            }
            
            
        } catch (Exception e) {
            System.out.println("Error al obtener getHistoricoDeltas SVOCDeltasLogEntity: " + e.getMessage());
        }

       return null;
        
    }

}
