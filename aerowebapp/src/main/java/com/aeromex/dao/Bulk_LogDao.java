package com.aeromex.dao;

import java.util.List;

import com.aeromex.model.BigDeltaError;

public interface Bulk_LogDao {
	public List<BigDeltaError> bulksErrors();
	public List<BigDeltaError> sfdcError();
}
