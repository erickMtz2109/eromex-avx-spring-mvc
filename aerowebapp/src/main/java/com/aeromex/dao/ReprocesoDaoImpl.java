package com.aeromex.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.aeromex.entity.AccountReactiveEntity;
import com.aeromex.entity.AccountSmallQuery;
import com.aeromex.entity.BigPersonAccountEntity;
import com.aeromex.utility.ConnectionUtil;

@Repository
public class ReprocesoDaoImpl implements ReprocesoDao{

	@Override
	public void bigBulkReproceso(List<BigPersonAccountEntity> pAE) {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		
		List<String> customerIdList = getCustomerIds(pAE);
		Query query = session.createQuery("FROM AccountSmallQuery pa WHERE pa.svoc_customerid__c IN :ids");
		query.setParameterList("ids", customerIdList);
		@SuppressWarnings("unchecked")
		List<AccountSmallQuery> items = query.list();
		if (items != null && items.size() > 0) {
			try {
				for (AccountSmallQuery p : items) {
					AccountReactiveEntity r = new AccountReactiveEntity();
					r.setCustomerID(p.getSvoc_customerid__c());
					r.setSfid(p.getSfid());
					r.setFechaCreacion(new java.util.Date());
					session.save(r);
				}
				session.flush();
				session.clear();
				session.getTransaction().commit();
			} catch (Exception e) {
				System.out.println("Error al registrar los ReactiveList");
				e.printStackTrace();
			}
		}
		if (session.isConnected()) {
			session.disconnect();
			session.close();
		}
	}
	
	private List<String> getCustomerIds(List<BigPersonAccountEntity> pAE){
		List<String> ids = new ArrayList<String>();
		for (BigPersonAccountEntity bigPerson : pAE) {
			ids.add(bigPerson.getSvoc_customerid__c());
		}
		return ids;
	}

}
