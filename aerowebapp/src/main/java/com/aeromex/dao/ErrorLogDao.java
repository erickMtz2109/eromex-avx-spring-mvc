package com.aeromex.dao;

import java.util.List;

import com.aeromex.entity.SvocLogEntity;

public interface ErrorLogDao {
	boolean insertErrorFromSvoc(List<SvocLogEntity> listErros, String folio);
        List<SvocLogEntity> getLogError(String folio);
}
