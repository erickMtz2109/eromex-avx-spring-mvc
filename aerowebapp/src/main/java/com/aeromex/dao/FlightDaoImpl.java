package com.aeromex.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

import com.aeromex.entity.FlightEntity;

@Repository
public class FlightDaoImpl implements FlightDao{

	public List<FlightEntity> flightList(){		
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		
		@SuppressWarnings("unchecked")
		List<FlightEntity> flights = (List<FlightEntity>) session.createQuery("from FlightEntity").list();		
		session.getTransaction().commit();
		session.close();
		return flights;
	}
}
