package com.aeromex.dao;

import java.util.List;

import com.aeromex.entity.FlightEntity;

public interface FlightDao {
	List<FlightEntity> flightList();
}
