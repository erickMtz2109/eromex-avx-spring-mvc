package com.aeromex.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;
import org.springframework.stereotype.Repository;

import com.aeromex.model.BigDeltaError;
import com.aeromex.utility.ConnectionUtil;
import com.aeromex.utility.ValidString;

@Repository
public class Bulk_LogDaoImpl implements Bulk_LogDao{
	private static int LESS_TIME = -30;

	public Bulk_LogDaoImpl() {
		try {
			LESS_TIME = System.getenv("LESS_TIME") != null ? new Integer(System.getenv("LESS_TIME")) : LESS_TIME;
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	@Override
	public List<BigDeltaError> bulksErrors() {
		List<BigDeltaError> logErrorLts = new ArrayList<BigDeltaError>();
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session
				.getSessionFactory();
		@SuppressWarnings("deprecation")
		ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		try {
			System.out.println("Extracting data");
			Connection connection = connectionProvider.getConnection();
			PreparedStatement pst = connection.prepareStatement(
				"SELECT  distinct object, external_id, sfdc_id, success, error,svoc_customerid__c, dml_type " +
				"FROM sync_svoc.bulk_log bulk inner join sync_svoc.account ac on bulk.external_id = ac.svoc_customerid__c " +
				"WHERE object ='Account' and success = false "
				);
			
			ResultSet rs = pst.executeQuery();
			System.out.println("converting data");
			Long count = 0L;
			while (rs.next()) {
				String sfid = "";
				
				String dml_typeStr = rs.getString("dml_type");
				if(dml_typeStr.contains("create")) {
					dml_typeStr = "altas";
				}else if(dml_typeStr.contains("update")) {
					dml_typeStr = "cambios";
				}
				
				int code_error = 0;
				String error_message = rs.getString("error").toLowerCase().trim();
				if ((error_message).contains(("not a valid phone").toLowerCase().trim())) {
					code_error = 201;
					error_message = "Teléfono con formato incorrecto";
				} else if ((error_message).contains(("invalid email address").toLowerCase().trim())) {
					code_error = 202;
					error_message = "Email con formato incorrecto";
				} else if ((error_message).contains(("record type id").toLowerCase().trim())) {
					code_error = 220;
					error_message = "LastName no definido";
				} else if ((error_message).contains(("A means of contact is necessary (Phone, Mobile, Email)").toLowerCase().trim())) {
					code_error = 304;
					error_message = "Es necesario al menos un metodo de contacto";
				} else if ((error_message).contains(("missing: [lastname]").toLowerCase().trim())) {
					code_error = 221;
					error_message = "Record Type ID";
				} else if ((error_message).contains(("country:").toLowerCase().trim())) {
					code_error = 222;
					error_message = "Country";
				} else if ((error_message).contains(("country code mobile:").toLowerCase().trim())) {
					code_error = 223;
					error_message = "Country Code Mobile";
				} else if ((error_message).contains(("country code:").toLowerCase().trim())) {
					code_error = 224;
					error_message = "Country Code:";
				} else if ((error_message).contains(("language:").toLowerCase().trim())) {
					code_error = 225;
					error_message = "Language:";
				} else if ((error_message).contains(("other email:").toLowerCase().trim())) {
					code_error = 226;
					error_message = "other email";
				} else if ((error_message).contains(("office country code:").toLowerCase().trim())) {
					code_error = 227;
					error_message = "office country code:";
				} else if ((error_message).contains(("email company:").toLowerCase().trim())) {
					code_error = 228;
					error_message = "Email Company:";
				} else if ((error_message).contains(("duplicates_detected").toLowerCase().trim())) {
					code_error = 301;
					error_message = "Registro duplicado";
				}else if ((error_message).contains(("cannot_insert_update_activate_entity:sampleaccounttrigger").toLowerCase().trim())) {
					code_error = 303;
					error_message = "Error validacion telefono";
				}else if ((error_message).contains(("invalid_field:foreign key external").toLowerCase().trim())) {
					code_error = 105;
					error_message = "Customer ID no encontrado";
				}else if ((error_message).contains(("id not specified in an update call").toLowerCase().trim())) {
					code_error = 109;
					error_message = "Cuenta no encontrada";
				}else if ((error_message).contains(("complete information, First Name, Last Name and BirthDate").toLowerCase().trim())) {
					code_error = 302;
					error_message = "Nombre o Primer Apellido o Fecha de cumpleaños son requerido";
				}else if ((error_message).contains(("Is not a valid age").toLowerCase().trim())) {
					code_error = 305;
					error_message = "No es una edad valida";
				}else if ((error_message).contains(("The SPID field must have information").toLowerCase().trim())) {
					code_error = 306;
					error_message = "El campo SPID debe tener información";
				}
				
				if(code_error==0) {
					code_error = 99;
					error_message = "Error no identificado";
				}
				
				/******/
				if (code_error == 301) {
					try {
						pst = connection.prepareStatement(
								"SELECT firstname, lastname, personbirthdate, personmobilephone, personemail, phone, "
										+ "svoc_email2__c, svoc_officephone__c, svoc_email3__c "
										+ "FROM sync_svoc.account " + "WHERE svoc_customerid__c = ?");
						pst.setString(1, rs.getString("external_id"));
						ResultSet rs1 = pst.executeQuery();

						while (rs1.next()) {
							pst = connection.prepareStatement("SELECT sfid " + "FROM salesforce.account "
									+ "WHERE firstname = ? and lastname = ? and personbirthdate = ? "
									+ "and (personmobilephone = ? or personemail = ? or phone = ? or svoc_email2__c = ? or "
									+ "svoc_officephone__c = ? or svoc_email3__c = ?)");
							pst.setString(1, rs1.getString("firstname"));
							pst.setString(2, rs1.getString("lastname"));
							pst.setTimestamp(3, rs1.getTimestamp("personbirthdate"));
							pst.setString(4, rs1.getString("personmobilephone"));
							pst.setString(5, rs1.getString("personemail"));
							pst.setString(6, rs1.getString("phone"));
							pst.setString(7, rs1.getString("svoc_email2__c"));
							pst.setString(8, rs1.getString("svoc_officephone__c"));
							pst.setString(9, rs1.getString("svoc_email3__c"));

							ResultSet rs2 = pst.executeQuery();
							while (rs2.next()) {
								sfid += rs2.getString("sfid") + ";";
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				/******/
				if (!ValidString.isNullOrEmpty(sfid)) {
					if (sfid.length() > 1) {
						sfid = sfid.substring(0, sfid.length() - 1);
					}
				}
				count++;
				if (count%500 == 0) {
					System.out.println("Number error: -> " + count);
					System.out.println("salesforce id -> " + sfid);
				}
				
				BigDeltaError logError = new BigDeltaError();
				logError.setSfid(sfid);
//				logError.setId(rs.getString("id"));
				logError.setObject_error("Bulk_Log");
				logError.setCustomer_id(rs.getString("external_id"));
//				logError.setDatetime_error(rs.getTimestamp("systemmodstamp"));
				logError.setDml_type(dml_typeStr);
				logError.setError_message(error_message);
				logError.setError_code(code_error);
				logErrorLts.add(logError);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return logErrorLts;
	}
	
	
	@Override
	public List<BigDeltaError> sfdcError() {
		List<BigDeltaError> logErrorLts = new ArrayList<BigDeltaError>();
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session
				.getSessionFactory();
		@SuppressWarnings("deprecation")
		ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, LESS_TIME);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		Date fecha = cal.getTime();
		
		System.out.println("date -> " + fecha);
		try {
			System.out.println("Extracting data sfdc");
			Connection connection = connectionProvider.getConnection();
		      PreparedStatement pst = connection.prepareStatement(
						"select sfid,svoc_customerid__c, _hc_err, _hc_lastop as lastop, svoc_delete_request__c " + 
						"from salesforce.account " + 
						"where svoc_delete_request__c >= ? and _hc_lastop = 'FAILED'"
						);
			pst.setTimestamp(1, new Timestamp(fecha.getTime()));
			ResultSet rs = pst.executeQuery();
			System.out.println("converting data");
			Long count = 0L;			
			
			while (rs.next()) {
				count++;
				int code_error = 0;
				
				String sfid = rs.getString("sfid");
				
				String error_message = rs.getString("_hc_err").toLowerCase().trim();
				if ((error_message).contains(("not a valid phone").toLowerCase().trim())) {
					code_error = 201;
					error_message = "Teléfono con formato incorrecto";
				} else if ((error_message).contains(("invalid email address").toLowerCase().trim())) {
					code_error = 202;
					error_message = "Email con formato incorrecto";
				} else if ((error_message).contains(("record type id").toLowerCase().trim())) {
					code_error = 220;
					error_message = "LastName no definido";
				} else if ((error_message).contains(("A means of contact is necessary (Phone, Mobile, Email)").toLowerCase().trim())) {
					code_error = 304;
					error_message = "Es necesario al menos un metodo de contacto";
				} else if ((error_message).contains(("missing: [lastname]").toLowerCase().trim())) {
					code_error = 221;
					error_message = "Record Type ID";
				} else if ((error_message).contains(("country:").toLowerCase().trim())) {
					code_error = 222;
					error_message = "Country";
				} else if ((error_message).contains(("country code mobile:").toLowerCase().trim())) {
					code_error = 223;
					error_message = "Country Code Mobile";
				} else if ((error_message).contains(("country code:").toLowerCase().trim())) {
					code_error = 224;
					error_message = "Country Code:";
				} else if ((error_message).contains(("language:").toLowerCase().trim())) {
					code_error = 225;
					error_message = "Language:";
				} else if ((error_message).contains(("other email:").toLowerCase().trim())) {
					code_error = 226;
					error_message = "other email";
				} else if ((error_message).contains(("office country code:").toLowerCase().trim())) {
					code_error = 227;
					error_message = "office country code:";
				} else if ((error_message).contains(("email company:").toLowerCase().trim())) {
					code_error = 228;
					error_message = "Email Company:";
				} else if ((error_message).contains(("duplicates_detected").toLowerCase().trim())) {
					code_error = 301;
					error_message = "Registro duplicado";
				}else if ((error_message).contains(("cannot_insert_update_activate_entity:sampleaccounttrigger").toLowerCase().trim())) {
					code_error = 303;
					error_message = "Error validacion telefono";
				}else if ((error_message).contains(("invalid_field:foreign key external").toLowerCase().trim())) {
					code_error = 105;
					error_message = "Customer ID no encontrado";
				}else if ((error_message).contains(("id not specified in an update call").toLowerCase().trim())) {
					code_error = 109;
					error_message = "Cuenta no encontrada";
				}else if ((error_message).contains(("complete information, First Name, Last Name and BirthDate").toLowerCase().trim())) {
					code_error = 302;
					error_message = "Nombre o Primer Apellido o Fecha de cumpleaños son requerido";
				}else if ((error_message).contains(("valid age").toLowerCase().trim())) {
					code_error = 305;
					error_message = "No es una edad valida";
				}else if ((error_message).contains(("The SPID field must have information").toLowerCase().trim())) {
					code_error = 306;
					error_message = "El campo SPID debe tener información";
				}
				
				if(code_error==0) {
					code_error = 99;
					error_message = "Error no identificado";
				}


				BigDeltaError logError = new BigDeltaError();
				logError.setSfid(sfid);
//				logError.setId(rs.getString("sfid"));
//				logError.setObject_error("");
				logError.setCustomer_id(rs.getString("svoc_customerid__c"));
				logError.setDatetime_error(rs.getTimestamp("svoc_delete_request__c"));
				logError.setDml_type("bajas");
				logError.setError_message(error_message);
				logError.setError_code(code_error);
				logErrorLts.add(logError);
			}
			System.out.println("numero de bajas total -> " + count);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return logErrorLts;
	}	
}
