package com.aeromex.dao;

import java.util.List;

import com.aeromex.entity.BigPersonAccountEntity;

public interface ReprocesoDao {

	void bigBulkReproceso(List<BigPersonAccountEntity> palist);

}
