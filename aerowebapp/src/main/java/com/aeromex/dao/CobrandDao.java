package com.aeromex.dao;

import java.util.List;

import com.aeromex.entity.BigCobrandEntity;
import com.aeromex.entity.CobrandEntity;

public interface CobrandDao {
	List<CobrandEntity> cobrandList();
	void upsertPersonAccountDelta(String customerid, CobrandEntity cobrandEntity);
	void upsertCobrand(List<CobrandEntity> cobrandEntityList, String hilo);
	void insertBigCobrand(List<BigCobrandEntity> cobrandList, String hilo);
}
