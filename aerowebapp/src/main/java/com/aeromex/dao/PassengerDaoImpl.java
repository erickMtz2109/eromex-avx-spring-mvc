package com.aeromex.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

import com.aeromex.entity.PassengerEntity;
import com.aeromex.entity.ReservationEntity;

@Repository
public class PassengerDaoImpl implements PassengerDao{

	public List<PassengerEntity> passengerList(){		
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		
		@SuppressWarnings("unchecked")
		List<PassengerEntity> passengers = (List<PassengerEntity>) session.createQuery("from PassengerEntity").list();		
		session.getTransaction().commit();
		session.close();
		return passengers;
	}
	
	public List<PassengerEntity> getPassengerByReserv(ReservationEntity reservObj) {
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		Session session = factory.openSession();
		@SuppressWarnings("unchecked")
		List<PassengerEntity> pasengerLts = (List<PassengerEntity>) 
		session.createQuery("from PassengerEntity p where p.svoc_reservation__c=:sfdc_id")
		.setParameter("sfdc_id", reservObj.getSfdc_id()).list();
		session.close();
		return pasengerLts;
	}
	
}
