package com.aeromex.dao;
import com.aeromex.model.HierarchyWrapper;

public interface HierarchyDao {
	HierarchyWrapper getHierarchies();
}
