package com.aeromex.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.aeromex.entity.PNR_AttributeEntity;
import com.aeromex.utility.ConnectionUtil;
import com.aeromex.utility.ValidString;

@Repository
public class PNR_AttributeDaoImpl implements PNR_AttributeDao {

	public HashMap<String, Integer> mapAttribute() {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<PNR_AttributeEntity> mapAttributes = (List<PNR_AttributeEntity>) session
				.createQuery("from PNR_AttributeEntity").list();
		session.getTransaction().commit();
		session.close();

		HashMap<String, Integer> attriHash = new HashMap<String, Integer>();
		for (PNR_AttributeEntity obj : mapAttributes) {
			if (obj != null) {
				if (!ValidString.isNullOrEmpty(obj.getPrn_attribute())) {
					attriHash.put(obj.getPrn_attribute(), obj.getPoints());
				}
			}
		}
		return attriHash;
	}
}
