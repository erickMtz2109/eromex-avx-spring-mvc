package com.aeromex.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import com.aeromex.controller.ProcessDeltasUtil;
import com.aeromex.entity.CobrandEntity;
import com.aeromex.entity.FrequentFlyerEntity;
import com.aeromex.entity.PersonAccountEntity;
import com.aeromex.model.Cobrand;
import com.aeromex.model.FrequentFlyer;
import com.aeromex.model.PersonAccount;
import com.aeromex.service.PersonAccountServiceImpl;
import com.aeromex.utility.ConnectionUtil;

@Repository
public class DeltasDaoImpl implements DeltasDao {
	
	public void upsertpAccountraft(List<PersonAccount> pAccounts) {
		SessionFactory factory = ConnectionUtil.getSessionFactory();

		Session session = factory.openSession();
		session.beginTransaction();

		int i = 1;
		for (PersonAccount pAccount : pAccounts) {

			System.out.println(i + " Customer id:  " + pAccount.getSvoc_customerid__c());
			i++;
			try {
				PersonAccountEntity pAccountEnt = (PersonAccountEntity) session
						.createQuery("from PersonAccountEntity air where air.svoc_customerid__c=:externalId")
						.setParameter("externalId", pAccount.getSvoc_customerid__c()).uniqueResult();

				PersonAccountEntity paEntity = PersonAccountServiceImpl.modelToPAEntity(pAccount);
				if (pAccountEnt != null) {
					pAccountEnt = updatePersonAccount(pAccountEnt, paEntity);
					session.update(pAccountEnt);

					if (pAccount.getCobrands() != null && !pAccount.getCobrands().isEmpty()) {
						for (Cobrand cobrand : pAccount.getCobrands()) {
							CobrandEntity cobranrEnt = modelToEntityCob(cobrand);
							CobrandEntity cbdDB = (CobrandEntity) session
									.createQuery(
											"from CobrandEntity cob where cob.svoc_cobrandexternalid__c=:externalId")
									.setParameter("externalId", cobranrEnt.getSvoc_cobrandexternalid__c())
									.uniqueResult();
							if (cbdDB != null) {
								cbdDB = updateCobrand(cbdDB, cobranrEnt);
								session.update(cbdDB);

							} else {
								cobranrEnt.setSvoc_cuenta__r__svoc_customerid__c(pAccountEnt.getSvoc_customerid__c());
								try {
									session.save(cobranrEnt);
								} catch (HibernateException hex) {
									ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
									String msgError = new StringBuilder().append("Customer ID: ")
											.append(pAccount.getSvoc_customerid__c()).append(" ").append("Error: ")
											.append(hex.getMessage()).toString();
									ProcessDeltasUtil.procesoDeltas.errores.add(msgError);
									System.out.println("Error al procesar: " + msgError);
								}
							}
						}
					}

					if (pAccount.getFrequentFlyers() != null && !pAccount.getFrequentFlyers().isEmpty()) {
						for (FrequentFlyer ffObj : pAccount.getFrequentFlyers()) {
							FrequentFlyerEntity ffEnt = modelToEntityFF(ffObj);
							FrequentFlyerEntity ffEntDB = (FrequentFlyerEntity) session
									.createQuery(
											"from FrequentFlyerEntity ff where ff.svoc_ffn_external_id__c=:externalId")
									.setParameter("externalId", ffEnt.getSvoc_ffn_external_id__c()).uniqueResult();
							if (ffEntDB != null) {
								ffEntDB = updateFrequentFlyer(ffEntDB, ffEnt);
								session.update(ffEntDB);

							} else {
								ffEnt.setSvoc_cuenta__r__svoc_customerid__c(pAccountEnt.getSvoc_customerid__c());
								try {
									session.save(ffEnt);
								} catch (HibernateException hex) {
									ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
									String msgError = new StringBuilder().append("ff Customer ID: ")
											.append(pAccount.getSvoc_customerid__c()).append(" ").append("Error: ")
											.append(hex.getMessage()).toString();
									ProcessDeltasUtil.procesoDeltas.errores.add(msgError);

									System.out.println("Error al procesar: " + msgError);
								}
							}
						}
					}
				} else {
					// Create objects
					paEntity.setSfid(null);
					try {
						session.persist(paEntity);

						List<CobrandEntity> cobrandEntList = new ArrayList<CobrandEntity>();
						if (pAccount.getCobrands() != null && !pAccount.getCobrands().isEmpty()) {
							for (Cobrand cobrand : pAccount.getCobrands()) {
								CobrandEntity cobranrEnt = modelToEntityCob(cobrand);
								cobranrEnt.setSvoc_cuenta__r__svoc_customerid__c(paEntity.getSvoc_customerid__c());
								try {
									CobrandEntity cbdDB = (CobrandEntity) session.createQuery(
											"from CobrandEntity cob where cob.svoc_cobrandexternalid__c=:externalId")
											.setParameter("externalId", cobranrEnt.getSvoc_cobrandexternalid__c())
											.uniqueResult();
									if (cbdDB != null) {
										cbdDB = updateCobrand(cbdDB, cobranrEnt);
										session.update(cbdDB);
										cobrandEntList.add(cobranrEnt);

									} else {
										session.persist(cobranrEnt);
										cobrandEntList.add(cobranrEnt);
									}
								} catch (HibernateException hex) {
									ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
									String msgError = new StringBuilder().append("cob Customer ID: ")
											.append(pAccount.getSvoc_customerid__c()).append(" ").append("Error: ")
											.append(hex.getMessage()).toString();
									ProcessDeltasUtil.procesoDeltas.errores.add(msgError);
									System.out.println("Error al procesar: " + msgError);
									session.flush();
								}

							}
//					CobrandEntity cobrandCat = getHighestGerarchyCobrand(session, cobrandEntList);
						}
						if (pAccount.getFrequentFlyers() != null && !pAccount.getFrequentFlyers().isEmpty()) {
							List<FrequentFlyerEntity> frequentEntList = new ArrayList<FrequentFlyerEntity>();
							for (FrequentFlyer ff : pAccount.getFrequentFlyers()) {
								FrequentFlyerEntity ffEnt = modelToEntityFF(ff);
								ffEnt.setSvoc_cuenta__r__svoc_customerid__c(paEntity.getSvoc_customerid__c());
								try {
									FrequentFlyerEntity ffEntDB = (FrequentFlyerEntity) session.createQuery(
											"from FrequentFlyerEntity ff where ff.svoc_ffn_external_id__c=:externalId")
											.setParameter("externalId", ffEnt.getSvoc_ffn_external_id__c())
											.uniqueResult();
									if (ffEntDB != null) {
										ffEntDB = updateFrequentFlyer(ffEntDB, ffEnt);
										session.update(ffEntDB);
										frequentEntList.add(ffEnt);
									} else {
										session.persist(ffEnt);
										frequentEntList.add(ffEnt);
									}
								} catch (HibernateException hex) {
									ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
									String msgError = new StringBuilder().append("ff Customer ID: ")
											.append(pAccount.getSvoc_customerid__c()).append(" ").append("Error: ")
											.append(hex.getMessage()).toString();
									ProcessDeltasUtil.procesoDeltas.errores.add(msgError);

									System.out.println("Error al procesar: " + msgError);
								}

							}
//					FrequentFlyerEntity ffCat = getHighestGerarchyFrequentFlyer(session, frequentEntList);
						}

					} catch (HibernateException hex) {
						ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
						String msgError = new StringBuilder().append("cob Customer ID: ")
								.append(pAccount.getSvoc_customerid__c()).append(" ").append("Error: ")
								.append(hex.getMessage()).toString();
						ProcessDeltasUtil.procesoDeltas.errores.add(msgError);

						System.out.println("Error al procesar: " + msgError);
					}

				}

				ProcessDeltasUtil.procesoDeltas.totalProcesadosOK++;
//				session.getTransaction().commit();

			} catch (Exception ex) {
				ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
				String msgError = new StringBuilder().append("Customer ID: ").append(pAccount.getSvoc_customerid__c())
						.append(" ").append("Error: ").append(ex.getMessage()).toString();
				ProcessDeltasUtil.procesoDeltas.errores.add(msgError);

				System.out.println("Error al procesar: " + msgError);

			}

		}

		session.getTransaction().commit();
		session.close();
	}

	private PersonAccountEntity updatePersonAccount(PersonAccountEntity paE, PersonAccountEntity pa) {
		paE.setSvoc_customerid__c(pa.getSvoc_customerid__c());
		paE.setName(pa.getName());
		paE.setFirstname(pa.getFirstname());
		paE.setMiddlename(pa.getMiddlename());
		paE.setLastname(pa.getLastname());
		paE.setSuffix(pa.getSuffix());
		paE.setSvoc_gaid__c(pa.getSvoc_gaid__c());
		paE.setSvoc_spid__c(pa.getSvoc_spid__c());
		paE.setSvoc_company__c(pa.getSvoc_company__c());
		paE.setSvoc_flowntrips__c(pa.getSvoc_flowntrips__c());
		paE.setSvoc_rfmtotal__c(pa.getSvoc_rfmtotal__c());
		paE.setSvoc_rfm_tier__c(pa.getSvoc_rfm_tier__c());
		paE.setSvoc_nps__c(pa.getSvoc_nps__c());
		paE.setSvoc_language__c(pa.getSvoc_language__c());
		paE.setSvoc_gendercode__c(pa.getSvoc_gendercode__c());
		paE.setSvoc_dominantdestination__c(pa.getSvoc_dominantdestination__c());
		paE.setSvoc_customercategory__c(pa.getSvoc_customercategory__c());
		paE.setSvoc_nationality__c(pa.getSvoc_nationality__c());
		paE.setSvoc_country__c(pa.getSvoc_country__c());
		paE.setPersonbirthdate(pa.getPersonbirthdate());
		paE.setSvoc_countrycodemobile__c(pa.getSvoc_countrycodemobile__c());
		paE.setPersonmobilephone(pa.getPersonmobilephone());
		paE.setSvoc_optin_mobile__c(pa.getSvoc_optin_mobile__c());
		paE.setPersonemail(pa.getPersonemail());
		paE.setSvoc_optin_email__c(pa.getSvoc_optin_email__c());
		paE.setSvoc_countrycode__c(pa.getSvoc_countrycode__c());
		paE.setPhone(pa.getPhone());
		paE.setSvoc_email2__c(pa.getSvoc_email2__c());
		paE.setSvoc_optin_email2__c(pa.getSvoc_optin_email2__c());
		paE.setSvoc_officecountrycode__c(pa.getSvoc_officecountrycode__c());
		paE.setSvoc_officephone__c(pa.getSvoc_officephone__c());
		paE.setSvoc_extension__c(pa.getSvoc_extension__c());
		paE.setSvoc_email3__c(pa.getSvoc_email3__c());
		paE.setSvoc_optincompanyemail__c(pa.getSvoc_optincompanyemail__c());
		paE.setSvoc_passport_number__c(pa.getSvoc_passport_number__c());
		paE.setSvoc_city__c(pa.getSvoc_city__c());
//		paE.setPersonassistantname(pa.getPersonassistantname());
//		paE.setSvoc_countrycodeassistant__c(pa.getSvoc_countrycodeassistant__c());
//		paE.setPersonassistantphone(pa.getPersonassistantphone());
//		paE.setSvoc_assistantemail__c(pa.getSvoc_assistantemail__c());
		paE.setLastmodifieddate(pa.getLastmodifieddate());
		paE.setPersonleadsource(pa.getPersonleadsource());
		paE.setSalutation(pa.getSalutation());
//		paE.setSvoc_employeenumber__c(pa.getSvoc_employeenumber__c());
		paE.setSvoc_contactemergencyname__c(pa.getSvoc_contactemergencyname__c());
		paE.setSvoc_emergencycountrycode__c(pa.getSvoc_emergencycountrycode__c());
		paE.setSvoc_emergencyphone__c(pa.getSvoc_emergencyphone__c());
		paE.setSvoc_emergencyemail__c(pa.getSvoc_emergencyemail__c());
		paE.setSvoc_relationship__c(pa.getSvoc_relationship__c());
//		paE.setPersontitle(pa.getPersontitle());
//		paE.setSvoc_ismigrated__c(pa.getSvoc_ismigrated__c());
		paE.setSvoc_paexternalid__c(pa.getSvoc_paexternalid__c());
		paE.setRecordtypeid(pa.getRecordtypeid());
		paE.setSvoc_purchasepropensity__c(pa.getSvoc_purchasepropensity__c());
		paE.setSvoc_winbackpropensity__c(pa.getSvoc_winbackpropensity__c());
		paE.setSvoc_dominantorigin__c(pa.getSvoc_dominantorigin__c());
		return paE;
	}

	private CobrandEntity updateCobrand(CobrandEntity cbdEnt, CobrandEntity cobranrEnt) {
		cbdEnt.setSvoc_cuenta__c(cobranrEnt.getSvoc_cuenta__c());
		cbdEnt.setSvoc_cardnumber__c(cobranrEnt.getSvoc_cardnumber__c());
		cbdEnt.setSvoc_bank__c(cobranrEnt.getSvoc_bank__c());
		cbdEnt.setSvoc_customerid__c(cobranrEnt.getSvoc_customerid__c());
		cbdEnt.setSvoc_cardtier__c(cobranrEnt.getSvoc_cardtier__c());
		cbdEnt.setSvoc_membersince__c(cobranrEnt.getSvoc_membersince__c());
		cbdEnt.setSvoc_expirationdate__c(cobranrEnt.getSvoc_expirationdate__c());
//		cbdEnt.setSvoc_ismigrated__c(cobranrEnt.getSvoc_ismigrated__c());
		return cbdEnt;
	}

	private FrequentFlyerEntity updateFrequentFlyer(FrequentFlyerEntity ffEntDB, FrequentFlyerEntity ffEnt) {
		ffEntDB.setName(ffEnt.getName());
		ffEntDB.setSvoc_cuenta__c(ffEnt.getSvoc_cuenta__c());
		ffEntDB.setSvoc_customerid__c(ffEnt.getSvoc_customerid__c());
		ffEntDB.setSvoc_ffn_external_id__c(ffEnt.getSvoc_ffn_external_id__c());
		ffEntDB.setSvoc_frequentflyertier__c(ffEnt.getSvoc_frequentflyertier__c());
		ffEntDB.setSvoc_frequentflyerprogram__c(ffEnt.getSvoc_frequentflyerprogram__c());
		ffEntDB.setSvoc_pcemail__c(ffEnt.getSvoc_pcemail__c());
		return ffEntDB;
	}

	private CobrandEntity modelToEntityCob(Cobrand cobrandObj) {
		ModelMapper modelMapper = new ModelMapper();
		CobrandEntity coEnt = modelMapper.map(cobrandObj, CobrandEntity.class);
		return coEnt;
	}

	private FrequentFlyerEntity modelToEntityFF(FrequentFlyer FrequentFlyerObj) {
		ModelMapper modelMapper = new ModelMapper();
		FrequentFlyerEntity ffEnt = modelMapper.map(FrequentFlyerObj, FrequentFlyerEntity.class);
		return ffEnt;
	}

}
