package com.aeromex.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aeromex.controller.ProcessDeltasUtil;
import com.aeromex.entity.AccountReactiveEntity;
import com.aeromex.entity.AccountSmallQuery;
import com.aeromex.entity.BigPersonAccountEntity;
import com.aeromex.entity.ErrorProcessDeltas;
import com.aeromex.entity.PersonAccountEntity;
import com.aeromex.entity.RegresoAltasAnalitica;
import com.aeromex.entity.ReservationEntity;
import com.aeromex.entity.SvocLogEntity;
import com.aeromex.model.FindAccount;
import com.aeromex.model.PassengerFind;
import com.aeromex.utility.ConnectionUtil;
import com.aeromex.utility.ValidString;
import com.fasterxml.jackson.databind.ObjectMapper;

@Repository
public class PersonAccountDaoImpl implements PersonAccountDao {
	@Autowired
	PNR_AttributeDao pnr_AttDao;
	@Autowired
	ErrorLogDao errorLogDao;
        private static final String SQL_UPDATE = "UPDATE salesforce.account "
            + "SET firstname = ? , middlename = ? , lastname = ? , suffix = ? ,\n" +
            "  svoc_spid__c = ? , svoc_company__c = ? , \n" +
            "svoc_flowntrips__c = ? , svoc_rfmtotal__c = ? , svoc_rfm_category__c = ? , \n" +
            "svoc_nps__c = ? , svoc_language__c = ? , svoc_gendercode__c = ? , \n" +
            "svoc_dominantdestination__c = ? , svoc_customercategory__c = ? , \n" +
            "svoc_nationality__c = ? , svoc_country__c = ? , personbirthdate = ? , \n" +
            "svoc_countrycodemobile__c = ? , personmobilephone = ? , \n" +
            "svoc_optin_mobile__c = ? , personemail = ? , svoc_optin_email__c = ? , \n" +
            "svoc_countrycode__c = ? , phone = ? , svoc_email2__c = ? , \n" +
            "svoc_optin_email2__c = ? , svoc_officecountrycode__c = ? , \n" +
            "svoc_officephone__c = ? , svoc_extension__c = ? , svoc_email3__c = ? , \n" +
            "svoc_optincompanyemail__c = ? , svoc_passport_number__c = ? , \n" +
            "svoc_city__c = ? , salutation = ? ,  \n" + /*svoc_employeenumber__c = ? ,*/
            "svoc_contactemergencyname__c = ? , svoc_emergencycountrycode__c = ? , \n" +
            "svoc_emergencyphone__c = ? , svoc_emergencyemail__c = ? , \n" +
            "svoc_relationship__c = ? , recordtypeid = ? , svoc_purchasepropensity__c = ? , \n" +
            "svoc_propensionwinback__c = ? , svoc_dominantorigin__c = ? " + 
            "WHERE sfid = ? and svoc_customerid__c = ?" ;
        
        private static final String SQL_INSERT = "INSERT INTO salesforce.account( firstname , middlename  , lastname  , suffix   ,\n" +
"              svoc_spid__c  , svoc_company__c   ,\n" +
"            svoc_flowntrips__c  , svoc_rfmtotal__c  , svoc_rfm_category__c   ,\n" +
"            svoc_nps__c  , svoc_language__c  , svoc_gendercode__c  , \n" +
"            svoc_dominantdestination__c  , svoc_customercategory__c  , \n" +
"            svoc_nationality__c  , svoc_country__c  , personbirthdate   ,\n" +
"            svoc_countrycodemobile__c  , personmobilephone   ,\n" +
"            svoc_optin_mobile__c  , personemail  , svoc_optin_email__c   ,\n" +
"            svoc_countrycode__c  , phone  , svoc_email2__c   ,\n" +
"            svoc_optin_email2__c  , svoc_officecountrycode__c   ,\n" +
"            svoc_officephone__c  , svoc_extension__c  , svoc_email3__c  , \n" +
"            svoc_optincompanyemail__c  , svoc_passport_number__c   ,\n" +
"            svoc_city__c  , salutation  , \n" + /*svoc_employeenumber__c  ,*/
"            svoc_contactemergencyname__c  , svoc_emergencycountrycode__c  , \n" +
"            svoc_emergencyphone__c  , svoc_emergencyemail__c   ,\n" +
"            svoc_relationship__c  , recordtypeid  , svoc_purchasepropensity__c  , \n" +
"            svoc_propensionwinback__c  , svoc_dominantorigin__c, svoc_customerid__c ,svoc_isactive__c,createddate)\n" +
"	VALUES(?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?,?,?,true,current_timestamp)";
	
//	private static int LESS_TIME = -19;
	private static int LESS_TIME = -72;
	
	public PersonAccountDaoImpl() {
		try {
		LESS_TIME = System.getenv("LESS_TIME")!=null?new Integer(System.getenv("LESS_TIME")):LESS_TIME;
		}catch(Exception exc) {
			exc.printStackTrace();
		}
	}
	
	@Override
	public boolean createDeltas(PersonAccountEntity pAE) {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		Timestamp timeS = new Timestamp(System.currentTimeMillis());
		try {
			pAE.setSvoc_isactive__c(true);
			pAE.setCreateddate(timeS);
			pAE.setSfid(null);
			session.persist(pAE);
			session.getTransaction().commit();
			session.close();
			ProcessDeltasUtil.procesoDeltas.totalProcesadosOK++;
		} catch (Exception hex) {
			hex.printStackTrace();
			ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
			String msgError = new StringBuilder().append(hex.getCause()).toString();
			ProcessDeltasUtil.procesoDeltas.errores.add(msgError);
			System.out.println("Error al insertar: " + msgError);
			session.getTransaction().rollback();
			Integer code_error= null;
			if(msgError.contains("duplicate key value")) {
				code_error = 101;//"Customer_id duplicado";
			}

			SvocLogEntity log = new SvocLogEntity();
			log.setObject_error("person account");
			log.setCustomer_id(pAE.getSvoc_customerid__c());
			log.setDml_type("altas");
			log.setError_message(msgError);
			log.setError_code(code_error);
//			log.setRecord(pAE.toString());
			log.setDatetime_error(timeS);
			ErrorProcessDeltas.errorlogs.add(log);
			return false;
		} finally {
			if (session.isConnected()) {
				session.close();
			}
		}
		return true;
	}
	
	@Override
	public boolean updateDeltas(PersonAccountEntity pAE) {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		try {
			pAE.setSvoc_isactive__c(true);
			session.update(pAE);
			session.getTransaction().commit();
			session.close();

			ProcessDeltasUtil.procesoDeltas.totalProcesadosOK++;
		} catch (HibernateException hex) {
			ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
			String msgError = hex.toString();
			ProcessDeltasUtil.procesoDeltas.errores.add(msgError);
			System.out.println("Error al update: " + msgError);
			session.getTransaction().rollback();
			Integer code_error= null;
			if(msgError.contains("duplicate key value")) {
				code_error = 101;//"Customer_id duplicado";
			}
			if(msgError.contains("given object has a null identifier")) {
				code_error = 102;//"Customer_id duplicado";
			}
			SvocLogEntity log = new SvocLogEntity();
			log.setObject_error("person account");
			log.setCustomer_id(pAE.getSvoc_customerid__c());
			log.setDml_type("cambios");
			log.setError_message(msgError);
			log.setError_code(code_error);
//			log.setRecord(pAE.toString());
			log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
			ErrorProcessDeltas.errorlogs.add(log);
			return false;
		} finally {
			if (session.isConnected()) {
				session.close();
			}
		}
		return true;
	}
	

	public List<PersonAccountEntity> personAccountList() {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<PersonAccountEntity> personAccountLts = (List<PersonAccountEntity>) session
				.createQuery("from PersonAccountEntity").setMaxResults(500).list();
		session.getTransaction().commit();
		session.close();
		return personAccountLts;
	}

	public List<SvocLogEntity> getErrorLogs() {
//		if(clearPersonAccountHerokuErrors()) {
//			deleteCreatedFail();
//		}

		List<SvocLogEntity> logErrorLts = new ArrayList<SvocLogEntity>();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, LESS_TIME);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date fecha = cal.getTime();
        
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		@SuppressWarnings("deprecation")
		ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();		
		try {
			Connection connection = connectionProvider.getConnection();
			PreparedStatement pst = connection
					.prepareStatement("SELECT id, customer_id, dml_type, error_code " 
							+ "FROM svoc.log_svoc "
							+ "WHERE datetime_error >= ? and dml_type != ? ");
			pst.setTimestamp(1, new Timestamp(fecha.getTime()));
			pst.setString(2, "m_dltac");
			ResultSet rs = pst.executeQuery();
			
			while (rs.next()) {				
				SvocLogEntity logError = new SvocLogEntity();
				logError.setId(rs.getString("id"));
				logError.setDml_type(rs.getString("dml_type"));
				Integer errorCode = rs.getInt("error_code");	
				logError.setError_code(errorCode);
				logError.setCustomer_id(rs.getString("customer_id"));
				if(errorCode!=null) {
					if (errorCode == 101) {
						logError.setError_message("CustomerID Duplicado");
					} else if (errorCode == 102) {
						logError.setError_message("CustomerID vac�o");
					} else if (errorCode == 103) {
						logError.setError_message("Salesforce ID no encontrado");
					} else if (errorCode == 104) {
						logError.setError_message("Salesforce ID no encontrado");
					} else if (errorCode == 201) {
						logError.setError_message("Tel�fono con formato incorrecto");
					} else if (errorCode == 202) {
						logError.setError_message("Email con formato incorrecto");
					} else if (errorCode == 220) {
						logError.setError_message("LastName no definido");
					} else if (errorCode == 221) {
						logError.setError_message("Record Type ID");
					} else if (errorCode == 222) {
						logError.setError_message("Country");
					} else if (errorCode == 223) {
						logError.setError_message("Country Code Mobile");
					}
					if(logError.getError_code()>0) {
					logErrorLts.add(logError);
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}		
		return logErrorLts;
	}
	
	private void deleteCreatedFail() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, LESS_TIME);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		Date fecha = cal.getTime();
		
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		/*getCreatedFailed*/
		try {
		@SuppressWarnings("unchecked")
		List<PersonAccountEntity> createdDeltasError = (List<PersonAccountEntity>) session
				.createQuery("FROM PersonAccountEntity "
				+ "WHERE createddate >=:time AND _hc_err IS NOT NULL AND _hc_err LIKE '%INSERT%' and svoc_customerid__c is not null")
				.setParameter("time", fecha).list();
		for(PersonAccountEntity ctdDeltasError : createdDeltasError) {
			session.delete(ctdDeltasError);
		}
		session.getTransaction().commit();
		session.close();
		}catch(HibernateException hx) {
			hx.printStackTrace();
			session.getTransaction().rollback();
			if(session.isConnected()) {
				session.close();
			}
		}
		if(session.isConnected()) {
			session.close();
		}
	}

	public List<PersonAccountEntity> updatePersonAccout(List<PersonAccountEntity> pAccLts) {
		List<PersonAccountEntity> pAccLtsReturned = new ArrayList<PersonAccountEntity>();
		return pAccLtsReturned;
	}

	public List<PersonAccountEntity> getAccoutByReserv(ReservationEntity reservObj) {
		@SuppressWarnings("deprecation")
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		Session session = factory.openSession();
		@SuppressWarnings("unchecked")
		List<PersonAccountEntity> pAccSpec = (List<PersonAccountEntity>) session
				.createQuery("from PersonAccountEntity pa where pa.svoc_reservation__c=:idSfdc")
				.setParameter("idSfdc", reservObj.getSfdc_id()).list();
		session.close();
		return pAccSpec;
	}

	public int upsertPersonAccountDelta(PersonAccountEntity pAE) {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		// Es un PersonAccount con SFID, entonces se actualiza
		if (pAE.getSfid() != null && !pAE.getSfid().isEmpty()) {
			try {
				session.update(pAE);
				session.getTransaction().commit();
				session.close();

				ProcessDeltasUtil.procesoDeltas.totalProcesadosOK++;
			} catch (HibernateException hex) {
				ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
				String msgError = hex.toString();				
				ProcessDeltasUtil.procesoDeltas.errores.add(msgError);
				System.out.println("Error al update: " + msgError);
				session.getTransaction().rollback();

				SvocLogEntity log = new SvocLogEntity();
				log.setObject_error("PersonAccount");
				log.setCustomer_id(pAE.getSvoc_customerid__c());
				log.setDml_type("cambios");
				log.setError_message(msgError);
//				log.setRecord(pAE.toString());
				log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
				ErrorProcessDeltas.errorlogs.add(log);

				return -1;
			}
		} else {
			try {
				pAE.setSfid(null);
				session.persist(pAE);
				session.getTransaction().commit();
				session.close();
				ProcessDeltasUtil.procesoDeltas.totalProcesadosOK++;
			} catch (HibernateException hex) {
				ProcessDeltasUtil.procesoDeltas.totalProcesadosError++;
				String msgError = new StringBuilder().append(hex.getCause()).toString();
				ProcessDeltasUtil.procesoDeltas.errores.add(msgError);
				System.out.println("Error al insertar: " + msgError);
				session.getTransaction().rollback();

				SvocLogEntity log = new SvocLogEntity();
				log.setObject_error("PersonAccount");
				log.setCustomer_id(pAE.getSvoc_customerid__c());
				log.setDml_type("cambios");
				log.setError_message(msgError);
				log.setRecord(pAE.toString());
				log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
				ErrorProcessDeltas.errorlogs.add(log);
				return -1;
			}
		}
		if (session.isConnected()) {
			session.close();
		}
		return 1;
	}

//	public void upserthierarchiesCobrandAndFrequentFlyer(String externalIdAcc, HierarchyWrapper hierarchyobj) {
//	}
	
	//Method Deplicated for us
	@Override
	public List<PersonAccountEntity> getDeltas(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, LESS_TIME);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		Date fecha = cal.getTime();
		
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<PersonAccountEntity> deltas = (List<PersonAccountEntity>) session
				.createQuery("from PersonAccountEntity WHERE _hc_err IS NULL AND "
						+ "svoc_customerid__c IS NOT NULL AND createddate >=:time AND recordtypeid =:recordType")
						.setParameter("recordType", System.getenv("RECORDTYPE_PERSON_ACCOUNT"))
						.setParameter("time", fecha).list();
//						+ System.getenv("RECORDTYPE_PERSON_ACCOUNT")).list();
		session.getTransaction().commit();
		session.close();
		return deltas;
	}

	public List<FindAccount> findAccountsDao(List<FindAccount> fAccLts) {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		PersonAccountEntity pAccSpec = null;

		for (FindAccount reservation : fAccLts) {
//			HashMap<String, Integer> attributeMap = pnr_AttDao.mapAttribute();
//			int passengerCounter = 0;
//			int totalsum = 0;
//			double reservationAveragePriority = 0;

			for (PassengerFind passen : reservation.getPassengers()) {
				if (!ValidString.isNullOrEmpty(passen.getFrequentflyernumber())) {
					if (!ValidString.isNullOrEmpty(passen.getFrequentflyernumber())) {
						pAccSpec = (PersonAccountEntity) session
								.createQuery("from PersonAccountEntity pa where pa.svoc_frequentflyernumber__c=:ffn"
										+ " AND pa.sfid IS NOT NULL")
								.setParameter("ffn", passen.getFrequentflyernumber()).setMaxResults(1).uniqueResult();
					}
				}
				if (pAccSpec == null) {
					if (!ValidString.isNullOrEmpty(passen.getEmail())) {
						pAccSpec = (PersonAccountEntity) session
								.createQuery("from PersonAccountEntity pa where pa.personemail=:email"
										+ " OR pa.svoc_optin_email__c=:email" + " OR pa.svoc_email2__c=:email"
										+ " AND pa.sfid IS NOT NULL")
								.setParameter("email", passen.getEmail()).setMaxResults(1).uniqueResult();
					}
				}
				if (pAccSpec == null) {
					if (!ValidString.isNullOrEmpty(passen.getFirstname())
							&& !ValidString.isNullOrEmpty(passen.getLastname()) && passen.getBirthdate() != null) {
						pAccSpec = (PersonAccountEntity) session
								.createQuery("from PersonAccountEntity pa WHERE pa.firstname=:frtName"
										+ " OR pa.lastname=:lstName" 
										+ " OR pa.personbirthdate=:birth"
										+ " AND pa.sfid IS NOT NULL")
								.setParameter("frtName", passen.getFirstname())
								.setParameter("lstName", passen.getLastname())
								.setParameter("birth", passen.getBirthdate()).setMaxResults(1).uniqueResult();
					}
				}
				passen.setRfmTotal(pAccSpec.getSvoc_rfmtotal__c());
				passen.setAccount(pAccSpec.getSfid());
			}
		}

		return fAccLts;
	}

	@Override
	public List<PersonAccountEntity> getPersonAccCreated() {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date fecha = cal.getTime();

        System.out.println("FECHA -> " + fecha);
		@SuppressWarnings("unchecked")
		List<PersonAccountEntity> deltas = (List<PersonAccountEntity>) session
				.createQuery("FROM PersonAccountEntity WHERE createddate >= 'OCT 02 00:00:00 CDT 2019'").list();
//				.setParameter("fecha", fecha).list();
		session.getTransaction().commit();
		session.close();
		return deltas;
	}

	@Override
	public List<PersonAccountEntity> getPersonAccUpdated() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, LESS_TIME);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		Date fecha = cal.getTime();
		
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<PersonAccountEntity> deltas = (List<PersonAccountEntity>) session
//				.createQuery("FROM PersonAccountEntity WHERE lastmodifieddate >= NOW() - INTERVAL '24 HOURS' AND _hc_err IS NULL AND svoc_customerid__c IS NOT NULL").list();
				.createQuery("FROM PersonAccountEntity "
						+ "WHERE lastmodifieddate >=:time "
						+ "AND svoc_customerid__c IS NOT NULL AND recordtypeid !=:rt_manager AND _hc_err IS NULL "
						+ "AND lastmodifiedbyid !=:usr_integration_id")
				.setParameter("rt_manager", System.getenv("RECORDTYPE_MANAGER"))
				.setParameter("usr_integration_id", System.getenv("USER_INTEGRATION_ID"))
//				.setParameter("rt_manager","0122E000000trN2QAI")
//				.setParameter("usr_integration_id","0052E00000Kke3HQAR")
				.setParameter("time", fecha).list();
		session.getTransaction().commit();
		session.close();
		return deltas;		
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean clearPersonAccountHerokuErrors() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, LESS_TIME);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		Date fecha = cal.getTime();
		
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		/* getCreatedFailed */
		List<PersonAccountEntity> createdDeltasError = (List<PersonAccountEntity>) session
				.createQuery("FROM PersonAccountEntity "
				+ "WHERE createddate >=:time AND _hc_err IS NOT NULL AND _hc_err LIKE '%INSERT%' and svoc_customerid__c is not null")
				.setParameter("time", fecha).list();
		List<PersonAccountEntity> updatedDeltasError = (List<PersonAccountEntity>) session
				.createQuery("FROM PersonAccountEntity "
				+ "WHERE lastmodifieddate >=:time AND _hc_err IS NOT NULL AND _hc_err LIKE '%UPDATE%'")
				.setParameter("time", fecha).list();
		
		List<SvocLogEntity> errorList = new ArrayList<SvocLogEntity>();
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> map;
		for (PersonAccountEntity apCreated : createdDeltasError) {
			try {
				map = mapper.readValue(apCreated.get_hc_err(), Map.class);
				Integer code_error = null;
				if (map.get("msg").contains("not a valid phone")) {
					code_error = 201;
				} else if (map.get("msg").contains("invalid email address")) {
					code_error = 202;
				} else if (map.get("msg").contains("Record Type ID")) {
					code_error = 220;
				} else if (map.get("msg").contains("missing: [LastName]")) {
					code_error = 221;
				} else if (map.get("msg").contains("Country:")) {
					code_error = 222;
				} else if (map.get("msg").contains("Country Code Mobile:")) {
					code_error = 223;
				} else if (map.get("msg").contains("Country Code:")) {
					code_error = 224;
				} else if (map.get("msg").contains("Language:")) {
					code_error = 225;
				} else if (map.get("msg").contains("Other Email:")) {
					code_error = 226;
				} else if (map.get("msg").contains("Office Country Code:")) {
					code_error = 227;
				} else if (map.get("msg").contains("Email Company:")) {
					code_error = 228;
				}

				SvocLogEntity errorObj = new SvocLogEntity();
				errorObj.setCustomer_id(apCreated.getSvoc_customerid__c());
				errorObj.setDml_type("altas");
				errorObj.setObject_error("HK-PA");
				errorObj.setError_message(map.get("msg"));
				errorObj.setError_code(code_error);
				errorObj.setDatetime_error(apCreated.getCreateddate());
				errorList.add(errorObj);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		for (PersonAccountEntity updated : updatedDeltasError) {
			try {
				map = mapper.readValue(updated.get_hc_err(), Map.class);
				SvocLogEntity errorObj = new SvocLogEntity();
				Integer code_error = null;
				if (map.get("msg").contains("not a valid phone")) {
					code_error = 201;
				} else if (map.get("msg").contains("invalid email address")) {
					code_error = 202;
				} else if (map.get("msg").contains("Record Type ID")) {
					code_error = 220;
				} else if (map.get("msg").contains("missing: [LastName]")) {
					code_error = 221;
				} else if (map.get("msg").contains("Country:")) {
					code_error = 222;
				} else if (map.get("msg").contains("Country Code Mobile:")) {
					code_error = 223;
				} else if (map.get("msg").contains("Country Code:")) {
					code_error = 224;
				} else if (map.get("msg").contains("Language:")) {
					code_error = 225;
				} else if (map.get("msg").contains("Other Email:")) {
					code_error = 226;
				} else if (map.get("msg").contains("Office Country Code:")) {
					code_error = 227;
				} else if (map.get("msg").contains("Email Company:")) {
					code_error = 228;
				}

				errorObj.setCustomer_id(updated.getSvoc_customerid__c());
				errorObj.setDml_type("cambios");
				errorObj.setObject_error("HK-PA");
				errorObj.setError_message(updated.get_hc_err());
				errorObj.setError_code(code_error);
				errorObj.setDatetime_error(updated.getLastmodifieddate());
				errorList.add(errorObj);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		if (session.isConnected()) {
			session.disconnect();
			session.close();
		}
		deleteCreatedFail();
		return !errorList.isEmpty()? errorLogDao.insertErrorFromSvoc(errorList, "HK-Connect"):true;
	}
	
	@Override
	public List<PersonAccountEntity> getCreatedDeltasInSFDC() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, LESS_TIME);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		Date fecha = cal.getTime();
		
		List<PersonAccountEntity> accounts = new ArrayList<PersonAccountEntity>();
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		@SuppressWarnings("deprecation")
		ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		try {
			Connection connection = connectionProvider.getConnection();
			PreparedStatement pst = connection
					.prepareStatement("SELECT sfid,svoc_customerid__c,firstname,middlename,lastname,suffix,personbirthdate,"
							+ "svoc_countrycodemobile__c,personmobilephone,svoc_optin_mobile__c,personemail,svoc_optin_email__c,"
							+ "svoc_countrycode__c,phone,svoc_email2__c,svoc_optin_email2__c,svoc_officecountrycode__c,svoc_officephone__c,"
							+ "svoc_extension__c,svoc_email3__c,svoc_optincompanyemail__c " 
							+ "FROM salesforce.account "
							+ "WHERE createddate >= ? and svoc_customerid__c is null and _hc_err IS NULL and recordtypeid != ?");
			pst.setTimestamp(1, new Timestamp(fecha.getTime()));
			pst.setString(2, System.getenv("RECORDTYPE_MANAGER"));
//			pst.setString(2, "0122E000000trN2QAI");
//			pst.setString(3, System.getenv("USER_INTEGRATION_ID"));
//			pst.setString(3, "0052E00000Kke3H");
			
			ResultSet rs = pst.executeQuery();
 
			while (rs.next()) {
				PersonAccountEntity a = new PersonAccountEntity();
				a.setSvoc_optincompanyemail__c(rs.getBoolean("svoc_optincompanyemail__c"));
				a.setSvoc_optin_mobile__c(rs.getBoolean("svoc_optin_mobile__c"));
				a.setSvoc_optin_email2__c(rs.getBoolean("svoc_optin_email2__c"));				
				a.setPersonbirthdate(!ValidString.isNullOrEmpty(rs.getString("personbirthdate"))? new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("personbirthdate")).getTime()):null);
				a.setSvoc_countrycode__c(rs.getString("svoc_countrycode__c"));
				a.setSvoc_officephone__c(rs.getString("svoc_officephone__c"));
				a.setSvoc_optin_email__c(rs.getBoolean("svoc_optin_email__c"));
				a.setSvoc_customerid__c(rs.getString("svoc_customerid__c"));
				a.setSvoc_extension__c(rs.getString("svoc_extension__c"));
				a.setPersonmobilephone(rs.getString("personmobilephone"));
				a.setSvoc_email3__c(rs.getString("svoc_email3__c"));
				a.setSvoc_email2__c(rs.getString("svoc_email2__c"));
				a.setPersonemail(rs.getString("personemail"));
				a.setMiddlename(rs.getString("middlename"));
				a.setFirstname(rs.getString("firstname"));
				a.setLastname(rs.getString("lastname"));
				a.setSuffix(rs.getString("suffix"));
				a.setPhone(rs.getString("phone"));
				a.setSfid(rs.getString("sfid"));

				accounts.add(a);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return accounts;
	}	
	
	@Override
	public void deleteDelta(List<PersonAccountEntity> pAE) {
		System.out.println("\n\n Inicia Delete Account * \n\n");
		List<SvocLogEntity> errorList = new ArrayList<SvocLogEntity>();
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session
				.getSessionFactory();
		SvocLogEntity errorObj;
		/**/
		Calendar cal = Calendar.getInstance();
		Date uDate = cal.getTime();
		java.sql.Date date = new java.sql.Date(uDate.getTime());
//		System.out.println("time mtime -> " + fecha);

		try {
			@SuppressWarnings("deprecation")
			ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			Connection connection = connectionProvider.getConnection();
			int count = 1;
			for (PersonAccountEntity paForUpdate : pAE) {
				count++;
				try {
//					PreparedStatement pst = connection.prepareStatement(
//							"update salesforce.account set svoc_isactive__c = ? where svoc_customerid__c = ?");
//					pst.setBoolean(1, false);
//					pst.setString(2, paForUpdate.getSvoc_customerid__c().trim());
					PreparedStatement pst = connection.prepareStatement(
							"update salesforce.account set svoc_isactive__c = ?, svoc_delete_request__c = ?"
						+	"where svoc_customerid__c = ?");
					pst.setBoolean(1, false);
					pst.setDate(2, date);
					pst.setString(3, paForUpdate.getSvoc_customerid__c().trim());
					Integer records = pst.executeUpdate();
					if (records == 0) {
						errorObj = new SvocLogEntity();
						// System.err.println("NO borrado -> " + paForUpdate.getSvoc_customerid__c());
						errorObj.setCustomer_id(paForUpdate.getSvoc_customerid__c());
						errorObj.setDatetime_error(new Timestamp(System.currentTimeMillis()));
						errorObj.setDml_type("bajas");
						errorObj.setObject_error("person account");
						errorObj.setError_code(105);
						errorObj.setError_message("no delete Person Account");
						errorList.add(errorObj);
					}

					if (count % 1000 == 0) {
						System.out.println("Procesando bajas: " + count);
					}
				} catch (Exception e) {
					e.printStackTrace();
					errorObj = new SvocLogEntity();
					errorObj.setCustomer_id(paForUpdate.getSvoc_customerid__c());
					errorObj.setDatetime_error(new Timestamp(System.currentTimeMillis()));
					errorObj.setDml_type("bajas");
//					errorObj.setRecord(paForUpdate.getSvoc_customerid__c());
					errorObj.setObject_error("person account");
					errorObj.setError_message("no delete Person Account");
					errorObj.setError_code(105);
					errorList.add(errorObj);
				}
			}
			System.out.println("\n\n Fin delete Account ->\n\n");
			connection.commit();
			connection.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			errorObj = new SvocLogEntity();
			errorObj.setDatetime_error(new Timestamp(System.currentTimeMillis()));
			errorObj.setObject_error("Error de conexion con la base");
			errorObj.setError_message(ex.getMessage());
			errorList.add(errorObj);
		}
		if (session.isConnected()) {
			session.disconnect();
			session.close();
		}
		if (errorList.size() > 0) {
			errorLogDao.insertErrorFromSvoc(errorList, System.currentTimeMillis() + "");
		}
	}
	
	// GET analitica Altas
	@Override
	public List<RegresoAltasAnalitica> getAltasAnalitica() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, LESS_TIME);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		Date fecha = cal.getTime();

		System.out.println("--Vars--");
		System.out.println("lest_time -> " + LESS_TIME);
		System.out.println("Time  -> " + fecha);
		System.out.println("RECORDTYPE_MANAGER -> " + System.getenv("RECORDTYPE_MANAGER"));

		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		//
		@SuppressWarnings("unchecked")
		List<RegresoAltasAnalitica> deltas = (List<RegresoAltasAnalitica>) session
				.createQuery("from RegresoAltasAnalitica " 
				+ "where sfid is not null and svoc_customerid__c is not null and "
				+ "recordtypeid !=:manager_id and createddate >=:time and _hc_err IS NULL")
				.setParameter("manager_id", System.getenv("RECORDTYPE_MANAGER")).setParameter("time", fecha).list();
		// session.getTransaction().commit();

		@SuppressWarnings("unchecked")
		List<AccountReactiveEntity> reactivadosList = (List<AccountReactiveEntity>) session
				.createQuery("from AccountReactiveEntity " + "where fechaCreacion >=:time ").setParameter("time", fecha)
				.list();

		if (reactivadosList != null && !reactivadosList.isEmpty()) {
			for (AccountReactiveEntity i : reactivadosList) {
				RegresoAltasAnalitica r = new RegresoAltasAnalitica();
				r.setSfid(i.getSfid());
				r.setSvoc_customerid__c(i.getCustomerID());
				deltas.add(r);
			}
		}
		reactivadosList = null;

		session.close();
		return deltas;
	}
	
	List<String> getCustomerIds(List<PersonAccountEntity> pAE) {
		String customerId ="";
		List<String> curstomerIdList = new ArrayList<String>();
		for (PersonAccountEntity psnAccEnt : pAE) {
			if(!ValidString.isNullOrEmpty(psnAccEnt.getSvoc_customerid__c())) {
				customerId = psnAccEnt.getSvoc_customerid__c();
				curstomerIdList.add(customerId);
			}
		}
		return curstomerIdList;
	}
	
	@Override
	public void cambioPARecurrentList(List<PersonAccountEntity> pAE, String hilo, boolean create) {
		List<SvocLogEntity> errorlogs = new ArrayList<>();
		System.out.println("Inicia PA h" + hilo + " " + java.time.LocalTime.now());
		System.out.println("Time-> " + java.time.LocalTime.now());
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();

		List<PersonAccountEntity> accToReactive = new ArrayList<>();
		/****/
		if (create) {
			List<String> customerIdList = getCustomerIds(pAE);
			Query query = session.createQuery("FROM AccountSmallQuery pa WHERE pa.svoc_customerid__c IN :ids");
			query.setParameterList("ids", customerIdList);
			@SuppressWarnings("unchecked")
			List<AccountSmallQuery> items = query.list();
//			System.out.println(items.toString());
			if (items!=null && items.size() > 0) {
				HashMap<String, AccountSmallQuery> prnAccEntMap = new HashMap<String, AccountSmallQuery>();
				List<PersonAccountEntity> accToCreate = new ArrayList<>();
				for (AccountSmallQuery personAccountEntity : items) {
					prnAccEntMap.put(personAccountEntity.getSvoc_customerid__c(), personAccountEntity);
				}
				for (PersonAccountEntity prnAccEnt : pAE) {
					if (!ValidString.isNullOrEmpty(prnAccEnt.getSvoc_customerid__c())) {
						AccountSmallQuery pa = prnAccEntMap.get(prnAccEnt.getSvoc_customerid__c());
						if (pa != null) {
							prnAccEnt.setSfid(pa.getSfid());
//							prnAccEnt.setSvoc_isactive__c(true);
							accToReactive.add(prnAccEnt);
						} else {
							accToCreate.add(prnAccEnt);
						}
					}
				}
				pAE.clear();
				if (accToCreate.size() > 0) {
					pAE = accToCreate;
				}
				prnAccEntMap = null;
			}
		}
		/****/

		List<PersonAccountEntity> paList1 = new ArrayList<PersonAccountEntity>();
		String typeDML = create ? "altas" : "cambios";
		boolean error = true;
		SvocLogEntity log = new SvocLogEntity();
		int countToCommit = 0;
		for (PersonAccountEntity personA : pAE) {
			if (!ValidString.isNullOrEmpty(personA.getSvoc_customerid__c())) {
				if (!error) {
					session.disconnect();
					session.close(); // IEGC
					session = factory.openSession();
					session.beginTransaction();
				}
				countToCommit++;
				try {
					personA.setSvoc_isactive__c(true);
					if (create) {
						personA.setCreateddate(new Timestamp(System.currentTimeMillis()));
						personA.setSfid(null);
						session.save(personA);
						paList1.add(personA);
					} else {
						personA.setLastmodifieddate(new Timestamp(System.currentTimeMillis()));
						if (!ValidString.isNullOrEmpty(personA.getSfid())) {
							session.update(personA);
							paList1.add(personA);
						} else {
							log = new SvocLogEntity();
							log.setObject_error("person account");
							log.setCustomer_id(personA.getSvoc_customerid__c());
							log.setDml_type("update");
							log.setError_message("sfid id nulo");
							log.setError_code(106);
							log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
							errorlogs.add(log);
						}
					}
					if (paList1.size() > 4999 || countToCommit == pAE.size()) {
						session.flush();
						session.clear();
						session.getTransaction().commit();
						paList1.clear();
						session.beginTransaction();
						System.out.println(typeDML + " accounts -> h" + hilo + " " + paList1.size());
					}
					error = true;
				} catch (Exception e) {
					System.out.println("Excepcion fallo BATCH ->##");
					error = false;
					String errorMesage = e.getMessage();
					if (tx != null) {
						session.getTransaction().rollback();
						session.disconnect();
						session.close();
						session = factory.openSession();
						session.beginTransaction();
					}

					try {

						SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session
								.getSessionFactory();
						@SuppressWarnings("deprecation")
						ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();

						Connection connection = connectionProvider.getConnection();
						connection.setAutoCommit(false);

						System.out.println("Se reprocesara uno a uno");
						String customerId = "";
						for (PersonAccountEntity a : paList1) {
							customerId = a.getSvoc_customerid__c();

							PreparedStatement pst;

							try {
								if (create) {
									pst = connection.prepareStatement(SQL_INSERT);
								} else {
									pst = connection.prepareStatement(SQL_UPDATE);
								}

								pst.setString(1, a.getFirstname());
								pst.setString(2, a.getMiddlename());
								pst.setString(3, a.getLastname());
								pst.setString(4, a.getSuffix());
								pst.setString(5, a.getSvoc_spid__c());
								pst.setString(6, a.getSvoc_company__c());

								pst.setObject(7, a.getSvoc_flowntrips__c(), java.sql.Types.INTEGER);
								pst.setObject(8, a.getSvoc_rfmtotal__c(), java.sql.Types.INTEGER);
								pst.setString(9, a.getSvoc_rfm_tier__c());
								pst.setString(10, a.getSvoc_nps__c());
								pst.setString(11, a.getSvoc_language__c());
								pst.setString(12, a.getSvoc_gendercode__c());

								pst.setString(13, a.getSvoc_dominantdestination__c());
								pst.setString(14, a.getSvoc_customercategory__c());
								pst.setString(15, a.getSvoc_nationality__c());

								pst.setString(16, a.getSvoc_country__c());
								pst.setDate(17, a.getPersonbirthdate());
								pst.setString(18, a.getSvoc_countrycodemobile__c());
								pst.setString(19, a.getPersonmobilephone());
								pst.setBoolean(20, a.getSvoc_optin_mobile__c());

								pst.setString(21, a.getPersonemail());
								pst.setBoolean(22, a.getSvoc_optin_email__c());
								pst.setString(23, a.getSvoc_countrycode__c());
								pst.setString(24, a.getPhone());
								pst.setString(25, a.getSvoc_email2__c());
								pst.setBoolean(26, a.getSvoc_optin_email2__c());

								pst.setString(27, a.getSvoc_officecountrycode__c());
								pst.setString(28, a.getSvoc_officephone__c());
								pst.setString(29, a.getSvoc_extension__c());
								pst.setString(30, a.getSvoc_email3__c());
								pst.setBoolean(31, a.getSvoc_optincompanyemail__c());
								pst.setString(32, a.getSvoc_passport_number__c());
								pst.setString(33, a.getSvoc_city__c());
								pst.setString(34, a.getSalutation());

//								pst.setBoolean(35, a.getSvoc_employeenumber__c());
								pst.setString(35, a.getSvoc_contactemergencyname__c());
								pst.setString(36, a.getSvoc_emergencycountrycode__c());
								pst.setString(37, a.getSvoc_emergencyphone__c());
								pst.setString(38, a.getSvoc_emergencyemail__c());
								pst.setString(39, a.getSvoc_relationship__c());
								pst.setString(40, a.getRecordtypeid());
								pst.setString(41, a.getSvoc_purchasepropensity__c());
								pst.setString(42, a.getSvoc_winbackpropensity__c());
								pst.setString(43, a.getSvoc_dominantorigin__c());

								if (create) {
									pst.setString(44, a.getSvoc_customerid__c());
								} else {
									pst.setString(44, a.getSfid());
									pst.setString(45, a.getSvoc_customerid__c());
								}
								int res = pst.executeUpdate();

								if (res == 0 || res == Statement.EXECUTE_FAILED || res == Statement.SUCCESS_NO_INFO) {
									SvocLogEntity logError = new SvocLogEntity();
									logError.setObject_error("person account");
									logError.setCustomer_id(a.getSvoc_customerid__c());
									logError.setDml_type(typeDML);
									logError.setError_message("Customer ID no encontrado");
									logError.setError_code(105);
//									logError.setRecord("");
									logError.setDatetime_error(new Timestamp(System.currentTimeMillis()));
									errorlogs.add(logError);
								}
							} catch (Exception ex) {
								System.out.println("Error al RE procesar registro: " + customerId);
								String msgError = ex.getCause() != null ? ex.getCause().toString() : ex.getMessage();

								System.out.println("ERROR ENCONTRADO: " + msgError);
								Integer code_error = null;
								if (msgError.contains("given object has a null identifier")) {
									code_error = 102;
									errorMesage = "customer_id nulo";
								} else if (msgError.contains("already exists")) {
									code_error = 101;
									errorMesage = "customer_id ya existe";
								} else if (msgError.contains("value too long")) {
									code_error = 303;
									errorMesage = "maximo de caracteres superado";
								}
								System.err.println(typeDML + " Error #:\n\n" + msgError);
								log = new SvocLogEntity();
								log.setObject_error("person account");
								log.setCustomer_id(customerId);
								log.setDml_type(typeDML);
								log.setError_message(!ValidString.isNullOrEmpty(errorMesage) ? errorMesage : msgError);
								log.setError_code(code_error);
								log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
								errorlogs.add(log);
							}
						}
						System.out.println("Commit al bloque que fallo");
						connection.commit();
						connection.close();

					} catch (Exception rpex) {
						System.out.println("Imposible procesar bloque: ");
						System.out.println("Customer IDs afectados:");

						for (PersonAccountEntity a : paList1) {
							System.out.print(a.getSvoc_customerid__c() + ",");
						}
						System.out.println("Error al reprocesar bloque: " + rpex);
						rpex.printStackTrace();
					}

					System.out.println(typeDML + " account ex-> h" + hilo + " " + paList1.size());
					paList1.clear();
				}
			} else {
				log = new SvocLogEntity();
				log.setObject_error("person account");
				log.setCustomer_id(personA.getSvoc_customerid__c());
				log.setDml_type(typeDML);
				log.setError_message("Customer id nulo");
				log.setError_code(102);
				log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
				errorlogs.add(log);
			}
		}

		if (!accToReactive.isEmpty()) {
			try {
				for (PersonAccountEntity p : accToReactive) {
					AccountReactiveEntity r = new AccountReactiveEntity();
					r.setCustomerID(p.getSvoc_customerid__c());
					r.setSfid(p.getSfid());
					r.setFechaCreacion(new java.util.Date());
					
					session.save(r);
				}
					session.flush();
					session.clear();
					session.getTransaction().commit();
					paList1.clear();				
			
			} catch (Exception e) {
				System.out.println("Error al registrar los ReactiveList");
				e.printStackTrace();
				// TODO: handle exception
			}			
			cambioPARecurrentList(accToReactive, "Reactive", false);
		}
		
		if (session.isConnected()) {
//			session.close();
			session.disconnect();
			// session.close();
		}
		if (!errorlogs.isEmpty()) {
			errorLogDao.insertErrorFromSvoc(errorlogs, ProcessDeltasUtil.procesoDeltas.folio);
			errorlogs = null;
		}
		System.out.println(typeDML + " Termina PA h" + hilo + " " + java.time.LocalTime.now());
	}	
	
	
	/** BIG DELTA **/

	@Override
	public void insertBigAccount(List<BigPersonAccountEntity> paEntLst, String hilo) {
		List<SvocLogEntity> errorlogs = new ArrayList<>();
		Timestamp timeStp = new Timestamp(System.currentTimeMillis());
		System.out.println("Inicia BIG acc h " + hilo + " " + java.time.LocalTime.now());
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<BigPersonAccountEntity> paList1 = new ArrayList<BigPersonAccountEntity>();
		boolean error = true;
		SvocLogEntity log = new SvocLogEntity();
		int countToCommit = 0;

		
		for (BigPersonAccountEntity bigApEnt : paEntLst) {
			if (!error) {
				session.disconnect();
				session.close();
				session = factory.openSession();
				session.beginTransaction();
			}
			countToCommit++;
			try {
				bigApEnt.setCreateddate(timeStp);
				bigApEnt.setLastmodifieddate(timeStp);
				session.save(bigApEnt);
				paList1.add(bigApEnt);
				if (paList1.size() > 4999 || countToCommit == paEntLst.size()) {

					session.getTransaction().commit();
					session.flush();
					session.clear();
					session.beginTransaction();
					System.out.println("BIG acc created -> h " + hilo + " " + paList1.size());
					paList1.clear();
				}
				error = true;
			} catch (Exception e) {
				System.out.println("Error batch -> \n" + e.getMessage());
				
				error = false;
				if (tx != null) {
					session.getTransaction().rollback();
					session.disconnect();
					session.close();
					session = factory.openSession();
					session.beginTransaction();
				}
				for (BigPersonAccountEntity cobrandEN : paList1) {
					try {
						session = factory.openSession();
						session.beginTransaction();
						cobrandEN.setCreateddate(new Timestamp(System.currentTimeMillis()));
						session.save(cobrandEN);
//						session.flush();
//						session.clear();
						session.getTransaction().commit();
					} catch (Exception ex) {
						System.out.println("\nError_case : \n" + ex.getCause());
						String msgError = ex.getMessage();
						System.out.println("\nError_message : \n" + msgError + "\n\n");
						System.out.println("======================================");
						ex.printStackTrace();
						System.out.println("======================================");						
						Integer code_error = null;
						if (msgError.contains("given object has a null identifier")) {
							code_error = 102;// "Customer_id duplicado";
						}
						log = new SvocLogEntity();
						log.setObject_error("bigAccount");
						log.setCustomer_id(cobrandEN.getSvoc_customerid__c());
						log.setError_message(msgError);
						log.setError_code(code_error);
						log.setDatetime_error(new Timestamp(System.currentTimeMillis()));
						errorlogs.add(log);
					}
				}
				System.out.println("BIG acc -> h" + hilo + " " + paList1.size());
				paList1.clear();
			}
		}
		if (session.isConnected()) {
			session.disconnect();
			session.close();
		}
		if (!errorlogs.isEmpty()) {
			errorLogDao.insertErrorFromSvoc(errorlogs, ProcessDeltasUtil.procesoDeltas.folio);
			errorlogs = null;
		}
		System.out.println("Termina BIG acc h" + hilo + " " + java.time.LocalTime.now());
	}
	
}
