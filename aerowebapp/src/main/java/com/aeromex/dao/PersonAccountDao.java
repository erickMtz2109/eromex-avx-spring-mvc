package com.aeromex.dao;

import java.util.Date;
import java.util.List;

import com.aeromex.entity.BigPersonAccountEntity;
import com.aeromex.entity.PersonAccountEntity;
import com.aeromex.entity.RegresoAltasAnalitica;
import com.aeromex.entity.ReservationEntity;
import com.aeromex.entity.SvocLogEntity;
import com.aeromex.model.FindAccount;

public interface PersonAccountDao {
	public boolean createDeltas(PersonAccountEntity pAE);
	public boolean updateDeltas(PersonAccountEntity pAE);
	List<PersonAccountEntity> personAccountList();
	List<PersonAccountEntity> updatePersonAccout(List<PersonAccountEntity> pAccLts);
	List<PersonAccountEntity> getAccoutByReserv(ReservationEntity reservObj);
	int upsertPersonAccountDelta(PersonAccountEntity pAE);
//	void upserthierarchiesCobrandAndFrequentFlyer(String externalIdAcc, HierarchyWrapper hierarchyobj);
    List<PersonAccountEntity> getDeltas(Date date);
    List<SvocLogEntity> getErrorLogs();
    List<FindAccount> findAccountsDao(List<FindAccount> fAccLts);
	public List<PersonAccountEntity> getPersonAccCreated();
	public List<PersonAccountEntity> getPersonAccUpdated();
	List<PersonAccountEntity> getCreatedDeltasInSFDC();
	void deleteDelta(List<PersonAccountEntity> pAE);
	public List<RegresoAltasAnalitica> getAltasAnalitica();	
	void cambioPARecurrentList(List<PersonAccountEntity> pAE, String hilo, boolean cOru);
	void insertBigAccount(List<BigPersonAccountEntity> bigPersonAccountList, String hilo);
	boolean clearPersonAccountHerokuErrors();
}
