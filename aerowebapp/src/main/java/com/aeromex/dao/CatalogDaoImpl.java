package com.aeromex.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.aeromex.entity.AircraftEntity;
import com.aeromex.utility.ConnectionUtil;

@Repository
public class CatalogDaoImpl implements CatalogDao{

	public void upsertAirCraft(List<AircraftEntity> Aircrafts) {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		for (AircraftEntity airCEntity : Aircrafts) {
			AircraftEntity airC = (AircraftEntity) session
					.createQuery("from AircraftEntity air where air.svoc_externalid__c=:externalId")
					.setParameter("externalId", airCEntity.getSvoc_externalid__c()).uniqueResult();
			if (airC != null) {
				airC = updateEntity(airC, airCEntity);
				session.update(airC);
			} else {
				session.save(airCEntity);
			}
		}
		session.getTransaction().commit();
		session.close();
	}

	private AircraftEntity updateEntity(AircraftEntity airC1, AircraftEntity airC2) {
		airC1.setCreatedbyid(airC2.getCreatedbyid());
		airC1.setCreateddate(airC2.getCreateddate());
		airC1.setIsdeleted(airC2.getIsdeleted());
		airC1.setLastmodifiedbyid(airC2.getLastmodifiedbyid());
		airC1.setLastmodifieddate(airC2.getLastmodifieddate());
		airC1.setName(airC2.getName());
		airC1.setOwnerid(airC2.getOwnerid());
		airC1.setSvoc_active__c(airC2.getSvoc_active__c());
		airC1.setSvoc_code__c(airC2.getSvoc_code__c());
		airC1.setSvoc_iata_code__c(airC2.getSvoc_iata_code__c());
		airC1.setSvoc_icao_code__c(airC2.getSvoc_icao_code__c());
		airC1.setSvoc_operator__c(airC2.getSvoc_operator__c());
		airC1.setSvoc_physical_capacity__c(airC2.getSvoc_physical_capacity__c());
		airC1.setSvoc_subfleet__c(airC2.getSvoc_subfleet__c());
		airC1.setSvoc_tail_number__c(airC2.getSvoc_tail_number__c());
		airC1.setSystemmodstamp(airC2.getSystemmodstamp());
		return airC1;
	}

}
