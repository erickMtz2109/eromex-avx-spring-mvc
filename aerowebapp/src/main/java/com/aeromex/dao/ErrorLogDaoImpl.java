package com.aeromex.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.aeromex.entity.SvocLogEntity;
import com.aeromex.utility.ConnectionUtil;

@Repository
public class ErrorLogDaoImpl implements ErrorLogDao {
//	public boolean insertErrorFromSvoc(List<SvocLogEntity> listErros, String folio) {
//		SessionFactory factory = ConnectionUtil.getSessionFactory();
//		Session session = factory.openSession();
//		session.beginTransaction();
//		try {
//			for (SvocLogEntity log : listErros) {
//				log.setFolio(folio);
//				session.save(log);
//			}
//			session.getTransaction().commit();
//			session.close();
//		} catch (Exception eh) {
//			System.out.println("\n\n\nException in Errors #");
////			System.out.println("");
//			eh.getCause();
//			eh.printStackTrace();
//			return false;
//		}
//		return true;
//	}

	public boolean insertErrorFromSvoc(List<SvocLogEntity> listErros, String folio) {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		System.out.println("Errores to insert -> " + listErros.size());
		for (SvocLogEntity log : listErros) {
			if (log.getError_message() != null) {
				if (log.getError_message().length() > 255) {
					log.setError_message(log.getError_message().substring(0, 255));
				}
			}
			try {
				log.setFolio(folio);
				session.save(log);
			} catch (Exception e) {
				System.out.println("\n\n\nException in Errors #");
				System.out.println("Record -> " + log.toString());
				System.out.println("Error message \n\n");
				e.printStackTrace();
				if (session.isConnected()) {
					session.disconnect();
					session.close();
				}
				return false;
			}
		}
		session.getTransaction().commit();
		if (session.isConnected()) {
			session.disconnect();
			session.close();
		}

		return true;
	}

	@Override
	public List<SvocLogEntity> getLogError(String folio) {
		SessionFactory factory = ConnectionUtil.getSessionFactory();
		Session session = factory.openSession();

		@SuppressWarnings("unchecked")
		List<SvocLogEntity> errores = (List<SvocLogEntity>) session
				.createQuery("from SvocLogEntity  where folio=:folio").setParameter("folio", folio).list();

		return errores;
	}
}
