package com.aeromex.dao;

import java.util.HashMap;

public interface PNR_AttributeDao {
	HashMap<String, Integer> mapAttribute();
}
