package com.aeromex.dao;

import java.util.List;
import com.aeromex.entity.PassengerEntity;
import com.aeromex.entity.ReservationEntity;

public interface PassengerDao {
	List<PassengerEntity> passengerList();
	List<PassengerEntity> getPassengerByReserv(ReservationEntity reservObj);
}
